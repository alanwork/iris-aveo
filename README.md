Steps

Clone from repo

Create app.php & database.php in app/config/local (copy from 'production')

Add lines below in MAMP/conf/apache/httpd.conf:
<Directory "/Applications/MAMP/htdocs/aveo/public/">
   Allow From All
   AllowOverride All
   Options +Indexes
</Directory>

Restart Apache

Setup database
