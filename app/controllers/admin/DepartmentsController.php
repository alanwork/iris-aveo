<?php namespace Controllers\Admin;

use AdminController;
use Input;
use Lang;
use Redirect;
use Setting;
use DB;
use Sentry;
use Str;
use Validator;
use View;
use Departments;
use Divisions;
use League\Csv\Reader;
use Chumper\Datatable\Facades\Datatable;
use Config;

class DepartmentsController extends AdminController
{
    protected $duplicate_dept = array();
    /**
     * Show a list of all the departments.
     *
     * @return View
     */

    public function getIndex()
    {
        // Grab all the departments
        $departments = Departments::orderBy('created_at', 'DESC');
        if (Input::get('Deleted')) {
            $departments->withTrashed()->Deleted();
        }

        $departments = $departments->with('division')->paginate(15);
        // Show the page
        return View::make('backend/departments/index', compact('departments'));


        // $models = $models->with('category','assets','depreciation')->get();

        // // Show the page
        // return View::make('backend/models/index', compact('models'));
    }


    /**
     * Department create.
     *
     * @return View
     */
    public function getCreate($div_id = null)
    {
        // Show the page
         //$department_types= array('' => '', 'asset' => 'Asset', 'accessory' => 'Accessory');

        $div_list = array('' => 'Select a Division') + DB::table('divisions')
        ->select(DB::raw('concat(name," - ",code) as name, id'))->orderBy('name', 'asc')
        ->orderBy('id', 'asc')
        ->whereNull('deleted_at')
        ->lists('name', 'id');

        // $supplier_list = array('' => '') + Supplier::orderBy('name', 'asc')->lists('name', 'id');
        // $assigned_to = array('' => 'Select a User') + DB::table('users')->select(DB::raw('concat (first_name," ",last_name) as full_name, id'))->whereNull('deleted_at')->lists('full_name', 'id');
        // $location_list = array('' => '') + Location::orderBy('name', 'asc')->lists('name', 'id');


        // // Grab the dropdown list of status
        // $statuslabel_list = Statuslabel::orderBy('name', 'asc')->lists('name', 'id');

        $view = View::make('backend/departments/edit');
        //$view->with('supplier_list',$supplier_list);
        //$view->with('model_list',$model_list);
        //$view->with('statuslabel_list',$statuslabel_list);
        //$view->with('assigned_to',$assigned_to);
        //$view->with('location_list',$location_list);
        $view->with('div_list',$div_list);
        $view->with('department',new Departments);

        if (!is_null($div_id)) {
            $selected_div = Divisions::find($div_id);
            $view->with('selected_div',$selected_div);
        }

        return $view;
        //return View::make('backend/departments/edit')->with('department',new Departments);
    }


    /**
     * Department create form processing.
     *
     * @return Redirect
     */
    public function postCreate()
    {

        // create a new model instance
        $department = new Departments();

        $validator = Validator::make(Input::all(), $department->rules);

        if ($validator->fails())
        {
            // The given data did not pass validation
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        else{

            // Update the department data
            $department->name                 = e(Input::get('name'));
            $department->code                 = e(Input::get('code'));
            $department->div_id               = e(Input::get('div_id'));
            //$department->user_id              = Sentry::getId();

            // Was the asset created?
            if($department->save()) {
                // Redirect to the new department  page
                return Redirect::to("admin/settings/departments")->with('success', Lang::get('admin/departments/message.create.success'));
            }
        }

        // Redirect to the department create page
        return Redirect::to('admin/settings/departments/create')->with('error', Lang::get('admin/departments/message.create.error'));


    }

    /**
     * Department update.
     *
     * @param  int  $departmentId
     * @return View
     */
    public function getEdit($departmentId = null)
    {
        // Check if the department exists
        if (is_null($department = Departments::find($departmentId))) {
            // Redirect to the blogs management page
            return Redirect::to('admin/settings/departments')->with('error', Lang::get('admin/departments/message.does_not_exist'));
        }

        $div_list = array('' => 'Select a Division') + DB::table('divisions')
        ->select(DB::raw('concat(name," - ",code) as name, id'))->orderBy('name', 'asc')
        ->orderBy('id', 'asc')
        ->whereNull('deleted_at')
        ->lists('name', 'id');


        // Show the page
        //$department_options = array('' => 'Top Level') + Department::lists('name', 'id');

        $department_options = array('' => 'Top Level') + DB::table('departments')->where('id', '!=', $departmentId)->lists('name', 'id');
        $department_types= array('' => '', 'asset' => 'Asset', 'accessory' => 'Accessory');
        
        return View::make('backend/departments/edit', compact('department'))
        ->with('department_options',$department_options)
        ->with('department_types',$department_types)
        ->with('div_list', $div_list);
    }


    /**
     * Department update form processing page.
     *
     * @param  int  $departmentId
     * @return Redirect
     */
    public function postEdit($departmentId = null)
    {
        // Check if the blog post exists
        if (is_null($department = Departments::find($departmentId))) {
            // Redirect to the blogs management page
            return Redirect::to('admin/departments')->with('error', Lang::get('admin/departments/message.does_not_exist'));
        }


        // get the POST data
        $new = Input::all();

        // attempt validation
        $validator = Validator::make(Input::all(), $department->validationRules($departmentId));


        if ($validator->fails())
        {
            // The given data did not pass validation
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        // attempt validation
        else {

            // Update the department data
            $department->name            = e(Input::get('name'));
            $department->code            = e(Input::get('code')); 
            $department->div_id          = e(Input::get('div_id')); 

            // Was the asset created?
            if($department->save()) {
                // Redirect to the new department page
                return Redirect::to("admin/settings/departments")->with('success', Lang::get('admin/departments/message.update.success'));
            }
        }

        // Redirect to the department management page
        return Redirect::to("admin/settings/departments/$departmentID/edit")->with('error', Lang::get('admin/departments/message.update.error'));

    }

    /**
     * Delete the given department.
     *
     * @param  int  $departmentId
     * @return Redirect
     */
    public function getDelete($departmentId)
    {
        // Check if the department exists
        if (is_null($department = Departments::find($departmentId))) {
            // Redirect to the blogs management page
            return Redirect::to('admin/settings/departments')->with('error', Lang::get('admin/departments/message.not_found'));
        }


        // if ($department->has_department() > 0) {

        //     // Redirect to the asset management page
        //     return Redirect::to('admin/settings/departments')->with('error', Lang::get('admin/departments/message.assoc_users'));
        // } else {

            $department->delete();

            // Redirect to the locations management page
            return Redirect::to('admin/settings/departments')->with('success', Lang::get('admin/departments/message.delete.success'));
       // }


    }



    /**
    *  Get the asset information to present to the department view page
    *
    * @param  int  $assetId
    * @return View
    **/
    public function getView($departmentID = null)
    {
        $department = Department::find($departmentID);

        if (isset($department->id)) {
                return View::make('backend/departments/view', compact('department'));
        } else {
            // Prepare the error message
            $error = Lang::get('admin/departments/message.does_not_exist', compact('id'));

            // Redirect to the user management page
            return Redirect::route('departments')->with('error', $error);
        }


    }

    public function getRestore($departmentId = null)
    {

        // Get user information
        $department = Departments::withTrashed()->find($departmentId);

         if (isset($department->id)) {

            // Restore the model
            $department->restore();

            // Prepare the success message
            $success = Lang::get('admin/departments/message.restore.success');

            // Redirect back
            return Redirect::back()->with('success', $success);

         } else {
             return Redirect::back()->with('error', Lang::get('admin/departments/message.not_found'));
         }

    }

/**
     * Department import.
     *
     * @return View
     */
    public function getImportDepartment()
    {
        // Get all the available groups
        $groups = Sentry::getGroupProvider()->findAll();
        // Selected groups
        $selectedGroups = Input::old('groups', array());
        // Get all the available permissions
        $permissions = Config::get('permissions');
        $this->encodeAllPermissions($permissions);
        // Selected permissions
        $selectedPermissions = Input::old('permissions', array('superuser' => -1));
        $this->encodePermissions($selectedPermissions);
        // Show the page
        return View::make('backend/departments/import', compact('groups', 'selectedGroups', 'permissions', 'selectedPermissions'));
    }
    
    
    /**
     * Asset import form processing.
     *
     * @return Redirect
     */
    public function postImportDepartment()
    {
        //dd(Input::file('asset_import_csv'));
        //$duplicate_serial = '';
        if (! ini_get("auto_detect_line_endings")) {
            ini_set("auto_detect_line_endings", '1');
        }
        
        $csv = Reader::createFromPath(Input::file('department_import_csv'));  
        $csv->setNewline("\r\n");
        
        if (Input::get('has_headers')==1) {
            $csv->setOffset(1); 
        }
        
        $duplicates = '';  

        //echo $duplicates;  
        //dd($csv);

        $nbInsert = $csv->each(function ($row) use ($duplicates) {
            
            if (array_key_exists(2, $row)) {
              
                try {   
                        $today = date('Y-m-d H:i:s');
                        //dd('try');
                        //dd($row[0]);
                        //var_dump($row);
                        //Check if this asset already exists in the system
                        $code = DB::table('departments')->where('code', $row[1])->first();
                        if ($code) {                    
                            $duplicates .= $row[1];
                            //echo $duplicates;
                        } else {

                            $div_id = Divisions::where('code', $row[2])->first();
                            if($div_id)
                                $div_id = $div_id->id;
                            else
                                $div_id = '';

                                    $newdept = array(
                                        'name'                   => $row[0],
                                        'code'                   => $row[1],
                                        'div_id'                 => $div_id,
                                        'created_at'             => $today,
                                        'updated_at'             => $today,
                                        'deleted_at'             => null,          
                                    );
                                        
                                    DB::table('departments')->insert($newdept);
                                }
                                
                    
                } catch (Exception $e) {
                    echo 'Caught exception: ',  $e->getMessage(), "\n";
                }
                //echo $duplicates;
                array_push($this->duplicate_dept, $duplicates); 
                return true;
            } //echo $duplicates;
        }); 
        
        //echo $duplicates;
        //dd($duplicates);
        //$duplicates = 'There is duplication';
         var_dump($this->duplicate_dept);
        if($this->duplicate_dept)
            $duplicates = "Duplicate departments(Code) : " . implode(",", $this->duplicate_dept);
        else
            $duplicates = '';
        //return Redirect::to("admin/settings/departments")->with('success', Lang::get('admin/departments/message.create.success'));
        return Redirect::route('departments')->with('duplicates',$duplicates)->with('success', 'Success');
        
    }
}
