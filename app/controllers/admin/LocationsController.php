<?php namespace Controllers\Admin;

use AdminController;
use Input;
use Lang;
use Location;
use Redirect;
use Setting;
use DB;
use Sentry;
use Str;
use Validator;
use View;
use Cluster;
use State;
use Site;
use Config;
use Departments;
use League\Csv\Reader;
use Chumper\Datatable\Facades\Datatable;

class LocationsController extends AdminController
{
    protected $duplicate_locations  = array();    
    protected $emptycluster         = array();    
    protected $emptystate           = array();    
    /**
     * Show a list of all the locations.
     *
     * @return View
     */

    public function getIndex()
    {
        // Grab all the locations
        $locations = Location::orderBy('created_at', 'DESC')->paginate(15);

        // Show the page
        return View::make('backend/locations/index', compact('locations'));
    }


    /**
     * Location create.
     *
     * @return View
     */
    public function getCreate($cluster_id = null, $state_id = null, $site_id = null)
    {
        // Show the page
        $location_options = array('0' => 'Select Location') + Location::lists('name', 'id');
        $cluster_options = array('0' => 'Select Cluster') + Cluster::lists('name', 'id');
        $state_options = array('0' => 'Select State') + State::lists('name', 'id');
        $site_options = array('0' => 'Select Site') + Site::lists('name', 'id');
        $view =  View::make('backend/locations/edit')->with('location_options',$location_options)->with('cluster_options',$cluster_options)->with('state_options',$state_options)->with('location',new Location)->with('site_options',$site_options);

        if (!is_null($cluster_id)) {
            $selected_cluster = Cluster::find($cluster_id);
            $view->with('selected_cluster',$selected_cluster);
        }  

        if (!is_null($state_id)) {
            $selected_state = Cluster::find($state_id);
            $view->with('selected_state',$selected_state);
        } 

        if (!is_null($site_id)) {
            $selected_site = Cluster::find($site_id);
            $view->with('selected_site',$selected_site);
        } 
        return $view;      
    }


    /**
     * Location create form processing.
     *
     * @return Redirect
     */
    public function postCreate()
    {

        // get the POST data
        $new = Input::all();

        // create a new location instance
        $location = new Location();

        // attempt validation
        if ($location->validate($new)) {

            // Save the location data
            $location->name            	= e(Input::get('name'));
            $location->address			= e(Input::get('address'));
            $location->address2			= e(Input::get('address2'));
            $location->city    			= e(Input::get('city'));
            $location->state    		= e(Input::get('state'));
            //$location->country          = e(Input::get('country'));
            $location->country          = 'my';
            $location->code    		    = e(Input::get('code'));
            $location->zip              = e(Input::get('zip'));
            $location->is_ktw           = e(Input::get('is_ktw'));
            $location->is_pi1m          = e(Input::get('is_pi1m'));
            $location->is_fiber         = e(Input::get('is_fiber'));
            $location->cluster_id       = e(Input::get('cluster_id'));
            $location->state_id         = e(Input::get('state_id'));
            //$location->site_id    	    = e(Input::get('site_id'));
            $location->user_id          = Sentry::getId();

            // Was the asset created?
            if($location->save()) {
                
                if ($location->is_fiber)
                    $type = Departments::find(3)->first()->code;

                if ($location->is_ktw)
                    $type = Departments::find(2)->first()->code;

                if ($location->is_pi1m)
                    $type = Departments::find(1)->first()->code;

                $site = new Site();
                $site->name         = $location->name;
                $site->code         = $location->code . '_00';
                $site->userid       = $location->user_id;
                //$site->created_at   = ;
                $site->location_id  = $location->id;
                $site->site_type    = $type;

                $site->save();

                // Redirect to the new location  page
                return Redirect::to("admin/settings/locations")->with('success', Lang::get('admin/locations/message.create.success'));
            }
        } else {
            // failure
            $errors = $location->errors();
            return Redirect::back()->withInput()->withErrors($errors);
        }

        // Redirect to the location create page
        return Redirect::to('admin/settings/locations/create')->with('error', Lang::get('admin/locations/message.create.error'));

    }


    /**
     * Location update.
     *
     * @param  int  $locationId
     * @return View
     */
    public function getEdit($locationId = null)
    {
        // Check if the location exists
        if (is_null($location = Location::find($locationId))) {
            // Redirect to the blogs management page
            return Redirect::to('admin/settings/locations')->with('error', Lang::get('admin/locations/message.does_not_exist'));
        }

        // Show the page
        //$location_options = array('' => 'Top Level') + Location::lists('name', 'id');
        $cluster_options = array('0' => 'Select Cluster') + Cluster::lists('name', 'id');
        $state_options = array('0' => 'Select State') + State::lists('name', 'id');
        $location_options = array('' => 'Select Location') + DB::table('locations')->where('id', '!=', $locationId)->lists('name', 'id');
        $site_options = array('0' => 'Select Site') + Site::lists('name', 'id');
        return View::make('backend/locations/edit', compact('location'))->with('location_options',$location_options)->with('cluster_options',$cluster_options)->with('state_options',$state_options)->with('site_options',$site_options);
    }


    /**
     * Location update form processing page.
     *
     * @param  int  $locationId
     * @return Redirect
     */
    public function postEdit($locationId = null)
    {
        // Check if the location exists
        if (is_null($location = Location::find($locationId))) {
            // Redirect to the blogs management page
            return Redirect::to('admin/settings/locations')->with('error', Lang::get('admin/locations/message.does_not_exist'));
        }

        //attempt to validate
        $validator = Validator::make(Input::all(), $location->validationRules($locationId));

        if ($validator->fails())
        {
            // The given data did not pass validation            
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        // attempt validation
        else {

            // Update the location data
            $location->name            	= e(Input::get('name'));
            $location->address			= e(Input::get('address'));
            $location->address2			= e(Input::get('address2'));
            $location->city    			= e(Input::get('city'));
            $location->state            = e(Input::get('state'));
            $location->code    		    = e(Input::get('code'));
            $location->country    		= e(Input::get('country'));
            $location->is_ktw          = e(Input::get('is_ktw'));
            $location->is_pi1m          = e(Input::get('is_pi1m'));
            $location->is_fiber         = e(Input::get('is_fiber')); 
            $location->cluster_id       = e(Input::get('cluster_id'));
            $location->state_id         = e(Input::get('state_id'));                       
            //$location->site_id         = e(Input::get('site_id'));                       
            $location->zip    		= e(Input::get('zip'));

            // Was the asset created?
            if($location->save()) {
                // Redirect to the saved location page
                return Redirect::to("admin/settings/locations/")->with('success', Lang::get('admin/locations/message.update.success'));
            }
        } 

        // Redirect to the location management page
        return Redirect::to("admin/settings/locations/$locationId/edit")->with('error', Lang::get('admin/locations/message.update.error'));

    }

    /**
     * Delete the given location.
     *
     * @param  int  $locationId
     * @return Redirect
     */
    public function getDelete($locationId)
    {
        // Check if the location exists
        if (is_null($location = Location::find($locationId))) {
            // Redirect to the blogs management page
            return Redirect::to('admin/settings/locations')->with('error', Lang::get('admin/locations/message.not_found'));
        }


        if ($location->has_users() > 0) {

            // Redirect to the asset management page
            return Redirect::to('admin/settings/locations')->with('error', Lang::get('admin/locations/message.assoc_users'));
        } else {

            $location->delete();

            // Redirect to the locations management page
            return Redirect::to('admin/settings/locations')->with('success', Lang::get('admin/locations/message.delete.success'));
        }



    }

public function getImportLocation()
    {
        // Get all the available groups
        //$userid = Sentry::getUser()->id;
        $groups = Sentry::getGroupProvider()->findAll();
        // Selected groups
        $selectedGroups = Input::old('groups', array());
        // Get all the available permissions
        $permissions = Config::get('permissions');
        $this->encodeAllPermissions($permissions);
        // Selected permissions
        $selectedPermissions = Input::old('permissions', array('superuser' => -1));
        $this->encodePermissions($selectedPermissions);
        // Show the page
        return View::make('backend/locations/import', compact('groups', 'selectedGroups', 'permissions', 'selectedPermissions'));
    }
    
    
    /**
     * Asset import form processing.
     *
     * @return Redirect
     */
    public function postImportLocation()
    {
        //dd(Input::file('asset_import_csv'));
        //$duplicate_serial = '';
        if (! ini_get("auto_detect_line_endings")) {
            ini_set("auto_detect_line_endings", '1');
        }
        
        $csv = Reader::createFromPath(Input::file('location_import_csv'));  
        $csv->setNewline("\r\n");
        
        if (Input::get('has_headers')==1) {
            $csv->setOffset(1); 
        }
        
        $duplicates = '';
        $emptyClusterColumns = null;  
        $emptyStateColumns = ''; 


        //echo $duplicates;  
        //dd($csv);

        $nbInsert = $csv->each(function ($row) use ($duplicates, $emptyClusterColumns) {
            
            if (array_key_exists(1, $row)) {
                
                try {   
                        $today = date('Y-m-d H:i:s');
                        //dd('try');
                        //dd($row[0]);
                        //var_dump($row);
                        //Check if this asset already exists in the system
                        $code = DB::table('locations')->where('code', $row[5])->first();
                        $cluster_check = Cluster::where('code', $row[9])->first();
                        if ($code) {                    
                            $duplicates .= $row[5];
                            //echo $duplicates;
                        }
                        // else if ($row[9] == ''){
                        //     $emptyClusterColumns .= $row[5];
                        // }                        
                        // else if ($row[10] == ''){
                        //     $emptyStateColumns .= $row[5];
                        // }
                        else if($cluster_check == null || $row[9] == ''){
                            //dd('field cluster empty');
                            $emptyClusterColumns = $row[5] . ",";
                        }                        
                         else {
                                //dd(addslashes($row[3]));
                                //Model::find($asset->model_id);
                                //$modelid = Model::where('modelno', $row[3])->firstOrFail();
                                $cluster_id = Cluster::where('code', $row[9])->first();
                                if($cluster_id)
                                    $cluster_id = $cluster_id->id;
                                else
                                    $cluster_id = '';
                                
                                $state_id = DB::table('State')->where('code', $row[10])->first();
                                if($state_id)
                                    $state_id = $state_id->id;
                                else
                                    $state_id = '';

                                    $newlocation = array(
                                        'name'                  => $row[0],
                                        'city'                  => $row[1],
                                        'state'                 => '',
                                        'country'               => 'my',
                                        'user_id'               => Sentry::getUser()->id,
                                        'created_at'            => $today,
                                        'updated_at'            => $today,
                                        'address'               => $row[2],
                                        'address2'              => $row[3],                       
                                        'zip'                   => $row[4],
                                        'code'                  => $row[5],
                                        'is_ktw'                => $row[6],
                                        'is_pi1m'               => $row[7],
                                        'is_fiber'              => $row[8],
                                        'cluster_id'            => $cluster_id,
                                        'state_id'              => $state_id,
                                        'deleted_at'            => null,
                                    );
                                        //var_dump($newlocation);
                                   //DB::table('locations')->insert($newlocation);
                                }
                                
                    
                } catch (Exception $e) {
                    echo 'Caught exception: ',  $e->getMessage(), "\n";
                }
                //echo $duplicates;
                array_push($this->duplicate_locations, $duplicates); 
                array_push($this->emptycluster, $emptyClusterColumns); 
                // array_push($this->emptystate, $emptyStateColumns); 
                return true;
            } //echo $duplicates;
        }); 
        
        //echo $duplicates;
        //dd($duplicates);
        //$duplicates = 'There is duplication';
         //var_dump($this->duplicate_locations);
        $duplicates = "Location Codes : " . implode(",", $this->duplicate_locations);
         $emptyClusterColumns = "Empty/Invalid Cluster Row : " . implode(",", $this->emptycluster);
        // $emptyStateColumns = "Empty Cluster Row : " . implode(",", $this->emptystate);
        
        echo $emptyClusterColumns;
        //return Redirect::route('locations')->with('duplicates',$duplicates)->with('success', 'Success');
        
        //->with('emptyClusterColumns',$emptyClusterColumns)->with('emptyStateColumns',$emptyStateColumns);
        
    }

}
