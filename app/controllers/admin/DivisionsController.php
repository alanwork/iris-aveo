<?php namespace Controllers\Admin;

use AdminController;
use Input;
use Lang;
use Division;
use Redirect;
use Setting;
use DB;
use Sentry;
use Str;
use Validator;
use View;
use Divisions;

class DivisionsController extends AdminController
{
    /**
     * Show a list of all the divisions.
     *
     * @return View
     */

    public function getIndex()
    {
        // Grab all the divisions
        $divisions = Divisions::orderBy('created_at', 'DESC');
        if (Input::get('Deleted')) {
            $divisions->withTrashed()->Deleted();
        }

        $divisions = $divisions->paginate(15);
        // Show the page
        return View::make('backend/divisions/index', compact('divisions'));
    }


    /**
     * Division create.
     *
     * @return View
     */
    public function getCreate()
    {
        // Show the page
         //$division_types= array('' => '', 'asset' => 'Asset', 'accessory' => 'Accessory');
        return View::make('backend/divisions/edit')->with('division',new Divisions);
    }


    /**
     * Division create form processing.
     *
     * @return Redirect
     */
    public function postCreate()
    {

        // create a new model instance
        $division = new Divisions();

        $validator = Validator::make(Input::all(), $division->rules);

        if ($validator->fails())
        {
            // The given data did not pass validation
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        else{

            // Update the division data
            $division->name                 = e(Input::get('name'));
            $division->code                 = e(Input::get('code'));
            //$division->user_id              = Sentry::getId();

            // Was the asset created?
            if($division->save()) {
                // Redirect to the new division  page
                return Redirect::to("admin/settings/divisions")->with('success', Lang::get('admin/divisions/message.create.success'));
            }
        }

        // Redirect to the division create page
        return Redirect::to('admin/settings/divisions/create')->with('error', Lang::get('admin/divisions/message.create.error'));


    }

    /**
     * Division update.
     *
     * @param  int  $divisionId
     * @return View
     */
    public function getEdit($divisionId = null)
    {
        // Check if the division exists
        if (is_null($division = Divisions::find($divisionId))) {
            // Redirect to the blogs management page
            return Redirect::to('admin/settings/divisions')->with('error', Lang::get('admin/divisions/message.does_not_exist'));
        }

        // Show the page
        //$division_options = array('' => 'Top Level') + Division::lists('name', 'id');

        $division_options = array('' => 'Top Level') + DB::table('divisions')->where('id', '!=', $divisionId)->lists('name', 'id');
        $division_types= array('' => '', 'asset' => 'Asset', 'accessory' => 'Accessory');
        
        return View::make('backend/divisions/edit', compact('division'))
        ->with('division_options',$division_options)
        ->with('division_types',$division_types);
    }


    /**
     * Division update form processing page.
     *
     * @param  int  $divisionId
     * @return Redirect
     */
    public function postEdit($divisionId = null)
    {
        // Check if the blog post exists
        if (is_null($division = Divisions::find($divisionId))) {
            // Redirect to the blogs management page
            return Redirect::to('admin/divisions')->with('error', Lang::get('admin/divisions/message.does_not_exist'));
        }


        // get the POST data
        $new = Input::all();

        // attempt validation
        $validator = Validator::make(Input::all(), $division->validationRules($divisionId));


        if ($validator->fails())
        {
            // The given data did not pass validation
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        // attempt validation
        else {

            // Update the division data
            $division->name            = e(Input::get('name'));
            $division->code            = e(Input::get('code')); 

            // Was the asset created?
            if($division->save()) {
                // Redirect to the new division page
                return Redirect::to("admin/settings/divisions")->with('success', Lang::get('admin/divisions/message.update.success'));
            }
        }

        // Redirect to the division management page
        return Redirect::to("admin/settings/divisions/$divisionID/edit")->with('error', Lang::get('admin/divisions/message.update.error'));

    }

    /**
     * Delete the given division.
     *
     * @param  int  $divisionId
     * @return Redirect
     */
    public function getDelete($divisionId)
    {
        // Check if the division exists
        if (is_null($division = Divisions::find($divisionId))) {
            // Redirect to the blogs management page
            return Redirect::to('admin/settings/divisions')->with('error', Lang::get('admin/divisions/message.not_found'));
        }


        // if ($division->has_department() > 0) {

        //     // Redirect to the asset management page
        //     return Redirect::to('admin/settings/divisions')->with('error', Lang::get('admin/divisions/message.assoc_users'));
        // } else {

            $division->delete();

            // Redirect to the locations management page
            return Redirect::to('admin/settings/divisions')->with('success', Lang::get('admin/divisions/message.delete.success'));
       // }


    }



    /**
    *  Get the asset information to present to the division view page
    *
    * @param  int  $assetId
    * @return View
    **/
    public function getView($divisionID = null)
    {
        $division = Division::find($divisionID);

        if (isset($division->id)) {
                return View::make('backend/divisions/view', compact('division'));
        } else {
            // Prepare the error message
            $error = Lang::get('admin/divisions/message.does_not_exist', compact('id'));

            // Redirect to the user management page
            return Redirect::route('divisions')->with('error', $error);
        }


    }

    public function getRestore($divisionId = null)
    {

        // Get user information
        $division = Divisions::withTrashed()->find($divisionId);

         if (isset($division->id)) {

            // Restore the model
            $division->restore();

            // Prepare the success message
            $success = Lang::get('admin/divisions/message.restore.success');

            // Redirect back
            return Redirect::back()->with('success', $success);

         } else {
             return Redirect::back()->with('error', Lang::get('admin/divisions/message.not_found'));
         }

    }


}
