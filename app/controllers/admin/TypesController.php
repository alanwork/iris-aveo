<?php namespace Controllers\Admin;

use AdminController;
use Input;
use Lang;
use Redirect;
use Setting;
use DB;
use Sentry;
use Str;
use Validator;
use View;
use Types;

class TypesController extends AdminController
{
    /**
     * Show a list of all the types.
     *
     * @return View
     */

    public function getIndex()
    {
        // Grab all the types
        $types = Types::orderBy('created_at', 'DESC')->get();

        // Show the page
        return View::make('backend/types/index', compact('types'));
    }


    /**
     * Division create.
     *
     * @return View
     */
    public function getCreate()
    {
        // Show the page
         //$division_types= array('' => '', 'asset' => 'Asset', 'accessory' => 'Accessory');
        return View::make('backend/types/edit')->with('type',new Types);
    }


    /**
     * Division create form processing.
     *
     * @return Redirect
     */
    public function postCreate()
    {

        // create a new model instance
        $type = new Types();

        $validator = Validator::make(Input::all(), $type->rules);

        if ($validator->fails())
        {
            // The given data did not pass validation
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        else{

            // Update the type data
            $type->name                 = e(Input::get('name'));
            $type->code                 = e(Input::get('code'));
            //$type->user_id              = Sentry::getId();

            // Was the asset created?
            if($type->save()) {
                // Redirect to the new type  page
                return Redirect::to("admin/settings/types")->with('success', Lang::get('admin/types/message.create.success'));
            }
        }

        // Redirect to the type create page
        return Redirect::to('admin/settings/types/create')->with('error', Lang::get('admin/types/message.create.error'));


    }

    /**
     * Division update.
     *
     * @param  int  $typeId
     * @return View
     */
    public function getEdit($typeId = null)
    {
        // Check if the type exists
        if (is_null($type = Types::find($typeId))) {
            // Redirect to the blogs management page
            return Redirect::to('admin/settings/types')->with('error', Lang::get('admin/types/message.does_not_exist'));
        }

        // Show the page
        //$division_options = array('' => 'Top Level') + Division::lists('name', 'id');

        $division_options = array('' => 'Top Level') + DB::table('types')->where('id', '!=', $typeId)->lists('name', 'id');
        $division_types= array('' => '', 'asset' => 'Asset', 'accessory' => 'Accessory');
        
        return View::make('backend/types/edit', compact('type'))
        ->with('division_options',$division_options)
        ->with('division_types',$division_types);
    }


    /**
     * Division update form processing page.
     *
     * @param  int  $typeId
     * @return Redirect
     */
    public function postEdit($typeId = null)
    {
        // Check if the blog post exists
        if (is_null($type = Types::find($typeId))) {
            // Redirect to the blogs management page
            return Redirect::to('admin/types')->with('error', Lang::get('admin/types/message.does_not_exist'));
        }


        // get the POST data
        $new = Input::all();

        // attempt validation
        $validator = Validator::make(Input::all(), $type->validationRules($typeId));


        if ($validator->fails())
        {
            // The given data did not pass validation
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        // attempt validation
        else {

            // Update the type data
            $type->name            = e(Input::get('name'));
            $type->code            = e(Input::get('code')); 

            // Was the asset created?
            if($type->save()) {
                // Redirect to the new type page
                return Redirect::to("admin/settings/types")->with('success', Lang::get('admin/types/message.update.success'));
            }
        }

        // Redirect to the type management page
        return Redirect::to("admin/settings/types/$typeID/edit")->with('error', Lang::get('admin/types/message.update.error'));

    }

    /**
     * Delete the given type.
     *
     * @param  int  $typeId
     * @return Redirect
     */
    public function getDelete($typeId)
    {
        // Check if the type exists
        if (is_null($type = Types::find($typeId))) {
            // Redirect to the blogs management page
            return Redirect::to('admin/settings/types')->with('error', Lang::get('admin/types/message.not_found'));
        }


        // if ($type->has_department() > 0) {

        //     // Redirect to the asset management page
        //     return Redirect::to('admin/settings/types')->with('error', Lang::get('admin/types/message.assoc_users'));
        // } else {

            $type->delete();

            // Redirect to the locations management page
            return Redirect::to('admin/settings/types')->with('success', Lang::get('admin/types/message.delete.success'));
       // }


    }



    /**
    *  Get the asset information to present to the type view page
    *
    * @param  int  $assetId
    * @return View
    **/
    public function getView($typeID = null)
    {
        $type = Division::find($typeID);

        if (isset($type->id)) {
                return View::make('backend/types/view', compact('type'));
        } else {
            // Prepare the error message
            $error = Lang::get('admin/types/message.does_not_exist', compact('id'));

            // Redirect to the user management page
            return Redirect::route('types')->with('error', $error);
        }


    }




}
