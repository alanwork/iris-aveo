<?php namespace Controllers\Admin;

use AdminController;
use Input;
use Lang;
use Asset;
use Supplier;
use Statuslabel;
use User;
use Setting;
use Redirect;
use DB;
use Actionlog;
use Model;
use Depreciation;
use Sentry;
use Str;
use Validator;
use View;
use Response;
use Config;
use Location;
use Log;
use DNS2D;
use Mail;
use Departments;
use Category;
use Divisions;
use League\Csv\Reader;
use Chumper\Datatable\Facades\Datatable;
use Site;
use Request;

class AssetsController extends AdminController
{
    protected $qrCodeDimensions = array( 'height' => 3.5, 'width' => 3.5);
    protected $duplicate_serial = array();
    protected $invalid_model = array();
    protected $invalid_supplier = array();
    protected $invalid_location = array();
    protected $invalid_department = array();

    /**
     * Show a list of all the assets.
     *
     * @return View
     */

    public function getIndex()
    {
        // Grab all the assets
    //$user = Sentry::getUser()->id;
        //dd(Sentry::getUser());
    //$statuslabel_list = [];
    $statuslabel_list = array('0' => 'All') + Statuslabel::orderBy('name', 'asc')->lists('name', 'id');
    
    //dd($statuslabel_list);
    if (Sentry::getUser()){
    if (Sentry::getUser()->hasAnyAccess(array('admin', 'finance'))) {
    		$assets = Asset::with('model','assigneduser','assetstatus','defaultLoc','assetlog','department')->Hardware();
            // Filter results
            if (Input::get('Pending')) {
            	$assets->Pending();
            } elseif (Input::get('RTD')) {
            	$assets->RTD();
            } elseif (Input::get('Undeployable')) {
            	$assets->Undeployable();
            } elseif (Input::get('Archived')) {
            	$assets->Archived();
            } elseif (Input::get('Requestable')) {
            	$assets->RequestableAssets();
            } elseif (Input::get('Deployed')) {
            	$assets->Deployed();
            } 
            elseif (Input::get('Dismissed')) {
                $assets->Dismissed();
            }
            elseif (Input::get('Deleted')) {
            	$assets->withTrashed()->Deleted();
            }

            $assets = $assets->orderBy('created_at', 'DESC')->paginate(15);
            //dd($assets);

            // Paginate the users
            /**$assets = $assets->paginate(Setting::getSettings()->per_page)
                ->appends(array(
                    'Pending' => Input::get('Pending'),
                    'RTD' => Input::get('RTD'),
                    'Undeployable' => Input::get('Undeployable'),
                    'Deployed' => Input::get('Deployed'),
                ));
            **/

            return View::make('backend/hardware/index', compact('assets'))->with('querySearch', '')->with('statuslabel_list',$statuslabel_list)->with('selectedstatusid', '');
        }
    else
        {
        // User has access to the given permission
        return Redirect::route('department-assets');
        }

    }
    else{
        return Redirect::route('signin');    
    }
    }

public function postIndex()
    {
        // Grab all the assets
    //$user = Sentry::getUser()->id;

    
    if (Sentry::getUser()){
    if (Sentry::getUser()->hasAnyAccess(array('admin', 'finance'))) {
            $assets = Asset::with('model','assigneduser','assetstatus','defaultLoc','assetlog','department')->Hardware();
            // Filter results
            if (Input::get('Pending')) {
                $assets->Pending();
            } elseif (Input::get('RTD')) {
                $assets->RTD();
            } elseif (Input::get('Undeployable')) {
                $assets->Undeployable();
            } elseif (Input::get('Archived')) {
                $assets->Archived();
            } elseif (Input::get('Requestable')) {
                $assets->RequestableAssets();
            } elseif (Input::get('Deployed')) {
                $assets->Deployed();
            } 
            elseif (Input::get('Dismissed')) {
                $assets->Dismissed();
            }
            elseif (Input::get('Deleted')) {
                $assets->withTrashed()->Deleted();
            }

            $assets = $assets->orderBy('created_at', 'DESC')->paginate(15);
            //dd($assets);

            // Paginate the users
            /**$assets = $assets->paginate(Setting::getSettings()->per_page)
                ->appends(array(
                    'Pending' => Input::get('Pending'),
                    'RTD' => Input::get('RTD'),
                    'Undeployable' => Input::get('Undeployable'),
                    'Deployed' => Input::get('Deployed'),
                ));
            **/

            return View::make('backend/hardware/index', compact('assets'));
        }
    else
        {
        // User has access to the given permission
        return Redirect::route('department-assets');
        }

    }
    else{
        return Redirect::route('signin');    
    }
    }

    /**
     * Asset create.
     *
     * @param null $model_id
     *
     * @return View
     */
    public function getCreate($model_id = null, $dept_id = null)
    {

        // Grab the dropdown list of models
        //$model_list = array('' => 'Select a Model') + Model::orderBy('name', 'asc')->lists('name'.' '. 'modelno', 'id');

        $model_list = array('' => 'Select a Type') + DB::table('models')
        //->select(DB::raw('concat(name," / ",modelno) as name, id'))->orderBy('name', 'asc')
        ->select(DB::raw('name, id'))->orderBy('name', 'asc')
        ->orderBy('modelno', 'asc')
        ->whereNull('deleted_at')
        ->lists('name', 'id');

        $dept_list = array('' => 'Select a Department') + DB::table('departments')
        ->select(DB::raw('concat(name," - ",code) as name, id'))->orderBy('name', 'asc')
        ->orderBy('id', 'asc')
        ->whereNull('deleted_at')
        ->lists('name', 'id');

        $cat_list = array('' => 'Select a Category') + DB::table('categories')
        ->select(DB::raw('concat(name," - ",category_code) as name, id'))->orderBy('name', 'asc')
        ->orderBy('id', 'asc')
        ->whereNull('deleted_at')
        ->lists('name', 'id');        

        $supplier_list = array('' => 'Select a Supplier') + Supplier::orderBy('name', 'asc')->lists('name', 'id');
        $assigned_to = array('' => 'Select a User') + DB::table('users')->select(DB::raw('concat (first_name," ",last_name) as full_name, id'))->whereNull('deleted_at')->lists('full_name', 'id');
        $location_list = array('' => 'Select a Location') + Location::orderBy('name', 'asc')->lists('name', 'id');


        // Grab the dropdown list of status
        $statuslabel_list = Statuslabel::orderBy('name', 'asc')->lists('name', 'id');

        $view = View::make('backend/hardware/edit');
        $view->with('supplier_list',$supplier_list);
        $view->with('model_list',$model_list);
        $view->with('statuslabel_list',$statuslabel_list);
        $view->with('assigned_to',$assigned_to);
        $view->with('location_list',$location_list);
        $view->with('dept_list',$dept_list);
        $view->with('cat_list',$cat_list);
        $view->with('asset',new Asset);

        if (!is_null($model_id)) {
            $selected_model = Model::find($model_id);
            $view->with('selected_model',$selected_model);
        }        

        if (!is_null($dept_id)) {
            $selected_dept = Departments::find($dept_id);
            $view->with('selected_dept',$selected_dept);
        }

        return $view;
    }

    /**
     * Asset create form processing.
     *
     * @return Redirect
     */
    public function postCreate()
    {
        // create a new model instance
        $asset = new Asset();

        //attempt to validate
        $validator = Validator::make(Input::all(), $asset->validationRules());

        if ($validator->fails())
        {
            // The given data did not pass validation
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        //attempt validation
        else {

            if ( e(Input::get('status_id')) == '') {
                $asset->status_id =  NULL;
            } else {
                $asset->status_id = e(Input::get('status_id'));
            }

            if (e(Input::get('warranty_months')) == '') {
                $asset->warranty_months =  NULL;
            } else {
                $asset->warranty_months        = e(Input::get('warranty_months'));
            }

            if (e(Input::get('purchase_cost')) == '') {
                $asset->purchase_cost =  NULL;
            } else {
                $asset->purchase_cost = ParseFloat(e(Input::get('purchase_cost')));
            }

            if (e(Input::get('purchase_date')) == '') {
                $asset->purchase_date =  NULL;
            } else {
                $asset->purchase_date        = e(Input::get('purchase_date'));
            }

            if (e(Input::get('assigned_to')) == '') {
                $asset->assigned_to =  NULL;
            } else {
                $asset->assigned_to        = e(Input::get('assigned_to'));
            }

            if (e(Input::get('supplier_id')) == '') {
                $asset->supplier_id =  0;
            } else {
                $asset->supplier_id        = e(Input::get('supplier_id'));
            }

            if (e(Input::get('requestable')) == '') {
                $asset->requestable =  0;
            } else {
                $asset->requestable        = e(Input::get('requestable'));
            }

            if (e(Input::get('rtd_location_id')) == '') {
                $asset->rtd_location_id = NULL;
            } else {
                $asset->rtd_location_id     = e(Input::get('rtd_location_id'));
            }



            // Save the asset data
            $asset->name            		= e(Input::get('name'));
            $asset->serial            		= e(Input::get('serial'));
            $asset->model_id           		= e(Input::get('model_id'));
            $asset->order_number            = e(Input::get('order_number'));
            $asset->notes            		= e(Input::get('notes'));
            //$asset->asset_tag            	= e(Input::get('asset_tag'));
            $asset->mac_address             = e(Input::get('mac_address'));
            $asset->dept_id        			= e(Input::get('dept_id'));
            $asset->user_id          		= Sentry::getUser()->id;
            $asset->archived          			= '0';
            $asset->physical            		= '1';
            $asset->depreciate          		= '0';
            $asset->status_id = 2;

            //for Asset Tagging Autogenerate purpose
             // $deptModel = Departments::find($asset->dept_id);

             // $catModel  = Model::find($asset->model_id);
             // $catCode  = $catModel->category->category_code;

             // $divModel  = Divisions::find($asset->dept_id);

             // //dd($catModel);
             // $assetTag = $divModel->code . "-" .$deptModel->code . "-" . $catCode;

             // $assetCount = Asset::where('asset_tag', 'like', $assetTag . '%')->count();
             // $assetCount += 1;

             // //dd($assetTag . "\n" . $assetCount);
             // $assetCount = sprintf( '%04d', $assetCount );

             // $assetTag = $assetTag . "-". $assetCount;
             //dd($assetTag);
             //return View::make('backend/hardware/edit');

             //New asset tag autogenerate number
             $assetTag = $this->generateAssetTag($asset->dept_id, $asset->model_id, $asset->purchase_date);
             $asset->asset_tag              = $assetTag;
             //$asset->asset_tag              = $this->;

             //for Pi1M manager
             $pim_deptid = Sentry::getUser()->dept_id;
             if($pim_deptid == 1){
                $siteid = DB::table('sites')->where('location_id', $asset->rtd_location_id)->where('site_type', 'PIM')->pluck('id');
                //dd($siteid);
                $asset->site_id = $siteid;
             }
            // Was the asset created?
            if($asset->save()) {

            	if (Input::get('assigned_to')!='') {
					$logaction = new Actionlog();
					$logaction->asset_id = $asset->id;
					$logaction->checkedout_to = $asset->assigned_to;
					$logaction->asset_type = 'hardware';
					$logaction->user_id = Sentry::getUser()->id;
					$logaction->note = e(Input::get('note'));
					$log = $logaction->logaction('checkout');
				}

                // Redirect to the asset listing page
                return Redirect::to("hardware")->with('success', Lang::get('admin/hardware/message.create.success'));
           }
       }

        // Redirect to the asset create page with an error
        //return Redirect::to('assets/create')->with('error', Lang::get('admin/hardware/message.create.error'));


    }

    /**
     * Asset update.
     *
     * @param  int  $assetId
     * @return View
     */
    public function getEdit($assetId = null)
    {
        // Check if the asset exists
        if (is_null($asset = Asset::find($assetId))) {
            // Redirect to the asset management page
            return Redirect::to('hardware')->with('error', Lang::get('admin/hardware/message.does_not_exist'));
        }


        // Grab the dropdown list of models
		$model_list = array('' => '') + DB::table('models')
		->select(DB::raw('name, id'))->orderBy('name', 'asc')
		->orderBy('modelno', 'asc')
		->lists('name', 'id');

        $dept_list = array('' => 'Select a Department') + DB::table('departments')
        ->select(DB::raw('concat(name," - ",code) as name, id'))->orderBy('name', 'asc')
        ->orderBy('id', 'asc')
        ->whereNull('deleted_at')
        ->lists('name', 'id');

        $supplier_list = array('' => '') + Supplier::orderBy('name', 'asc')->lists('name', 'id');
        $location_list = array('' => '') + Location::orderBy('name', 'asc')->lists('name', 'id');

        // Grab the dropdown list of status
        $statuslabel_list = Statuslabel::orderBy('name', 'asc')->lists('name', 'id');

        return View::make('backend/hardware/edit', compact('asset'))->with('model_list',$model_list)->with('supplier_list',$supplier_list)->with('location_list',$location_list)->with('statuslabel_list',$statuslabel_list)->with('dept_list', $dept_list);
    }


    /**
     * Asset update form processing page.
     *
     * @param  int  $assetId
     * @return Redirect
     */
    public function postEdit($assetId = null)
    {
        // Check if the asset exists
        if (is_null($asset = Asset::find($assetId))) {
            // Redirect to the asset management page with error
            return Redirect::to('hardware')->with('error', Lang::get('admin/hardware/message.does_not_exist'));
        }

        //attempt to validate
        $validator = Validator::make(Input::all(), $asset->validationRules($assetId));

        if ($validator->fails())
        {
            // The given data did not pass validation
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        // attempt validation
        else {


            if ( e(Input::get('status_id')) == '' ) {
                $asset->status_id =  NULL;
            } else {
                $asset->status_id = e(Input::get('status_id'));
            }

            if (e(Input::get('warranty_months')) == '') {
                $asset->warranty_months =  NULL;
            } else {
                $asset->warranty_months        = e(Input::get('warranty_months'));
            }

            if (e(Input::get('purchase_cost')) == '') {
                $asset->purchase_cost =  NULL;
            } else {
                $asset->purchase_cost = ParseFloat(e(Input::get('purchase_cost')));
            }

            if (e(Input::get('purchase_date')) == '') {
                $asset->purchase_date =  NULL;
            } else {
                $asset->purchase_date        = e(Input::get('purchase_date'));
            }

            if (e(Input::get('supplier_id')) == '') {
                $asset->supplier_id =  NULL;
            } else {
                $asset->supplier_id        = e(Input::get('supplier_id'));
            }

            if (e(Input::get('requestable')) == '') {
                $asset->requestable =  0;
            } else {
                $asset->requestable        = e(Input::get('requestable'));
            }

            if (e(Input::get('rtd_location_id')) == '') {
                $asset->rtd_location_id = 0;
            } else {
                $asset->rtd_location_id     = e(Input::get('rtd_location_id'));
            }

            // Update the asset data
            $asset->name            		= e(Input::get('name'));
            $asset->serial            		= e(Input::get('serial'));
            $asset->model_id           		= e(Input::get('model_id'));
            $asset->order_number            = e(Input::get('order_number'));
            //$asset->asset_tag           	= e(Input::get('asset_tag'));
            $asset->notes            		= e(Input::get('notes'));
            $asset->physical            	= '1';
            $asset->mac_address             = e(Input::get('mac_address'));
            $asset->dept_id      			= e(Input::get('dept_id'));

            // Was the asset updated?
            if($asset->save()) {
                // Redirect to the new asset page
                return Redirect::to("hardware/$assetId/view")->with('success', Lang::get('admin/hardware/message.update.success'));
            }
            else
            {
                 return Redirect::to('hardware')->with('error', Lang::get('admin/hardware/message.does_not_exist'));
             }
        }


        // Redirect to the asset management page with error
        return Redirect::to("hardware/$assetId/edit")->with('error', Lang::get('admin/hardware/message.update.error'));

    }

    /**
     * Delete the given asset.
     *
     * @param  int  $assetId
     * @return Redirect
     */
    public function getDelete($assetId)
    {
        // Check if the asset exists
        if (is_null($asset = Asset::find($assetId))) {
            // Redirect to the asset management page with error
            return Redirect::to('hardware')->with('error', Lang::get('admin/hardware/message.not_found'));
        }

        if (isset($asset->assigneduser->id) && ($asset->assigneduser->id!=0)) {
            // Redirect to the asset management page
            return Redirect::to('hardware')->with('error', Lang::get('admin/hardware/message.assoc_users'));
        } else {
            // Delete the asset

            DB::table('assets')
            ->where('id', $asset->id)
            ->update(array('assigned_to' => NULL));


            $asset->delete();

            // Redirect to the asset management page
            return Redirect::to('hardware')->with('success', Lang::get('admin/hardware/message.delete.success'));
        }



    }

    /**
    * Check out the asset to a person
    **/
    public function getCheckout($assetId)
    {
        // Check if the asset exists
        if (is_null($asset = Asset::find($assetId))) {
            // Redirect to the asset management page with error
            return Redirect::to('hardware')->with('error', Lang::get('admin/hardware/message.not_found'));
        }

        // Get the dropdown of users and then pass it to the checkout view
        $users_list = array('' => 'Select a User') + DB::table('users')->select(DB::raw('concat(last_name,", ",first_name) as full_name, id'))->whereNull('deleted_at')->orderBy('last_name', 'asc')->orderBy('first_name', 'asc')->lists('full_name', 'id');

        return View::make('backend/hardware/checkout', compact('asset'))->with('users_list',$users_list);

    }

    /**
    * Check out the asset to a person
    **/
    public function postCheckout($assetId)
    {
        // Check if the asset exists
        if (is_null($asset = Asset::find($assetId))) {
            // Redirect to the asset management page with error
            return Redirect::to('hardware')->with('error', Lang::get('admin/hardware/message.not_found'));
        }

        $assigned_to = e(Input::get('assigned_to'));



        // Declare the rules for the form validation
        $rules = array(
            'assigned_to'   => 'required|min:1',
            'note'   => 'alpha_space',
        );

        // Create a new validator instance from our validation rules
        $validator = Validator::make(Input::all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::back()->withInput()->withErrors($validator);
        }


        // Check if the user exists
        if (is_null($user = User::find($assigned_to))) {
            // Redirect to the asset management page with error
            return Redirect::to('hardware')->with('error', Lang::get('admin/hardware/message.user_does_not_exist'));
        }

        // Update the asset data
        $asset->assigned_to            		= e(Input::get('assigned_to'));

        // Was the asset updated?
        if($asset->save()) {
            $logaction = new Actionlog();

            if (Input::has('checkout_at')) {
            	if (Input::get('checkout_at')!= date("Y-m-d")){
					$logaction->created_at = e(Input::get('checkout_at')).' 00:00:00';
				}
        	}



            $logaction->asset_id = $asset->id;
            $logaction->checkedout_to = $asset->assigned_to;
            $logaction->asset_type = 'hardware';
            $logaction->location_id = $user->location_id;
            $logaction->user_id = Sentry::getUser()->id;
            $logaction->note = e(Input::get('note'));
            $logaction->delivery_order = e(Input::get('delivery'));
            $log = $logaction->logaction('checkout');

            $data['log_id'] = $logaction->id;
            $data['eula'] = $asset->getEula();
            $data['first_name'] = $user->first_name;
            $data['item_name'] = $asset->name;
            $data['require_acceptance'] = $asset->requireAcceptance();


            if (($asset->requireAcceptance()=='1')  || ($asset->getEula())) {

	            Mail::send('emails.accept-asset', $data, function ($m) use ($user) {
	                $m->to($user->email, $user->first_name . ' ' . $user->last_name);
	                $m->subject('Confirm asset delivery');
	            });
            }

            // Redirect to the new asset page
            return Redirect::to("hardware")->with('success', Lang::get('admin/hardware/message.checkout.success'));
        }

        // Redirect to the asset management page with error
        return Redirect::to("hardware/$assetId/checkout")->with('error', Lang::get('admin/hardware/message.checkout.error'));
    }


    /**
    * Check the asset back into inventory
    *
    * @param  int  $assetId
    * @return View
    **/
    public function getCheckin($assetId, $backto = null)
    {
        // Check if the asset exists
        if (is_null($asset = Asset::find($assetId))) {
            // Redirect to the asset management page with error
            return Redirect::to('hardware')->with('error', Lang::get('admin/hardware/message.not_found'));
        }

        return View::make('backend/hardware/checkin', compact('asset'))->with('backto', $backto);
    }


    /**
    * Check in the item so that it can be checked out again to someone else
    *
    * @param  int  $assetId
    * @return View
    **/
    public function postCheckin($assetId = null, $backto = null)
    {
        // Check if the asset exists
        if (is_null($asset = Asset::find($assetId))) {
            // Redirect to the asset management page with error
            return Redirect::to('hardware')->with('error', Lang::get('admin/hardware/message.not_found'));
        }

        if (!is_null($asset->assigned_to)) {
            $user = User::find($asset->assigned_to);
        }

        // This is just used for the redirect
        $return_to = $asset->assigned_to;

        $logaction = new Actionlog();
        $logaction->checkedout_to = $asset->assigned_to;

        // Update the asset data to null, since it's being checked in
        $asset->assigned_to            		= NULL;


        // Was the asset updated?
        if($asset->save()) {

        	 if (Input::has('checkin_at')) {

        	 	if (!strtotime(Input::get('checkin_at'))) {
					$logaction->created_at = date("Y-m-d h:i:s");
        	 	} elseif (Input::get('checkin_at')!= date("Y-m-d")) {
					$logaction->created_at = e(Input::get('checkin_at')).' 00:00:00';
				}
        	}

            $logaction->asset_id = $asset->id;
            $logaction->location_id = NULL;
            $logaction->asset_type = 'hardware';
            $logaction->note = e(Input::get('note'));
            $logaction->user_id = Sentry::getUser()->id;
            $log = $logaction->logaction('checkin from');


			if ($backto=='user') {
				return Redirect::to("admin/users/".$return_to.'/view')->with('success', Lang::get('admin/hardware/message.checkin.success'));
			} else {
				return Redirect::to("hardware")->with('success', Lang::get('admin/hardware/message.checkin.success'));
			}

        }

        // Redirect to the asset management page with error
        return Redirect::to("hardware")->with('error', Lang::get('admin/hardware/message.checkin.error'));
    }


    /**
    *  Get the asset information to present to the asset view page
    *
    * @param  int  $assetId
    * @return View
    **/
    public function getView($assetId = null)
    {
        $asset = Asset::withTrashed()->find($assetId);

        if (isset($asset->id)) {

            $settings = Setting::getSettings();

            $qr_code = (object) array(
                'display' => $settings->qr_code == '1',
                'url' => route('qr_code/hardware', $asset->id)
            );

            return View::make('backend/hardware/view', compact('asset', 'qr_code'));
        } else {
            // Prepare the error message
            $error = Lang::get('admin/hardware/message.does_not_exist', compact('id'));

            // Redirect to the user management page
            return Redirect::route('hardware')->with('error', $error);
        }

    }

    /**
    *  Get the QR code representing the asset
    *
    * @param  int  $assetId
    * @return View
    **/
    public function getQrCode($assetId = null)
    {
        $settings = Setting::getSettings();

        if ($settings->qr_code == '1') {
            $asset = Asset::find($assetId);

            if (isset($asset->id,$asset->asset_tag)) {

                //$content = DNS2D::getBarcodePNG(route('view/hardware', $asset->id), $settings->barcode_type,
                //    $this->qrCodeDimensions['height'],$this->qrCodeDimensions['width']);
        

                //$content = DNS2D::getBarcodePNG("Asset Tag = ". $asset->asset_tag, $settings->barcode_type,
                //    $this->qrCodeDimensions['height'],$this->qrCodeDimensions['width']);

                $content = DNS2D::getBarcodePNG($asset->asset_tag, $settings->barcode_type,
                    $this->qrCodeDimensions['height'],$this->qrCodeDimensions['width']);

                $img = imagecreatefromstring(base64_decode($content));
                imagepng($img);
                imagedestroy($img);

                $content_disposition = sprintf('attachment;filename=qr_code_%s.png', preg_replace('/\W/', '', $asset->asset_tag));
                $response = Response::make($content, 200);
                $response->header('Content-Type', 'image/png');
                $response->header('Content-Disposition', $content_disposition);
                return $response;                 
            }
           
        }

        $response = Response::make('', 404);
        return $response;
    }

    /**
     * Asset update.
     *
     * @param  int  $assetId
     * @return View
     */
    public function getClone($assetId = null)
    {
        // Check if the asset exists
        if (is_null($asset_to_clone = Asset::find($assetId))) {
            // Redirect to the asset management page
            return Redirect::to('hardware')->with('error', Lang::get('admin/hardware/message.does_not_exist'));
        }

        // Grab the dropdown list of models
        $model_list = array('' => '') + Model::lists('name', 'id');

        // Grab the dropdown list of status
        $statuslabel_list = Statuslabel::lists('name', 'id');

        $location_list = array('' => '') + Location::lists('name', 'id');

        // get depreciation list
        $depreciation_list = array('' => '') + Depreciation::lists('name', 'id');
        $supplier_list = array('' => '') + Supplier::orderBy('name', 'asc')->lists('name', 'id');
        $assigned_to = array('' => 'Select a User') + DB::table('users')->select(DB::raw('concat (first_name," ",last_name) as full_name, id'))->whereNull('deleted_at')->lists('full_name', 'id');

        $asset = clone $asset_to_clone;
        $asset->id = null;
        $asset->asset_tag = '';
        return View::make('backend/hardware/edit')->with('supplier_list',$supplier_list)->with('model_list',$model_list)->with('statuslabel_list',$statuslabel_list)->with('assigned_to',$assigned_to)->with('asset',$asset)->with('location_list',$location_list);

    }


    public function getRestore($assetId = null)
    {

		// Get user information
		$asset = Asset::withTrashed()->find($assetId);

		 if (isset($asset->id)) {

			// Restore the user
			$asset->restore();

			// Prepare the success message
			$success = Lang::get('admin/hardware/message.restore.success');

			// Redirect to the user management page
			return Redirect::route('hardware')->with('success', $success);

		 } else {
			 return Redirect::to('hardware')->with('error', Lang::get('admin/hardware/message.not_found'));
		 }

    }


       /**
    *  Upload the file to the server
    *
    * @param  int  $assetId
    * @return View
    **/
    public function postUpload($assetID = null)
    {
        $asset = Asset::find($assetID);

		// the asset is valid
		$destinationPath = app_path().'/private_uploads';

        if (isset($asset->id)) {

        	if (Input::hasFile('assetfile')) {

				foreach(Input::file('assetfile') as $file) {

				$rules = array(
				   'assetfile' => 'required|mimes:png,gif,jpg,jpeg,doc,docx,pdf,txt|max:2000'
				);
				$validator = Validator::make(array('assetfile'=> $file), $rules);

					if($validator->passes()){

						$extension = $file->getClientOriginalExtension();
						$filename = 'hardware-'.$asset->id.'-'.str_random(8);
						$filename .= '-'.Str::slug($file->getClientOriginalName()).'.'.$extension;
						$upload_success = $file->move($destinationPath, $filename);

						//Log the deletion of seats to the log
						$logaction = new Actionlog();
						$logaction->asset_id = $asset->id;
						$logaction->asset_type = 'hardware';
						$logaction->user_id = Sentry::getUser()->id;
						$logaction->note = e(Input::get('notes'));
						$logaction->checkedout_to =  NULL;
						$logaction->created_at =  date("Y-m-d h:i:s");
						$logaction->filename =  $filename;
						$log = $logaction->logaction('uploaded');
					} else {
						 return Redirect::back()->with('error', Lang::get('admin/hardware/message.upload.invalidfiles'));
					}


				}

				if ($upload_success) {
				  	return Redirect::back()->with('success', Lang::get('admin/hardware/message.upload.success'));
				} else {
				   return Redirect::back()->with('success', Lang::get('admin/hardware/message.upload.error'));
				}

			} else {
				 return Redirect::back()->with('success', Lang::get('admin/hardware/message.upload.nofiles'));
			}





        } else {
            // Prepare the error message
            $error = Lang::get('admin/hardware/message.does_not_exist', compact('id'));

            // Redirect to the hardware management page
            return Redirect::route('hardware')->with('error', $error);
        }
    }


    /**
    *  Delete the associated file
    *
    * @param  int  $assetId
    * @return View
    **/
    public function getDeleteFile($assetID = null, $fileId = null)
    {
        $asset = Asset::find($assetID);
        $destinationPath = app_path().'/private_uploads';

		// the asset is valid
        if (isset($asset->id)) {

			$log = Actionlog::find($fileId);
			$full_filename = $destinationPath.'/'.$log->filename;
			if (file_exists($full_filename)) {
				unlink($destinationPath.'/'.$log->filename);
			}
			$log->delete();
			return Redirect::back()->with('success', Lang::get('admin/hardware/message.deletefile.success'));

        } else {
            // Prepare the error message
            $error = Lang::get('admin/hardware/message.does_not_exist', compact('id'));

            // Redirect to the hardware management page
            return Redirect::route('hardware')->with('error', $error);
        }
    }



    /**
    *  Display/download the uploaded file
    *
    * @param  int  $assetId
    * @return View
    **/
    public function displayFile($assetID = null, $fileId = null)
    {

        $asset = Asset::find($assetID);

		// the asset is valid
        if (isset($asset->id)) {
				$log = Actionlog::find($fileId);
				$file = $log->get_src();
				return Response::download($file);
        } else {
            // Prepare the error message
            $error = Lang::get('admin/hardware/message.does_not_exist', compact('id'));

            // Redirect to the hardware management page
            return Redirect::route('hardware')->with('error', $error);
        }
    }




    /**
    *  Display bulk edit screen
    *
    * @return View
    **/
    public function postBulkEdit($assets = null)
    {
        //dd($querySearch);
        $statuslabel_list = array('0' => 'All') +Statuslabel::orderBy('name', 'asc')->lists('name', 'id');

		if (Input::has('edit_asset')) {

			$assets = Input::get('edit_asset');
            $statuslabel_list = [];
			$supplier_list = array('' => '') + Supplier::orderBy('name', 'asc')->lists('name', 'id');
	        $statuslabel_list = array('0' => 'All') + Statuslabel::lists('name', 'id');
            

	        $location_list = array('' => '') + Location::lists('name', 'id');
		

		return View::make('backend/hardware/bulk')->with('assets',$assets)->with('supplier_list',$supplier_list)->with('statuslabel_list',$statuslabel_list)->with('location_list',$location_list);
        }

        else if (isset($_GET['submit'])){
            //$search = (Input::get('querySearch'));

            $search = Request::get('querySearch');
            $statusid = Request::get('statuslabel_list');
            if (Sentry::getUser()){
            if (Sentry::getUser()->hasAnyAccess(array('admin', 'finance'))) {
                    $assets = Asset::with('model','assigneduser','assetstatus','defaultLoc','assetlog','department')->Hardware();
                    // Filter results
                    if (Input::get('Pending')) {
                        $assets->Pending();
                    } elseif (Input::get('RTD')) {
                        $assets->RTD();
                    } elseif (Input::get('Undeployable')) {
                        $assets->Undeployable();
                    } elseif (Input::get('Archived')) {
                        $assets->Archived();
                    } elseif (Input::get('Requestable')) {
                        $assets->RequestableAssets();
                    } elseif (Input::get('Deployed')) {
                        $assets->Deployed();
                    } 
                    elseif (Input::get('Dismissed')) {
                        $assets->Dismissed();
                    }
                    elseif (Input::get('Deleted')) {
                        $assets->withTrashed()->Deleted();
                    }

                    $assets = $assets->orderBy('created_at', 'DESC')->where("asset_tag", "LIKE", "%$search%");

                    if($statusid != 0)
                        $assets = $assets->where("status_id", $statusid)->paginate(15);
                    else
                        $assets = $assets->paginate(15);
                    //dd($assets);

                    // Paginate the users
                    /**$assets = $assets->paginate(Setting::getSettings()->per_page)
                        ->appends(array(
                            'Pending' => Input::get('Pending'),
                            'RTD' => Input::get('RTD'),
                            'Undeployable' => Input::get('Undeployable'),
                            'Deployed' => Input::get('Deployed'),
                        ));
                    **/
                    //dd($statuslabel_list);
                    return View::make('backend/hardware/index', compact('assets'))->with('querySearch', $search)->with('statuslabel_list', $statuslabel_list)->with('selectedstatusid', $statusid);
                }
            else
                {
                // User has access to the given permission
                return Redirect::route('department-assets');
                }

            }
            else{
                return Redirect::route('signin');    
            }
        }
    }



    /**
    *  Save bulk edits
    *
    * @return View
    **/
    public function postBulkSave($assets = null)
    {

		if (Input::has('bulk_edit')) {

			$assets = Input::get('bulk_edit');

			if ( (Input::has('purchase_date')) ||  (Input::has('rtd_location_id')) ||  (Input::has('status_id')) )  {

				foreach ($assets as $key => $value) {

					$update_array = array();

					if (Input::has('purchase_date')) {
						$update_array['purchase_date'] =  e(Input::get('purchase_date'));
					}

					if (Input::has('rtd_location_id')) {
						$update_array['rtd_location_id'] = e(Input::get('rtd_location_id'));
					}

					if (Input::has('status_id')) {
						$update_array['status_id'] = e(Input::get('status_id'));
					}


					if (DB::table('assets')
		            ->where('id', $key)
		            ->update($update_array)) {

			            $logaction = new Actionlog();
			            $logaction->asset_id = $key;
			            $logaction->asset_type = 'hardware';
			            $logaction->created_at =  date("Y-m-d h:i:s");

			            if (Input::has('rtd_location_id')) {
			            	$logaction->location_id = e(Input::get('rtd_location_id'));
			            }
			            $logaction->user_id = Sentry::getUser()->id;
			            $log = $logaction->logaction('update');

		            }

				} // endforeach

				return Redirect::to("hardware")->with('success', Lang::get('admin/hardware/message.update.success'));

			// no values given, nothing to update
			} else {
				return Redirect::to("hardware")->with('info',Lang::get('admin/hardware/message.update.nothing_updated'));

			}


		} // endif

		return Redirect::to("hardware");

    }

    public function selectCategory()
    {
        return 'Abc';
    }

 /**
     * Asset import.
     *
     * @return View
     */
    public function getImportAsset()
    {
        // Get all the available groups
        //$userid = Sentry::getUser()->id;
        $groups = Sentry::getGroupProvider()->findAll();
        // Selected groups
        $selectedGroups = Input::old('groups', array());
        // Get all the available permissions
        $permissions = Config::get('permissions');
        $this->encodeAllPermissions($permissions);
        // Selected permissions
        $selectedPermissions = Input::old('permissions', array('superuser' => -1));
        $this->encodePermissions($selectedPermissions);
        // Show the page
        return View::make('backend/hardware/import', compact('groups', 'selectedGroups', 'permissions', 'selectedPermissions'));
    }
    
    
    /**
     * Asset import form processing.
     *
     * @return Redirect
     */
    public function postImportAsset()
    {
        //dd(Input::file('asset_import_csv'));
        //$duplicate_serial = '';
        if (! ini_get("auto_detect_line_endings")) {
            ini_set("auto_detect_line_endings", '1');
        }
        
        $csv = Reader::createFromPath(Input::file('asset_import_csv'));  
        $csv->setNewline("\r\n");
        
        if (Input::get('has_headers')==1) {
            $csv->setOffset(1); 
        }
        
        $duplicates = '';
        $inv_model = '';  
        $inv_location = '';  
        $inv_supp = '';  
        $inv_dept = '';  

        //echo $duplicates;  
        //dd($csv);

        $nbInsert = $csv->each(function ($row) use ($duplicates, $inv_model, $inv_location, $inv_supp, $inv_dept) {
            
            if (array_key_exists(1, $row)) {
              
                try {   
                        $today = date('Y-m-d H:i:s');
                        //dd('try');
                        //dd($row[0]);
                        //var_dump($row);
                        //Check if this asset already exists in the system
                        $serial = DB::table('assets')->where('serial', $row[2])->first();
                        $modelid = Model::where('modelno', addslashes($row[1]))->first();
                        $suppid = DB::table('suppliers')->where('code', $row[6])->first();
                        $locationid = DB::table('locations')->where('code', $row[7])->first();
                        $deptid = DB::table('departments')->where('code', $row[8])->first();



                        if($modelid)
                            $modelid = $modelid->id;
                        else
                            $modelid = '';

                        if($suppid)
                            $suppid = $suppid->id;
                        else
                            $suppid = '';

                        if($locationid)
                            $locationid = $locationid->id;
                        else
                            $locationid = ''; 
                                                   
                        if($deptid)
                            $deptid = $deptid->id;
                        else
                            $deptid = '';


                        if ($serial) {                    
                            $duplicates .= $row[2];
                            //echo $duplicates;
                        }
                        else if ($modelid == ''){
                            $inv_model = $row[1];
                        }                           
                        else if ($suppid == ''){
                            $inv_supp = $row[6];
                        }                         
                        else if ($locationid == ''){
                            $inv_location = $row[7];
                        }                        
                        else if ($deptid == ''){
                            $inv_dept = $row[8];
                        }                        
                         else {
                                if ($modelid != '' && $deptid != '' && $row[3] != ''){
                                    $dt = date_create_from_format('d/m/y', $row[3]);
                                    $purDate =  $dt->format('Y-m-d H:i:s');
                                    //var_dump($dt);
                                    //dd($dt->format('Y-m-d H:i:s'));
                                    $assetTag = $this->generateAssetTag($deptid, $modelid, $purDate);//dept_id, model_id, purchase date
                                }
                                else
                                    $assetTag = '';

                                    $newasset = array(
                                        'name'                   => $row[0],
                                        'asset_tag'              => $assetTag,
                                        'model_id'               => $modelid,
                                        'serial'                 => $row[2],
                                        'purchase_date'          => $row[3],
                                        'purchase_cost'          => $row[4],
                                        'order_number'           => $row[5],
                                        'assigned_to'            => null,
                                        'notes'                  => '',
                                        'user_id'                => Sentry::getUser()->id,
                                        'created_at'             => $today,
                                        'updated_at'             => $today,
                                        'physical'               => 1,
                                        'deleted_at'             => null,
                                        'status_id'              => 2,
                                        'archived'               => 0,
                                        'warranty_months'        => null,
                                        'depreciate'             => 0,
                                        'supplier_id'            => $suppid,
                                        'requestable'            => 0,
                                        'rtd_location_id'        => $locationid,
                                        'mac_address'            => null,
                                        'dept_id'                => $deptid,
                                        //'type_id'                => $row[24],
                           
                                    );
                                   //     var_dump($newasset);
                                    DB::table('assets')->insert($newasset);
                                }
                                
                    
                } catch (Exception $e) {
                    echo 'Caught exception: ',  $e->getMessage(), "\n";
                }
                //echo $duplicates;
                array_push($this->duplicate_serial, $duplicates); 
                array_push($this->invalid_model, $inv_model); 
                array_push($this->invalid_department, $inv_dept); 
                array_push($this->invalid_location, $inv_location); 
                array_push($this->invalid_supplier, $inv_supp); 
                return true;
            } //echo $duplicates;
        }); 
        
        //echo $duplicates;
        //dd($duplicates);
        //$duplicates = 'There is duplication';
         //var_dump($this->duplicate_serial);
        $duplicates = "Serial Numbers : " . implode(" ", $this->duplicate_serial);

        if ($this->invalid_model != '')
            $inv_model = implode(" ", $this->invalid_model);
         else
             $inv_model = '';

        if (isset($this->invalid_department))
            $inv_dept = implode(" ", $this->invalid_department);
         else
             $inv_dept = '';

        if(isset($this->invalid_location))
            $inv_location = implode(" ", $this->invalid_location);

        if(isset($this->invalid_supplier))
            $inv_supp = implode(" ", $this->invalid_supplier);
        
        //dd($inv_supp);
        return Redirect::route('hardware')
        ->with('duplicates',$duplicates)
        ->with('invalid_model',$inv_model)
        ->with('invalid_department',$inv_dept)
        ->with('invalid_location',$inv_location)
        ->with('invalid_supplier',$inv_supp)
        ->with('success', 'Success');
        
    }

    public function getQRAsset()
    {
        return View::make('backend/hardware/qr');
    }

/**
     * Asset Status Update.
     *
     * @param  int  $assetId
     * @return View
     */
    public function getStatusEdit($assetId = null)
    {
        // Check if the asset exists
        if (is_null($asset = Asset::find($assetId))) {
            // Redirect to the asset management page
            return Redirect::to('hardware')->with('error', Lang::get('admin/hardware/message.does_not_exist'));
        }

        // Grab the dropdown list of status
            $settings = Setting::getSettings();

            $qr_code = (object) array(
                'display' => $settings->qr_code == '1',
                'url' => route('qr_code/hardware', $asset->id)
            );        
        $statuslabel_list = Statuslabel::orderBy('name', 'asc')->lists('name', 'id');
        $statuslabel_admin_list = Statuslabel::orderBy('name', 'asc')->where('id', '!=', '2')->lists('name', 'id');

        return View::make('backend/hardware/status', compact('asset'))->with('statuslabel_list',$statuslabel_list)->with('statuslabel_admin_list',$statuslabel_admin_list)->with('qr_code', $qr_code);
    }


    /**
     * Asset Status Update form processing page.
     *
     * @param  int  $assetId
     * @return Redirect
     */
    public function postStatusEdit($assetId = null)
    {
        //dd(e(Input::get('_modelid')));
        // Check if the asset exists
        if (is_null($asset = Asset::find($assetId))) {
            // Redirect to the asset management page with error
            return Redirect::to('hardware')->with('error', Lang::get('admin/hardware/message.does_not_exist'));
        }

        //attempt to validate
        $validator = Validator::make(Input::all(), $asset->validationRules($assetId));
        //dd($validator);
        if ($validator->fails())
        {
            // The given data did not pass validation
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        // attempt validation
        else {


            if ( e(Input::get('status_id')) == '' ) {
                $asset->status_id =  NULL;
            } else {
                $asset->status_id = e(Input::get('status_id'));
            }

            $asset->model_id = e(Input::get('model_id'));

            $asset->serial = e(Input::get('serial'));
            //dd($asset->serial);
            // Was the asset updated?
            if($asset->save()) {
                // Redirect to the new asset page
                return Redirect::to("hardware/$assetId/view")->with('success', Lang::get('admin/hardware/message.update.success'));
            }
            else
            {
                 return Redirect::to('hardware')->with('error', Lang::get('admin/hardware/message.does_not_exist'));
             }
        }

        // Redirect to the asset management page with error
        return Redirect::to("hardware/$assetId/edit")->with('error', Lang::get('admin/hardware/message.update.error'));

    }    

   /**
     * Asset Print.
     *
     * @param 
     * @return View
     */
    public function getPrint()
    {
        // Grab the dropdown list of models
        $model_list = array('' => '') + DB::table('models')
        ->select(DB::raw('concat(name," / ",modelno) as name, id'))->orderBy('name', 'asc')
        ->orderBy('modelno', 'asc')
        ->lists('name', 'id');

        $dept_list = array('' => 'Select a Department') + DB::table('departments')
        ->select(DB::raw('concat(name," - ",code) as name, id'))->orderBy('name', 'asc')
        ->orderBy('id', 'asc')
        ->whereNull('deleted_at')
        ->lists('name', 'id');

        $supplier_list = array('' => '') + Supplier::orderBy('name', 'asc')->lists('name', 'id');
        $location_list = array('' => '') + Location::orderBy('name', 'asc')->lists('name', 'id');

        // Grab the dropdown list of status
        $statuslabel_list = array('' => '') + Statuslabel::orderBy('name', 'asc')->lists('name', 'id');

        return View::make('backend/hardware/print', compact('asset'))->with('model_list',$model_list)->with('supplier_list',$supplier_list)->with('location_list',$location_list)->with('statuslabel_list',$statuslabel_list)->with('dept_list', $dept_list);
    }


    /**
     * Asset Print form processing page.
     *
     * @param  
     * @return Redirect
     */
    public function postPrint()
    {

            $asset_list = DB::table('assets')
            ->select(DB::raw('id, asset_tag'))->orderBy('created_at', 'desc')
            ->whereNull('deleted_at');

        $asset = array();

            if ( e(Input::get('status_id')) == '' ) {
                $asset['status_id'] =  NULL;
            } else {
                $asset['status_id'] = e(Input::get('status_id'));
                $asset_list = $asset_list->where('status_id', $asset['status_id']);
            }

            if (e(Input::get('purchase_date')) == '') {
                $asset['purchase_date'] =  NULL;
            } else {
                $asset['purchase_date']   = e(Input::get('purchase_date'));
                $asset_list = $asset_list->where('purchase_date', $asset['purchase_date']);
            }

            if (e(Input::get('supplier_id')) == '') {
                $asset['supplier_id'] =  NULL;
            } else {
                $asset['supplier_id']        = e(Input::get('supplier_id'));
                $asset_list = $asset_list->where('supplier_id', $asset['supplier_id']);
            }


            if (e(Input::get('rtd_location_id')) == '') {
                $asset['rtd_location_id'] = NULL;
            } else {
                $asset['rtd_location_id']     = e(Input::get('rtd_location_id'));
                $asset_list = $asset_list->where('rtd_location_id', $asset['rtd_location_id']);
            }            

            if (e(Input::get('dept_id')) == '') {
                $asset['dept_id']                 = NULL;
            } else {
               $asset['dept_id']                 = e(Input::get('dept_id'));
               $asset_list = $asset_list->where('dept_id', $asset['dept_id']);
            }

            if (e(Input::get('model_id')) == '') {
                $asset['model_id']                = NULL;
            } else {
               $asset['model_id']                = e(Input::get('model_id'));
               $asset_list = $asset_list->where('model_id', $asset['model_id']);
            }

            $asset_list = $asset_list->get();            
            //$query = DB::getQueryLog();
            //dd($asset_list);
            $settings = Setting::getSettings();
            //$x = array('abc' => '123');
            $x = 3;
            foreach ($asset_list as $oneasset) {
                //dd($oneasset->id);
                $qr_code = (object) array(
                    'display' => $settings->qr_code == '1',
                    'url' => route('qr_code/hardware', $oneasset->id)
                );
                //$asset_list->add($x);
                $oneasset->qr_code = $qr_code;
                //dd($oneasset);
                //array_push($oneasset, $x);
            }
            

  
            //dd($asset_list);
            return View::make('backend/hardware/printpage')->with ('asset_list', $asset_list);
        // Redirect to the asset management page with error
        //return Redirect::to("hardware/$assetId/edit")->with('error', Lang::get('admin/hardware/message.update.error'));

    }


/**
    * Check out the asset to a person
    **/
    public function getDeletePage($assetId)
    {
        // Check if the asset exists
        if (is_null($asset = Asset::find($assetId))) {
            // Redirect to the asset management page with error
            return Redirect::to('hardware')->with('error', Lang::get('admin/hardware/message.not_found'));
        }

        return View::make('backend/hardware/delete', compact('asset'));

    }

    /**
    * Check out the asset to a person
    **/
    public function postDelete($assetId)
    {
        // Check if the asset exists
        if (is_null($asset = Asset::find($assetId))) {
            // Redirect to the asset management page with error
            return Redirect::to('hardware')->with('error', Lang::get('admin/hardware/message.not_found'));
        }



        // Declare the rules for the form validation
        $rules = array(
            'note'   => 'required|alpha_space|min:3|max:255',
        );

        // Create a new validator instance from our validation rules
        $validator = Validator::make(Input::all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::back()->withInput()->withErrors($validator);
        }

        if (isset($asset->assigneduser->id) && ($asset->assigneduser->id!=0)) {
            // Redirect to the asset management page
            return Redirect::to('hardware')->with('error', Lang::get('admin/hardware/message.assoc_users'));
        } else {
            // Delete the asset

            DB::table('assets')
            ->where('id', $asset->id)
            ->update(array('assigned_to' => NULL));

            $logaction = new Actionlog();


            $logaction->asset_id = $asset->id;
            $logaction->asset_type = 'hardware';
            $logaction->user_id = Sentry::getUser()->id;
            $logaction->note = e(Input::get('note'));
            $log = $logaction->logaction('deleteAsset');

            $asset->delete();

            // Redirect to the asset management page
            return Redirect::to('hardware')->with('success', Lang::get('admin/hardware/message.delete.success'));
        }

        // Redirect to the asset management page with error
        return Redirect::to("hardware/$assetId")->with('error', Lang::get('admin/hardware/message.delete.error'));
    }

    public function getSingleQR($assetId = null)
    {
        $asset = Asset::withTrashed()->find($assetId);

        if (isset($asset->id)) {

            $settings = Setting::getSettings();

            $qr_code = (object) array(
                'display' => $settings->qr_code == '1',
                'url' => route('qr_code/hardware', $asset->id)
            );

            return View::make('backend/hardware/printpage', compact('asset', 'qr_code'));
        } else {
            // Prepare the error message
            $error = Lang::get('admin/hardware/message.does_not_exist', compact('id'));

            // Redirect to the user management page
            return Redirect::route('hardware')->with('error', $error);
        }

    }


    public function generateAssetTag($deptId, $modelId, $purchaseDate)
    {
        //dd($deptId . $modelId);
        //for Asset Tagging Autogenerate purpose
             $divModel  = Departments::find($deptId);;
//dd($divModel);
             $deptModel = Departments::find($deptId);

             $catModel  = Model::find($modelId);

             $catCode  = $catModel->category->category_code;

             $purchaseYear = substr($purchaseDate, 0, 4);

             
             //$assetTag = $divModel->division->code . "-" .$deptModel->code . "-" . $catCode;
             $assetTag = $divModel->division->code . "-" . $catCode;

             $assetCount = Asset::where('asset_tag', 'like', $assetTag . '%')->count();
             $assetCount += 1;

             //dd($assetTag . "\n" . $assetCount);
             $assetCount = sprintf( '%04d', $assetCount );

             $assetTag = $assetTag . $assetCount . "-" . $purchaseYear;
             //dd($assetTag);
             return $assetTag;
             //dd($assetTag);
             //return View::make('backend/hardware/edit');
    }


    /**
    *  Get the QR code representing the asset
    *
    * @param  int  $assetId
    * @return View
    **/
    public function qrCodeReader($assetTag = null)
    {
        
        $count = Asset::where('asset_tag', $assetTag)->count();
        //dd($count);
        if($count > 0){
            //dd($assetTag);
            $asset = Asset::where('asset_tag', $assetTag)->first();
            //dd($asset->id);
            $result = (route('view/hardware', $asset->id));

        }
        else {
            $result = "none";
        }

        echo $result;
    }  

    public function getDODetail($donumber){
        $do_overview = Actionlog::where('delivery_order', $donumber)->first();
        $do = Actionlog::where('delivery_order', $donumber)->get();

        if($do){
            //var_dump($do);
            return View::make('backend/hardware/do', compact('do', 'do_overview'));
        }
        else {
            $error = 'No such Delivery Order number found.';
            // Redirect to the user management page
            return Redirect::route('hardware')->with('error', $error);
        }
    }

    public function getDOPrint($donumber){
        //dd($donumber);
        $do_overview = Actionlog::where('delivery_order', $donumber)->first();
        $do = Actionlog::where('delivery_order', $donumber)->get();

        if($do){
            //var_dump($do);
            return View::make('backend/hardware/do_print', compact('do', 'do_overview'));
        }
        else {
            $error = 'No such Delivery Order number found.';
            // Redirect to the user management page
            return Redirect::route('hardware')->with('error', $error);
        }
    }

/**
    * Check out the asset to a person
    **/
    public function getCheckoutSite($assetId)
    {
        // Check if the asset exists
        if (is_null($asset = Asset::find($assetId))) {
            // Redirect to the asset management page with error
            return Redirect::to('hardware')->with('error', Lang::get('admin/hardware/message.not_found'));
        }
        $asset_dept_type = $asset->department->code;
        //dd($asset_dept_type);
        // Get the dropdown of users and then pass it to the checkout view
        $sites_list = array('' => 'Select a Site') + DB::table('sites')->select(DB::raw('name, id'))
            ->whereNull('deleted_at')->where('site_type', $asset_dept_type)->orderBy('name', 'asc')->lists('name', 'id');

        return View::make('backend/hardware/checkout_site', compact('asset'))->with('sites_list',$sites_list);

    }

    /**
    * Check out the asset to a person
    **/
    public function postCheckoutSite($assetId)
    {
        // Check if the asset exists
        if (is_null($asset = Asset::find($assetId))) {
            // Redirect to the asset management page with error
            return Redirect::to('hardware')->with('error', Lang::get('admin/hardware/message.not_found'));
        }

        $assigned_to = e(Input::get('assigned_to'));
        //dd($assigned_to);


        // Declare the rules for the form validation
        $rules = array(
            'assigned_to'   => 'required|min:1',
            'note'   => 'alpha_space',
            'delivery' => 'alpha_dash',
        );

        // Create a new validator instance from our validation rules
        $validator = Validator::make(Input::all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::back()->withInput()->withErrors($validator);
        }


        // Check if the user exists
        if (is_null($site = Site::find($assigned_to))) {
            // Redirect to the asset management page with error
            return Redirect::to('hardware')->with('error', Lang::get('admin/hardware/message.site_does_not_exist'));
        }

        // Update the asset data
        $asset->site_id                 = $assigned_to;

        // Was the asset updated?
        if($asset->save()) {
            $logaction = new Actionlog();

            if (Input::has('checkout_at')) {
                if (Input::get('checkout_at')!= date("Y-m-d")){
                    $logaction->created_at = e(Input::get('checkout_at')).' 00:00:00';
                }
            }

            $logaction->asset_id = $asset->id;
            $logaction->site_id = $assigned_to;
            $logaction->asset_type = 'hardware';
            $logaction->location_id = $site->location_id;
            $logaction->user_id = Sentry::getUser()->id;
            $logaction->note = e(Input::get('note'));
            $logaction->delivery_order = e(Input::get('delivery'));
            $log = $logaction->logaction('checkout');

            // Redirect to the new asset page
            return Redirect::to("hardware")->with('success', Lang::get('admin/hardware/message.checkout.success'));
        }

        // Redirect to the asset management page with error
        return Redirect::to("hardware/$assetId/checkout")->with('error', Lang::get('admin/hardware/message.checkout.error'));
    }

public function getCheckinSite($assetId, $backto = null)
    {
        // Check if the asset exists
        if (is_null($asset = Asset::find($assetId))) {
            // Redirect to the asset management page with error
            return Redirect::to('hardware')->with('error', Lang::get('admin/hardware/message.not_found'));
        }

        return View::make('backend/hardware/checkin_site', compact('asset'))->with('backto', $backto);
    }


    /**
    * Check in the item so that it can be checked out again to someone else
    *
    * @param  int  $assetId
    * @return View
    **/
    public function postCheckinSite($assetId = null, $backto = null)
    {
        // Check if the asset exists
        if (is_null($asset = Asset::find($assetId))) {
            // Redirect to the asset management page with error
            return Redirect::to('hardware')->with('error', Lang::get('admin/hardware/message.not_found'));
        }

        // if (!is_null($asset->assigned_to)) {
        //     $user = User::find($asset->assigned_to);
        // }

        // This is just used for the redirect
        $return_to = $asset->site_id;
        //dd($return_to);
        $logaction = new Actionlog();
        $logaction->site_id = $return_to;

        // Update the asset data to null, since it's being checked in
        $asset->site_id                 = NULL;


        // Was the asset updated?
        if($asset->save()) {

             if (Input::has('checkin_at')) {

                if (!strtotime(Input::get('checkin_at'))) {
                    $logaction->created_at = date("Y-m-d h:i:s");
                } elseif (Input::get('checkin_at')!= date("Y-m-d")) {
                    $logaction->created_at = e(Input::get('checkin_at')).' 00:00:00';
                }
            }

            $logaction->asset_id = $asset->id;
            $logaction->location_id = NULL;
            $logaction->asset_type = 'hardware';
            $logaction->note = e(Input::get('note'));
            $logaction->user_id = Sentry::getUser()->id;
            $log = $logaction->logaction('checkin from');


            if ($backto=='user') {
                return Redirect::to("admin/users/".$return_to.'/view')->with('success', Lang::get('admin/hardware/message.checkin.success'));
            } else {
                return Redirect::to("hardware")->with('success', Lang::get('admin/hardware/message.checkin.success'));
            }

        }

        // Redirect to the asset management page with error
        return Redirect::to("hardware")->with('error', Lang::get('admin/hardware/message.checkin.error'));
    }
}
