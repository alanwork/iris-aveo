<?php namespace Controllers\Admin;

use AdminController;
use Input;
use Lang;
use Redirect;
use Setting;
use DB;
use Sentry;
use Str;
use Validator;
use View;
use Site;
use Departments;
use Location;
use Accessory;
use League\Csv\Reader;
use Chumper\Datatable\Facades\Datatable;
use Config;

class SitesController extends AdminController
{
    protected $duplicate_sites = array();
    protected $invalid_type_array = array();
    protected $invalid_location_array = array();
    /**
     * Show a list of all the sites.
     *
     * @return View
     */

    public function getIndex()
    {
        // Grab all the sites
        $sites = Site::orderBy('id', 'ASC');
        if (Input::get('Deleted')) {
            $sites->withTrashed()->Deleted();
        }

        //$sites = $sites->with('locision')->get();
        $sites = $sites->paginate(15);
        // Show the page
        return View::make('backend/sites/index', compact('sites'));


        // $models = $models->with('category','assets','depreciation')->get();

        // // Show the page
        // return View::make('backend/models/index', compact('models'));
    }


    /**
     * Site create.
     *
     * @return View
     */
    public function getCreate($loc_id = null, $site_type = null)
    {
        // Show the page
         //$site_types= array('' => '', 'asset' => 'Asset', 'accessory' => 'Accessory');

        $loc_list = array('' => 'Select a Location') + DB::table('locations')
        ->select(DB::raw('name, id'))->orderBy('name', 'asc')
        ->orderBy('id', 'asc')
        ->whereNull('deleted_at')
        ->lists('name', 'id');

        $type_list = array('' => 'Select a Site Type') + DB::table('departments')
        ->select(DB::raw('name, code'))->orderBy('name', 'asc')
        ->orderBy('name', 'asc')
        ->whereNull('deleted_at')
        ->lists('name', 'code');        

        $view = View::make('backend/sites/edit');
        //$view->with('supplier_list',$supplier_list);
        //$view->with('model_list',$model_list);
        //$view->with('statuslabel_list',$statuslabel_list);
        //$view->with('assigned_to',$assigned_to);
        //$view->with('location_list',$location_list);
        $view->with('loc_list',$loc_list);
        $view->with('type_list',$type_list);
        $view->with('site',new Site);

        if (!is_null($loc_id)) {
            $selected_loc = Location::find($loc_id);
            $view->with('selected_loc',$selected_loc);
        }

        return $view;
        //return View::make('backend/sites/edit')->with('site',new Sites);
    }


    /**
     * Site create form processing.
     *
     * @return Redirect
     */
    public function postCreate()
    {

        // create a new model instance
        $site = new Site();

        $validator = Validator::make(Input::all(), $site->rules);

        if ($validator->fails())
        {
            // The given data did not pass validation
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        else{

            // Update the site data
            $site->name                 = e(Input::get('name'));
            $site->code                 = e(Input::get('code'));
            $site->site_type            = e(Input::get('site_type'));
            $site->location_id          = e(Input::get('loc_id'));
            $site->userid              = Sentry::getId();

            // Was the asset created?
            if($site->save()) {
                // Redirect to the new site  page
                return Redirect::to("admin/settings/sites")->with('success', Lang::get('admin/sites/message.create.success'));
            }
        }

        // Redirect to the site create page
        return Redirect::to('admin/settings/sites/create')->with('error', Lang::get('admin/sites/message.create.error'));


    }

    /**
     * Site update.
     *
     * @param  int  $siteId
     * @return View
     */
    public function getEdit($siteId = null)
    {
        // Check if the site exists
        if (is_null($site = Site::find($siteId))) {
            // Redirect to the blogs management page
            return Redirect::to('admin/settings/sites')->with('error', Lang::get('admin/sites/message.does_not_exist'));
        }

        $loc_list = array('' => 'Select a Location') + DB::table('locations')
        ->select(DB::raw('name, id'))->orderBy('name', 'asc')
        ->orderBy('id', 'asc')
        ->whereNull('deleted_at')
        ->lists('name', 'id');

        $type_list = array('' => 'Select a Site Type') + DB::table('departments')
        ->select(DB::raw('name, code'))->orderBy('name', 'asc')
        ->orderBy('name', 'asc')
        ->whereNull('deleted_at')
        ->lists('name', 'code'); 
        // Show the page
        //$site_options = array('' => 'Top Level') + Site::lists('name', 'id');

        //$site_options = array('' => 'Top Level') + DB::table('sites')->where('id', '!=', $siteId)->lists('name', 'id');
        $site_types= array('' => '', 'asset' => 'Asset', 'accessory' => 'Accessory');
        
        return View::make('backend/sites/edit', compact('site'))
        //->with('site_options',$site_options)
        ->with('type_list',$type_list)
        ->with('loc_list', $loc_list);
    }


    /**
     * Site update form processing page.
     *
     * @param  int  $siteId
     * @return Redirect
     */
    public function postEdit($siteId = null)
    {
        // Check if the blog post exists
        if (is_null($site = Site::find($siteId))) {
            // Redirect to the blogs management page
            return Redirect::to('admin/sites')->with('error', Lang::get('admin/sites/message.does_not_exist'));
        }


        // get the POST data
        $new = Input::all();

        // attempt validation
        $validator = Validator::make(Input::all(), $site->validationRules($siteId));


        if ($validator->fails())
        {
            // The given data did not pass validation
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        // attempt validation
        else {

            // Update the site data
            $site->name            = e(Input::get('name'));
            $site->code            = e(Input::get('code')); 
            $site->site_type            = e(Input::get('site_type'));
            $site->location_id          = e(Input::get('loc_id')); 

            // Was the asset created?
            if($site->save()) {
                // Redirect to the new site page
                return Redirect::to("admin/settings/sites")->with('success', Lang::get('admin/sites/message.update.success'));
            }
        }

        // Redirect to the site management page
        return Redirect::to("admin/settings/sites/$siteID/edit")->with('error', Lang::get('admin/sites/message.update.error'));

    }

    /**
     * Delete the given site.
     *
     * @param  int  $siteId
     * @return Redirect
     */
    public function getDelete($siteId)
    {
        // Check if the site exists
        if (is_null($site = Site::find($siteId))) {
            // Redirect to the blogs management page
            return Redirect::to('admin/settings/sites')->with('error', Lang::get('admin/sites/message.not_found'));
        }


        // if ($site->has_site() > 0) {

        //     // Redirect to the asset management page
        //     return Redirect::to('admin/settings/sites')->with('error', Lang::get('admin/sites/message.assoc_users'));
        // } else {

            $site->delete();

            // Redirect to the locations management page
            return Redirect::to('admin/settings/sites')->with('success', Lang::get('admin/sites/message.delete.success'));
       // }


    }



    /**
    *  Get the asset information to present to the site view page
    *
    * @param  int  $assetId
    * @return View
    **/
    public function getView($siteID = null)
    {

        $site = Site::find($siteID);

        foreach ($site->accessoriesLocation as $siteaccloc) {
            $acc = Accessory::find($siteaccloc->accessory_id);

            $siteaccloc->accname = $acc->name;
        }
        //dd($site->accessoriesLocation->accname);
        //var_dump($site->assets());
        if (isset($site->id)) {
                return View::make('backend/sites/view', compact('site'));
        } else {
            // Prepare the error message
            $error = Lang::get('admin/sites/message.does_not_exist', compact('id'));

            // Redirect to the user management page
            //dd($site->);
            return Redirect::route('sites')->with('error', $error);
        }


    }

    public function getRestore($siteId = null)
    {

        // Get user information
        $site = Site::withTrashed()->find($siteId);

         if (isset($site->id)) {

            // Restore the model
            $site->restore();

            // Prepare the success message
            $success = Lang::get('admin/sites/message.restore.success');

            // Redirect back
            return Redirect::back()->with('success', $success);

         } else {
             return Redirect::back()->with('error', Lang::get('admin/sites/message.not_found'));
         }

    }

/**
     * Site import.
     *
     * @return View
     */
    public function getImportSite()
    {
        // Get all the available groups
        $groups = Sentry::getGroupProvider()->findAll();
        // Selected groups
        $selectedGroups = Input::old('groups', array());
        // Get all the available permissions
        $permissions = Config::get('permissions');
        $this->encodeAllPermissions($permissions);
        // Selected permissions
        $selectedPermissions = Input::old('permissions', array('superuser' => -1));
        $this->encodePermissions($selectedPermissions);
        // Show the page
        return View::make('backend/sites/import', compact('groups', 'selectedGroups', 'permissions', 'selectedPermissions'));
    }
    
    
    /**
     * Asset import form processing.
     *
     * @return Redirect
     */
    public function postImportSite()
    {
        //dd(Input::file('asset_import_csv'));
        //$duplicate_serial = '';
        if (! ini_get("auto_detect_line_endings")) {
            ini_set("auto_detect_line_endings", '1');
        }
        
        $csv = Reader::createFromPath(Input::file('site_import_csv'));  
        $csv->setNewline("\r\n");
        
        if (Input::get('has_headers')==1) {
            $csv->setOffset(1); 
        }
        
        $duplicates = '';  
        $invalid_type = '';  
        $invalid_location = '';  

        //echo $duplicates;  
        //dd($csv);

        $nbInsert = $csv->each(function ($row) use ($duplicates, $invalid_type, $invalid_location) {
            
            if (array_key_exists(2, $row)) {
              
                try {   
                        $today = date('Y-m-d H:i:s');
                        //dd('try');
                        //dd($row[0]);
                        //var_dump($row);
                        //Check if this asset already exists in the system
                //<strong>Name</strong> ,           0
                //<strong>Site Code</strong> ,      1
                //<strong>Site Type</strong> ,      2
                //<strong>Location Code</strong>    3                   
                        $code = DB::table('sites')->where('code', $row[1])->first();

                        $dept_code = Departments::where('code', $row[2])->first();
                            if($dept_code)
                                $dept_code = $dept_code->code;
                            else
                                $dept_code = '';

                        $loc_id = Location::where('code', $row[3])->first();
                            if($loc_id)
                                $loc_id = $loc_id->id;
                            else
                                $loc_id = '';

                        if ($code) {                    
                            $duplicates .= $row[1];
                            //echo $duplicates;
                        }
                        else if ($dept_code == ''){
                            $invalid_type .= $row[2];
                        }                        
                        else if ($loc_id == ''){
                            $invalid_location .= $row[3];
                        }
                         else {
                                    $newsite = array(
                                        'name'                   => $row[0],
                                        'code'                   => $row[1],
                                        'site_type'              => $dept_code,
                                        'location_id'            => $loc_id,
                                        'created_at'             => $today,
                                        'updated_at'             => $today,
                                        'userid'                 => Sentry::getUser()->id,
                                        'deleted_at'             => null,          
                                    );
                                        
                                    DB::table('sites')->insert($newsite);
                                }
                                
                    
                } catch (Exception $e) {
                    echo 'Caught exception: ',  $e->getMessage(), "\n";
                }
                //echo $duplicates;
                array_push($this->duplicate_sites, $duplicates); 
                array_push($this->invalid_type_array, $invalid_type); 
                array_push($this->invalid_location_array, $invalid_location); 
                return true;
            } //echo $duplicates;
        }); 
        
        //echo $duplicates;
        //dd($duplicates);
        //$duplicates = 'There is duplication';
        // var_dump($this->duplicate_dept);

        if($this->duplicate_sites)
            $duplicates = "Duplicate sites(Code) : " . implode(",", $this->duplicate_sites);
        else
            $duplicates = '';

        if ($this->invalid_type_array)
            $invalid_type = "Invalid Site Type(Code) : " . implode(",", $this->invalid_type_array);  
        else 
            $invalid_type = 'none';

        if ($this->invalid_location_array)
            $invalid_location = "Invalid Location(Code) : " . implode(",", $this->invalid_location_array);        
        else
            $invalid_location = 'none';

        //return Redirect::to("admin/settings/sites")->with('success', Lang::get('admin/sites/message.create.success'));
        return Redirect::route('sites')->with('duplicates',$duplicates)->with('invalid_type',$invalid_type)->with('invalid_location',$invalid_location)->with('success', 'Success');
        
    }
}
