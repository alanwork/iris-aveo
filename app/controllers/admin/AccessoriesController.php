<?php namespace Controllers\Admin;

use AdminController;
use Input;
use Lang;
use Accessory;
use Redirect;
use Setting;
use DB;
use Sentry;
use Str;
use Validator;
use View;
use User;
use Actionlog;
use Mail;
use Model;
use Location;
use Site;
use AccessoryLocation;
use Request;

class AccessoriesController extends AdminController
{
    /**
     * Show a list of all the accessories.
     *
     * @return View
     */

    public function getIndex()
    {
        if (Sentry::getUser()){
        // Grab all the accessories

            $accessories = Accessory::orderBy('created_at', 'DESC');

            if (Input::get('Deleted')) {
                $accessories->withTrashed()->Deleted();
            }

            if (isset($_GET['submit'])){
                $search = Request::get('querySearch');
                $accessories = $accessories->where("name", "LIKE", "%$search%")->paginate(15);
            }
            else{
                $accessories = $accessories->paginate(15);
            }
            

            $checkedoutLocation = 0;
            foreach ($accessories as $accessory) {
                $remainQuantity = $this->calcRemaining($accessory);
                $accessory->remainQuantity = $remainQuantity;
                //$accLocations = DB::table('accessories_locations')->where('accessory_id', $accessory->id)->get();
                //dd($accLocations);
            }
            //dd($accessories->remainQuantity);
            // Show the page
            return View::make('backend/accessories/index', compact('accessories'));
        }
        else{
            return Redirect::route('signin');
        }


    }

    public function calcRemaining($acc){
            $checkedoutLocation = 0;
            $accLocations = DB::table('accessories_locations')->where('accessory_id', $acc->id)->get();
            //dd($acc->id);
            //dd($accLocations);
            if($accLocations){
                foreach ($accLocations as $location) {
                    //dd($location);
                   $checkedoutLocation +=  $location->quantity;
                }
                    //dd($checkedoutLocation);
                    $checkedoutPerson  = DB::table('accessories_users')->where('accessory_id', $acc->id)->count();
                    //dd($checkedoutPerson);
                    $totalCheckout = $checkedoutLocation + $checkedoutPerson;
                    //dd($accessory->id);
                    //dd($acc->qty);
                    //dd($totalCheckout);
                    $remainQuantity = $acc->qty - $totalCheckout;
                    //dd($remainQuantity);


            }
            else {
                    $checkedoutPerson  = DB::table('accessories_users')->where('accessory_id', $acc->id)->count();
                    $totalCheckout = $checkedoutPerson;
                    //dd($accessory->id);
                    $remainQuantity = $acc->qty - $totalCheckout;                
            }        
            return $remainQuantity;
    }
    /**
     * Accessory create.
     *
     * @return View
     */
    public function getCreate()
    {
        // Show the page
        $category_list = array('' => '') + DB::table('categories')->where('category_type','=','accessory')->whereNull('deleted_at')->lists('name', 'id');

        $model_list = array('' => 'Select a Type') + DB::table('models')
        //->select(DB::raw('concat(name," / ",modelno) as name, id'))->orderBy('name', 'asc')
        ->select(DB::raw('name, id'))->orderBy('name', 'asc')
        ->orderBy('modelno', 'asc')
        ->whereNull('deleted_at')
        ->lists('name', 'id');


        return View::make('backend/accessories/edit')->with('accessory',new Accessory)->with('category_list',$category_list)->with('model_list', $model_list);
    }


    /**
     * Accessory create form processing.
     *
     * @return Redirect
     */
    public function postCreate()
    {

        // create a new model instance
        $accessory = new Accessory();

        $validator = Validator::make(Input::all(), $accessory->rules);

        if ($validator->fails())
        {
            // The given data did not pass validation
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        else{

            // Update the accessory data
            $accessory->name            		= e(Input::get('name'));
            $accessory->category_id             = e(Input::get('category_id'));
            $accessory->model_id            	= e(Input::get('model_id'));
            $accessory->qty            			= e(Input::get('qty'));
            $accessory->user_id          		= Sentry::getUser()->id;

            // Was the accessory created?
            if($accessory->save()) {
                // Redirect to the new accessory  page
                return Redirect::to("admin/accessories")->with('success', Lang::get('admin/accessories/message.create.success'));
            }
        }

        // Redirect to the accessory create page
        return Redirect::to('admin/accessories/create')->with('error', Lang::get('admin/accessories/message.create.error'));


    }

    /**
     * Accessory update.
     *
     * @param  int  $accessoryId
     * @return View
     */
    public function getEdit($accessoryId = null)
    {
        // Check if the accessory exists
        if (is_null($accessory = Accessory::find($accessoryId))) {
            // Redirect to the blogs management page
            return Redirect::to('admin/accessories')->with('error', Lang::get('admin/accessories/message.does_not_exist'));
        }

		$category_list = array('' => '') + DB::table('categories')->where('category_type','=','accessory')->whereNull('deleted_at')->lists('name', 'id');

        $model_list = array('' => 'Select a Type') + DB::table('models')
        //->select(DB::raw('concat(name," / ",modelno) as name, id'))->orderBy('name', 'asc')
        ->select(DB::raw('name, id'))->orderBy('name', 'asc')
        ->orderBy('modelno', 'asc')
        ->whereNull('deleted_at')
        ->lists('name', 'id');

        return View::make('backend/accessories/edit', compact('accessory'))->with('category_list',$category_list)->with('model_list', $model_list);
    }


    /**
     * Accessory update form processing page.
     *
     * @param  int  $accessoryId
     * @return Redirect
     */
    public function postEdit($accessoryId = null)
    {
        // Check if the blog post exists
        if (is_null($accessory = Accessory::find($accessoryId))) {
            // Redirect to the blogs management page
            return Redirect::to('admin/accessories')->with('error', Lang::get('admin/accessories/message.does_not_exist'));
        }


        // get the POST data
        $new = Input::all();

        // attempt validation
        $validator = Validator::make(Input::all(), $accessory->validationRules($accessoryId));


        if ($validator->fails())
        {
            // The given data did not pass validation
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        // attempt validation
        else {

            // Update the accessory data
            $accessory->name            		= e(Input::get('name'));
            $accessory->category_id             = e(Input::get('category_id'));
            $accessory->model_id            	= e(Input::get('model_id'));
            $accessory->qty            			= e(Input::get('qty'));

            // Was the accessory created?
            if($accessory->save()) {
                // Redirect to the new accessory page
                return Redirect::to("admin/accessories")->with('success', Lang::get('admin/accessories/message.update.success'));
            }
        }

        // Redirect to the accessory management page
        return Redirect::to("admin/accessories/$accessoryID/edit")->with('error', Lang::get('admin/accessories/message.update.error'));

    }

    /**
     * Delete the given accessory.
     *
     * @param  int  $accessoryId
     * @return Redirect
     */
    public function getDelete($accessoryId)
    {
        // Check if the blog post exists
        if (is_null($accessory = Accessory::find($accessoryId))) {
            // Redirect to the blogs management page
            return Redirect::to('admin/accessories')->with('error', Lang::get('admin/accessories/message.not_found'));
        }


		if ($accessory->hasUsers() > 0) {
			 return Redirect::to('admin/accessories')->with('error', Lang::get('admin/accessories/message.assoc_users', array('count'=> $accessory->hasUsers())));
		} else {
			$accessory->delete();

            // Redirect to the locations management page
            return Redirect::to('admin/accessories')->with('success', Lang::get('admin/accessories/message.delete.success'));

		}





    }



    /**
    *  Get the accessory information to present to the accessory view page
    *
    * @param  int  $accessoryId
    * @return View
    **/
    public function getView($accessoryID = null)
    {
        $accessory = Accessory::find($accessoryID);

        if (isset($accessory->id)) {
                //var_dump($accessory->locations);
                return View::make('backend/accessories/view', compact('accessory'));
        } else {
            // Prepare the error message
            $error = Lang::get('admin/accessories/message.does_not_exist', compact('id'));

            // Redirect to the user management page

            return Redirect::route('accessories')->with('error', $error);
        }


    }

    /**
    * Check out the accessory to a person
    **/
    public function getCheckout($accessoryId)
    {
        // Check if the accessory exists
        if (is_null($accessory = Accessory::find($accessoryId))) {
            // Redirect to the accessory management page with error
            return Redirect::to('accessories')->with('error', Lang::get('admin/accessories/message.not_found'));
        }

        // Get the dropdown of users and then pass it to the checkout view
        $users_list = array('' => 'Select a User') + DB::table('users')->select(DB::raw('concat(last_name,", ",first_name) as full_name, id'))->whereNull('deleted_at')->orderBy('last_name', 'asc')->orderBy('first_name', 'asc')->lists('full_name', 'id');

        return View::make('backend/accessories/checkout', compact('accessory'))->with('users_list',$users_list);

    }

    /**
    * Check out the accessory to a person
    **/
    public function postCheckout($accessoryId)
    {
        // Check if the accessory exists
        if (is_null($accessory = Accessory::find($accessoryId))) {
            // Redirect to the accessory management page with error
            return Redirect::to('accessories')->with('error', Lang::get('admin/accessories/message.not_found'));
        }
        
        $assigned_to = e(Input::get('assigned_to'));


        // Declare the rules for the form validation
        $rules = array(
            'assigned_to'   => 'required|min:1',
            'delivery' => 'alpha_dash',
        );

        // Create a new validator instance from our validation rules
        $validator = Validator::make(Input::all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::back()->withInput()->withErrors($validator);
        }


        // Check if the user exists
        if (is_null($user = User::find($assigned_to))) {
            // Redirect to the accessory management page with error
            return Redirect::to('admin/accessories')->with('error', Lang::get('admin/accessories/message.user_does_not_exist'));
        }

        // Update the accessory data
        $accessory->assigned_to            		= e(Input::get('assigned_to'));

        $accessory->users()->attach($accessory->id, array(
        'accessory_id' => $accessory->id,
        'assigned_to' => e(Input::get('assigned_to'))));

            $logaction = new Actionlog();
            $logaction->accessory_id = $accessory->id;
            $logaction->checkedout_to = $accessory->assigned_to;
            $logaction->asset_type = 'accessory';
            $logaction->location_id = $user->location_id;
            $logaction->user_id = Sentry::getUser()->id;
            $logaction->note = e(Input::get('note'));
            $logaction->delivery_order = e(Input::get('delivery'));
            $log = $logaction->logaction('checkout');

            $accessory_user = DB::table('accessories_users')->where('assigned_to','=',$accessory->assigned_to)->where('accessory_id','=',$accessory->id)->first();

            $data['log_id'] = $logaction->id;
            //$data['eula'] = $accessory->getEula();
            $data['first_name'] = $user->first_name;
            $data['item_name'] = $accessory->name;
            //$data['require_acceptance'] = $accessory->requireAcceptance();


            // if (($accessory->requireAcceptance()=='1')  || ($accessory->getEula())) {

	           //  Mail::send('emails.accept-asset', $data, function ($m) use ($user) {
	           //      $m->to($user->email, $user->first_name . ' ' . $user->last_name);
	           //      $m->subject('Confirm accessory delivery');
	           //  });
            // }

            // Redirect to the new accessory page
            return Redirect::to("admin/accessories")->with('success', Lang::get('admin/accessories/message.checkout.success'));



    }


    /**
    * Check the accessory back into inventory
    *
    * @param  int  $accessoryId
    * @return View
    **/
    public function getCheckin($accessoryUserId = null, $backto = null)
    {
        // Check if the accessory exists
        if (is_null($accessory_user = DB::table('accessories_users')->find($accessoryUserId))) {
            // Redirect to the accessory management page with error
            return Redirect::to('admin/accessories')->with('error', Lang::get('admin/accessories/message.not_found'));
        }

		$accessory = Accessory::find($accessory_user->accessory_id);
        return View::make('backend/accessories/checkin', compact('accessory'))->with('backto',$backto);
    }


    /**
    * Check in the item so that it can be checked out again to someone else
    *
    * @param  int  $accessoryId
    * @return View
    **/
    public function postCheckin($accessoryUserId = null, $backto = null)
    {
        // Check if the accessory exists
        if (is_null($accessory_user = DB::table('accessories_users')->find($accessoryUserId))) {
            // Redirect to the accessory management page with error
            return Redirect::to('admin/accessories')->with('error', Lang::get('admin/accessories/message.not_found'));
        }


		$accessory = Accessory::find($accessory_user->accessory_id);
        $logaction = new Actionlog();
        $logaction->checkedout_to = $accessory_user->assigned_to;
        $return_to = $accessory_user->assigned_to;

        // Was the accessory updated?
        if(DB::table('accessories_users')->where('id', '=', $accessory_user->id)->delete()) {

            $logaction->accessory_id = $accessory->id;
            $logaction->location_id = NULL;
            $logaction->asset_type = 'accessory';
            $logaction->user_id = Sentry::getUser()->id;
            $log = $logaction->logaction('checkin from');

            if ($backto=='user') {
				return Redirect::to("admin/users/".$return_to.'/view')->with('success', Lang::get('admin/accessories/message.checkin.success'));
			} else {
				return Redirect::to("admin/accessories/".$accessory->id."/view")->with('success', Lang::get('admin/accessories/message.checkin.success'));
			}
        }

        // Redirect to the accessory management page with error
        return Redirect::to("admin/accessories")->with('error', Lang::get('admin/accessories/message.checkin.error'));
    }

    public function getRestore($assetId = null)
    {

        // Get user information
        $acc = Accessory::withTrashed()->find($assetId);

         if (isset($acc->id)) {

            // Restore the user
            $acc->restore();

            // Prepare the success message
            $success = Lang::get('admin/accessories/message.restore.success');

            // Redirect to the user management page
            return Redirect::route('accessories')->with('success', $success);

         } else {
             return Redirect::to('accessories')->with('error', Lang::get('admin/accessories/message.not_found'));
         }

    }

/**
    * Check out the accessory to a person
    **/
    public function getCheckoutLocation($accessoryId)
    {
        // Check if the accessory exists
        if (is_null($accessory = Accessory::find($accessoryId))) {
            // Redirect to the accessory management page with error
            return Redirect::to('accessories')->with('error', Lang::get('admin/accessories/message.not_found'));
        }

        // Get the dropdown of users and then pass it to the checkout view
        $locations_list = array('' => 'Select a site') + DB::table('sites')->select(DB::raw('name, id'))->whereNull('deleted_at')->lists('name', 'id');

        return View::make('backend/accessories/checkoutLocation', compact('accessory'))->with('locations_list',$locations_list);

    }

    /**
    * Check out the accessory to a person
    **/
    public function postCheckoutLocation($accessoryId)
    {

        // Declare the rules for the form validation
        $rules = array(
            'assigned_to'   => 'required|min:1',
            'quantity'   => 'required|min:1',
            'delivery' => 'alpha_dash',
        );
        $assigned_to = e(Input::get('assigned_to'));
        $quantity    = e(Input::get('quantity'));
        $delivery    = e(Input::get('delivery'));

        // Create a new validator instance from our validation rules
        $validator = Validator::make(Input::all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::back()->withInput()->withErrors($validator);
        }

        // Check if the accessory exists
        if (is_null($accessory = Accessory::find($accessoryId))) {
            // Redirect to the accessory management page with error
            return Redirect::to('accessories')->with('error', Lang::get('admin/accessories/message.not_found'));
        }

        // Check if the user exists
        if (is_null($location = Location::find($assigned_to))) {
            // Redirect to the accessory management page with error
            return Redirect::to('admin/accessories')->with('error', Lang::get('admin/accessories/message.location_does_not_exist'));
        }        
        $remainQuantity = $this->calcRemaining($accessory);
        //dd($remainQuantity);
        if ($quantity > $remainQuantity ){
            //dd('quantity over limit');
            return Redirect::to('admin/accessories')->with('error', Lang::get('admin/accessories/message.checkout.checkout_exceed'));
        }

        $accLocation = DB::table('accessories_locations')->where('assigned_to', $assigned_to)->where('accessory_id', $accessoryId)->first();

        if($accLocation)
        {
            //dd('same acc and location. Update the current row');
            $quantity = $accLocation->quantity + $quantity;
            DB::table('accessories_locations')
            ->where('id', $accLocation->id)
            ->update(array('quantity' => $quantity, 'created_at' => date('Y-m-d H:i:s'), 'user_id' => Sentry::getUser()->id));
        }
        else {
            //dd('diff acc and location. Add new row in accessories_locations');
            $accessory->locations()->attach($accessory->id, array(
            'accessory_id' => $accessory->id,
            'assigned_to' => e(Input::get('assigned_to')),
            'quantity'    => $quantity
                ));            
        }

        // Update the accessory data
        $accessory->assigned_to                 = e(Input::get('assigned_to'));

            $logaction = new Actionlog();
            $logaction->accessory_id = $accessory->id;
            $logaction->checkedout_to = null;
            $logaction->site_id = $accessory->assigned_to;
            $logaction->asset_type = 'accessory';
            //$logaction->location_id = $user->location_id;
            $logaction->user_id = Sentry::getUser()->id;
            $logaction->note = e(Input::get('note'));
            $logaction->accessory_quantity = $quantity;
            $logaction->delivery_order = $delivery;
            $log = $logaction->logaction('checkout');

            //$accessory_user = DB::table('accessories_locations')->where('assigned_to','=',$accessory->assigned_to)->where('accessory_id','=',$accessory->id)->where('quantity','=',$accessory->quantity)->first();

            $data['log_id'] = $logaction->id;
            //$data['eula'] = $accessory->getEula();
            //$data['first_name'] = $user->first_name;
            $data['item_name'] = $accessory->name;
            //$data['require_acceptance'] = $accessory->requireAcceptance();


            // if (($accessory->requireAcceptance()=='1')  || ($accessory->getEula())) {

               //  Mail::send('emails.accept-asset', $data, function ($m) use ($user) {
               //      $m->to($user->email, $user->first_name . ' ' . $user->last_name);
               //      $m->subject('Confirm accessory delivery');
               //  });
            // }

            // Redirect to the new accessory page
            return Redirect::to("admin/accessories")->with('success', Lang::get('admin/accessories/message.checkout.success'));



    }

/**
    * Check the accessory back into inventory
    *
    * @param  int  $accessoryId
    * @return View
    **/
    public function getCheckinLocation($accessoryLocationId = null, $backto = null)
    {
        // Check if the accessory exists
        if (is_null($accessory_location = DB::table('accessories_locations')->find($accessoryLocationId))) {
            // Redirect to the accessory management page with error
            return Redirect::to('admin/accessories')->with('error', Lang::get('admin/accessories/message.not_found'));
        }

        $accessory = Accessory::find($accessory_location->accessory_id);
        $sitename = Site::find($accessory_location->assigned_to);
        return View::make('backend/accessories/checkinLocation', compact('accessory'))->with('backto',$backto)->with('sitename', $sitename);
    }


    /**
    * Check in the item so that it can be checked out again to someone else
    *
    * @param  int  $accessoryId
    * @return View
    **/
    public function postCheckinLocation($accessoryLocationId = null, $backto = null)
    {
        // Check if the accessory exists
        if (is_null($accessories_locations = DB::table('accessories_locations')->find($accessoryLocationId))) {
            // Redirect to the accessory management page with error
            return Redirect::to('admin/accessories')->with('error', Lang::get('admin/accessories/message.not_found'));
        }


        //$accessory = Accessory::find($accessories_locations->accessory_id);


        $quantityCheckin    = e(Input::get('quantity'));

        $accLocation = DB::table('accessories_locations')->where('id', $accessoryLocationId)->first();

        if($accLocation)
        {
            if($accLocation->quantity > $quantityCheckin){

                $logaction = new Actionlog();
                $return_to = $accessories_locations->assigned_to;

                $quantity = $accLocation->quantity - $quantityCheckin;

                DB::table('accessories_locations')
                ->where('id', $accLocation->id)
                ->update(array('quantity' => $quantity, 'updated_at' => date('Y-m-d H:i:s'), 'user_id' => Sentry::getUser()->id));

                $logaction->accessory_id = $accLocation->accessory_id;
                $logaction->asset_type = 'accessory';
                $logaction->user_id = Sentry::getUser()->id;
                $logaction->checkedout_to = null;
                $logaction->location_id = $return_to;             
                $logaction->accessory_quantity = $quantityCheckin;             
                $log = $logaction->logaction('checkin from');

                if ($backto=='user') {
                    return Redirect::to("settings/locations/".$return_to.'/view')->with('success', Lang::get('admin/accessories/message.checkin.success'));
                } else {
                    return Redirect::to("admin/accessories/".$accLocation->accessory_id."/view")->with('success', Lang::get('admin/accessories/message.checkin.success'));
                }                
            }
            else if (($accLocation->quantity - $quantityCheckin) == 0){
                //dd('quantity checkout is 0');

                if(DB::table('accessories_locations')->where('id', '=', $accLocation->id)->delete()) {
                    $logaction = new Actionlog();
                    $return_to = $accessories_locations->assigned_to;

                    $logaction->accessory_id = $accLocation->accessory_id;
                    $logaction->asset_type = 'accessory';
                    $logaction->user_id = Sentry::getUser()->id;
                    $logaction->checkedout_to = null;
                    $logaction->site_id = $return_to;             
                    $logaction->accessory_quantity = $quantityCheckin;             
                    $log = $logaction->logaction('checkin from');

                
                    if ($backto=='user') {
                        return Redirect::to("settings/locations/".$return_to.'/view')->with('success', Lang::get('admin/accessories/message.checkin.success'));
                    } else {
                        return Redirect::to("admin/accessories/".$accLocation->accessory_id."/view")->with('success', Lang::get('admin/accessories/message.checkin.success'));
                    }
                }                 
            }
            else {
                //alert message
            }
        }
        else {          
            //acclocation not found
        }

        // Redirect to the accessory management page with error
        return Redirect::to("admin/accessories")->with('error', Lang::get('admin/accessories/message.checkin.error'));
    } 


/**
    * Check out the asset to a person
    **/
    public function getDeletePage($accessoryId)
    {
        // Check if the asset exists
        if (is_null($accessory = Accessory::find($accessoryId))) {
            // Redirect to the asset management page with error
            return Redirect::to('accessories')->with('error', Lang::get('admin/accessories/message.not_found'));
        }

        return View::make('backend/accessories/delete', compact('accessory'));

    }

    /**
    * Check out the asset to a person
    **/
    public function postDelete($accessoryId)
    {
        // Check if the asset exists
        if (is_null($accessory = Accessory::find($accessoryId))) {
            // Redirect to the asset management page with error
            return Redirect::to('accessories')->with('error', Lang::get('admin/accessories/message.not_found'));
        }



        // Declare the rules for the form validation
        $rules = array(
            'note'   => 'required|alpha_space|min:3|max:255',
        );

        // Create a new validator instance from our validation rules
        $validator = Validator::make(Input::all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::back()->withInput()->withErrors($validator);
        }

        // Check if the blog post exists
        if (is_null($accessory = Accessory::find($accessoryId))) {
            // Redirect to the blogs management page
            return Redirect::to('admin/accessories')->with('error', Lang::get('admin/accessories/message.not_found'));
        }


        if ($accessory->hasUsers() > 0) {
             return Redirect::to('admin/accessories')->with('error', Lang::get('admin/accessories/message.assoc_users', array('count'=> $accessory->hasUsers())));
        } else {
            $logaction = new Actionlog();


            $logaction->accessory_id = $accessory->id;
            $logaction->asset_type = 'accessory';
            $logaction->user_id = Sentry::getUser()->id;
            $logaction->note = e(Input::get('note'));
            $log = $logaction->logaction('deleteAccessory'); 
                       
            $accessory->delete();

            // Redirect to the locations management page
            return Redirect::to('admin/accessories')->with('success', Lang::get('admin/accessories/message.delete.success'));

        }
    }       

}
