<?php namespace Controllers\Account;

use AuthorizedController;
use Input;
use Redirect;
use Sentry;
use Validator;
use Location;
use View;
use Asset;
use Actionlog;
use Lang;
use Accessory;
use DB;
use User;
use Setting;

class ViewAssetsController extends AuthorizedController
{
    /**
     * Redirect to the profile page.
     *
     * @return Redirect
     */
    public function getIndex()
    {
    	$user = Sentry::getUser();


            if (isset($user->id)) {
                //return View::make('frontend/account/view-assets', compact('user'));
            // Grab all the assets

                    $assets = Asset::with('model','assigneduser','assetstatus','defaultLoc','assetlog')->Hardware();
                    // Filter results
                    if (Input::get('Pending')) {
                        $assets->Pending();
                    } elseif (Input::get('RTD')) {
                        $assets->RTD();
                    } elseif (Input::get('Undeployable')) {
                        $assets->Undeployable();
                    } elseif (Input::get('Archived')) {
                        $assets->Archived();
                    } elseif (Input::get('Requestable')) {
                        $assets->RequestableAssets();
                    } elseif (Input::get('Deployed')) {
                        $assets->Deployed();
                    } elseif (Input::get('Deleted')) {
                        $assets->withTrashed()->Deleted();
                    }

                    $assets = $assets->orderBy('created_at', 'DESC')->get();


                    // Paginate the users
                    /**$assets = $assets->paginate(Setting::getSettings()->per_page)
                        ->appends(array(
                            'Pending' => Input::get('Pending'),
                            'RTD' => Input::get('RTD'),
                            'Undeployable' => Input::get('Undeployable'),
                            'Deployed' => Input::get('Deployed'),
                        ));
                    **/

                    return View::make('backend/hardware/index', compact('assets'));                
            } else {
                // Prepare the error message
                $error = Lang::get('admin/users/message.user_not_found', compact('id' ));

                // Redirect to the user management page
                return Redirect::route('users')->with('error', $error);
            }

	}


	public function getRequestableIndex() {

		$assets = Asset::with('model','defaultLoc')->Hardware()->RequestableAssets()->get();
        return View::make('frontend/account/requestable-assets', compact('user','assets'));
    }

    public function getAssetByDeptIndex() {

        $user = Sentry::getUser();
        $userdept = $user->location_id;


        $sitetable = DB::table('sites')->where('location_id', $userdept)->where('site_type', 'PIM')->pluck('id');
        //dd($sitetable);
        // $assets = DB::table('assets')
        //             ->leftJoin('models', 'model_id', '=' , 'models.id')
        //             ->leftJoin('departments', 'dept_id', '=', 'departments.id')
        //             ->leftJoin('users', 'assets.dept_id', '=', 'users.dept_id')
        //             ->where('assets.dept_id', $userdept)
        //             ->get();

        //$assets = Asset::with('model','department')->where('rtd_location_id', $userdept)->get();                    
        $assets = Asset::with('model','department')->where('site_id', $sitetable)->get();                    
                   // dd($assets);
        return View::make('frontend/account/department-assets', compact('user', 'assets','userdept'));
        //return View::make('frontend/account/department-assets')->with('user', $user)->with('assets', $assets)->with('userdept', $userdept);
    }

    public function getRequestAsset($assetId = null) {

    	// Check if the asset exists
        if (is_null($asset = Asset::find($assetId))) {
            // Redirect to the asset management page
            return Redirect::to('frontend/account/view-assets')->with('error', Lang::get('admin/hardware/message.does_not_exist'));
        } else {


			 return View::make('frontend/account/view-assets', compact('asset'));
        }


    }
    
    
    
    // Get the acceptance screen
    public function getAcceptAsset($logID = null) {
	    
	    if (is_null($findlog = Actionlog::find($logID))) {
            // Redirect to the asset management page
            return Redirect::to('account')->with('error', Lang::get('admin/hardware/message.does_not_exist'));
        }
        
        // Asset
        if (($findlog->asset_id!='') && ($findlog->asset_type=='hardware')) {
        	$item = Asset::find($findlog->asset_id);
        
        // software	
        } elseif (($findlog->asset_id!='') && ($findlog->asset_type=='software')) {     
	        $item = License::find($findlog->asset_id);
	    // accessories    
	    } elseif ($findlog->accessory_id!='') {
		   $item = Accessory::find($findlog->accessory_id);         
        }
        
	    // Check if the asset exists
        if (is_null($item)) {
            // Redirect to the asset management page
            return Redirect::to('account')->with('error', Lang::get('admin/hardware/message.does_not_exist'));
        }
        
        return View::make('frontend/account/accept-asset', compact('item'))->with('findlog', $findlog);
        
        

	    
    }
    
    // Save the acceptance
    public function postAcceptAsset($logID = null) {
	  
	  	
	  	// Check if the asset exists
        if (is_null($findlog = Actionlog::find($logID))) {
            // Redirect to the asset management page
            return Redirect::to('account/view-assets')->with('error', Lang::get('admin/hardware/message.does_not_exist'));
        }
                
        
        if ($findlog->accepted_id!='') {
            // Redirect to the asset management page
            return Redirect::to('account/view-assets')->with('error', Lang::get('admin/users/message.error.asset_already_accepted'));
        }
        
    	$user = Sentry::getUser();
		$logaction = new Actionlog();
			
		// Asset
        if (($findlog->asset_id!='') && ($findlog->asset_type=='hardware')) {
        	$logaction->asset_id = $findlog->asset_id;
        	$logaction->accessory_id = NULL;
        	$logaction->asset_type = 'hardware';
        
        // software	
        } elseif (($findlog->asset_id!='') && ($findlog->asset_type=='software')) {     
	        $logaction->asset_id = $findlog->asset_id;
        	$logaction->accessory_id = NULL;
        	$logaction->asset_type = 'software';
        	
		// accessories    
	    } elseif ($findlog->accessory_id!='') {
		    $logaction->asset_id = NULL;
        	$logaction->accessory_id = $findlog->accessory_id;
        	$logaction->asset_type = 'accessory';         
        }
			
		$logaction->checkedout_to = $findlog->checkedout_to;
		
		$logaction->note = e(Input::get('note'));
		$logaction->user_id = $user->id;
		$logaction->accepted_at = date("Y-m-d h:i:s");
		$log = $logaction->logaction('accepted');
		
      
		$update_checkout = DB::table('asset_logs')
			->where('id',$findlog->id)
			->update(array('accepted_id' => $logaction->id));
		
		if ($update_checkout ) {
			return Redirect::to('account/view-assets')->with('success', 'You have successfully accept this asset.');
			
		} else {
			return Redirect::to('account/view-assets')->with('error', 'Something went wrong ');
		} 
    }
    

    public function getAssetDetails($assetId = null)
    {
        $asset = Asset::withTrashed()->find($assetId);

        if (isset($asset->id)) {
            //dd('in isset');
            $settings = Setting::getSettings();

            $qr_code = (object) array(
                'display' => $settings->qr_code == '1',
                'url' => route('qr_code/hardware', $asset->id)
            );

            return View::make('frontend/account/view-asset-details', compact('asset', 'qr_code'));
        } else {
            // Prepare the error message
            $error = Lang::get('admin/hardware/message.does_not_exist', compact('id'));

            // Redirect to the user management page
            return Redirect::route('department-assets')->with('error', $error);
        }

    }


/**
     * Asset import.
     *
     * @return View
     */
    public function getImportAsset()
    {
        // Get all the available groups
        //$userid = Sentry::getUser()->id;
        // $groups = Sentry::getGroupProvider()->findAll();
        // // Selected groups
        // $selectedGroups = Input::old('groups', array());
        // // Get all the available permissions
        // $permissions = Config::get('permissions');
        // $this->encodeAllPermissions($permissions);
        // // Selected permissions
        // $selectedPermissions = Input::old('permissions', array('superuser' => -1));
        // $this->encodePermissions($selectedPermissions);
        // Show the page
        return View::make('frontend/account/import');
    }
    
    
    /**
     * Asset import form processing.
     *
     * @return Redirect
     */
    public function postImportAsset()
    {
        //dd(Input::file('asset_import_csv'));
        //$duplicate_serial = '';
        if (! ini_get("auto_detect_line_endings")) {
            ini_set("auto_detect_line_endings", '1');
        }
        
        $csv = Reader::createFromPath(Input::file('asset_import_csv'));  
        $csv->setNewline("\r\n");
        
        if (Input::get('has_headers')==1) {
            $csv->setOffset(1); 
        }
        
        $duplicates = '';
        $inv_model = '';  
        $inv_location = '';  
        $inv_supp = '';  
        $inv_dept = '';  

        //echo $duplicates;  
        //dd($csv);

        $nbInsert = $csv->each(function ($row) use ($duplicates, $inv_model, $inv_location, $inv_supp, $inv_dept) {
            
            if (array_key_exists(1, $row)) {
              
                try {   
                        $today = date('Y-m-d H:i:s');
                        //dd('try');
                        //dd($row[0]);
                        //var_dump($row);
                        //Check if this asset already exists in the system
                        $serial = DB::table('assets')->where('serial', $row[2])->first();
                        $modelid = Model::where('modelno', addslashes($row[1]))->first();
                        $suppid = DB::table('suppliers')->where('code', $row[6])->first();
                        $locationid = DB::table('locations')->where('code', $row[7])->first();
                        $deptid = DB::table('departments')->where('code', $row[8])->first();



                        if($modelid)
                            $modelid = $modelid->id;
                        else
                            $modelid = '';

                        if($suppid)
                            $suppid = $suppid->id;
                        else
                            $suppid = '';

                        if($locationid)
                            $locationid = $locationid->id;
                        else
                            $locationid = ''; 
                                                   
                        if($deptid)
                            $deptid = $deptid->id;
                        else
                            $deptid = '';


                        if ($serial) {                    
                            $duplicates .= $row[2];
                            //echo $duplicates;
                        }
                        else if ($modelid == ''){
                            $inv_model = $row[1];
                        }                           
                        else if ($suppid == ''){
                            $inv_supp = $row[6];
                        }                         
                        else if ($locationid == ''){
                            $inv_location = $row[7];
                        }                        
                        else if ($deptid == ''){
                            $inv_dept = $row[8];
                        }                        
                         else {
                                if ($modelid != '' && $deptid != '' && $row[3] != ''){
                                    $dt = date_create_from_format('d/m/y', $row[3]);
                                    $purDate =  $dt->format('Y-m-d H:i:s');
                                    //var_dump($dt);
                                    //dd($dt->format('Y-m-d H:i:s'));
                                    $assetTag = $this->generateAssetTag($deptid, $modelid, $purDate);//dept_id, model_id, purchase date
                                }
                                else
                                    $assetTag = '';

                                    $newasset = array(
                                        'name'                   => $row[0],
                                        'asset_tag'              => $assetTag,
                                        'model_id'               => $modelid,
                                        'serial'                 => $row[2],
                                        'purchase_date'          => $row[3],
                                        'purchase_cost'          => $row[4],
                                        'order_number'           => $row[5],
                                        'assigned_to'            => null,
                                        'notes'                  => '',
                                        'user_id'                => Sentry::getUser()->id,
                                        'created_at'             => $today,
                                        'updated_at'             => $today,
                                        'physical'               => 1,
                                        'deleted_at'             => null,
                                        'status_id'              => 2,
                                        'archived'               => 0,
                                        'warranty_months'        => null,
                                        'depreciate'             => 0,
                                        'supplier_id'            => $suppid,
                                        'requestable'            => 0,
                                        'rtd_location_id'        => $locationid,
                                        'mac_address'            => null,
                                        'dept_id'                => $deptid,
                                        //'type_id'                => $row[24],
                           
                                    );
                                   //     var_dump($newasset);
                                    DB::table('assets')->insert($newasset);
                                }
                                
                    
                } catch (Exception $e) {
                    echo 'Caught exception: ',  $e->getMessage(), "\n";
                }
                //echo $duplicates;
                array_push($this->duplicate_serial, $duplicates); 
                array_push($this->invalid_model, $inv_model); 
                array_push($this->invalid_department, $inv_dept); 
                array_push($this->invalid_location, $inv_location); 
                array_push($this->invalid_supplier, $inv_supp); 
                return true;
            } //echo $duplicates;
        }); 
        
        //echo $duplicates;
        //dd($duplicates);
        //$duplicates = 'There is duplication';
         //var_dump($this->duplicate_serial);
        $duplicates = "Serial Numbers : " . implode(" ", $this->duplicate_serial);

        if ($this->invalid_model != '')
            $inv_model = implode(" ", $this->invalid_model);
         else
             $inv_model = '';

        if (isset($this->invalid_department))
            $inv_dept = implode(" ", $this->invalid_department);
         else
             $inv_dept = '';

        if(isset($this->invalid_location))
            $inv_location = implode(" ", $this->invalid_location);

        if(isset($this->invalid_supplier))
            $inv_supp = implode(" ", $this->invalid_supplier);
        
        //dd($inv_supp);
        return Redirect::route('hardware')
        ->with('duplicates',$duplicates)
        ->with('invalid_model',$inv_model)
        ->with('invalid_department',$inv_dept)
        ->with('invalid_location',$inv_location)
        ->with('invalid_supplier',$inv_supp)
        ->with('success', 'Success');
        
    }    


}
