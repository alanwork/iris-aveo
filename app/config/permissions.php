<?php

return array(

    'Global' => array(
        array(
            'permission' => 'superuser',
            'label'      => 'Super User',
        ),
    ),

    'Admin' => array(
        array(
            'permission' => 'admin',
            'label'      => 'Admin Rights',
        ),
    ),

    'Reporting' => array(
        array(
            'permission' => 'reports',
            'label'      => 'View Reports',
        ),
    ),

    'Finance' => array(
        array(
            'permission' => 'finance',
            'label'      => 'Finance Rights',
        ),
    ),
);
