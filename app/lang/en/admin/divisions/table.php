<?php

return array(

    'create'                => 'Create Division',
    'created_at'            => 'Created at',
    'name'                  => 'Division Name',
    'numassets'             => 'Assets',
    'update'                => 'Update Division',
    'view'                  => 'View Division',
    'update'                => 'Update Division',
    'clone'             => 'Clone Division',
    'edit'              => 'Edit Division',
    'title'                 => 'Divisions',
    'code'                  => 'Division Code',    
);

