<?php

return array(

    'deleted'  					=> 'This division has been deleted. <a href="/hardware/divisions/:division_id/restore">Click here to restore it</a>.',
    'restore'                   => 'Restore Division',
	'show_mac_address'			=> 'Show MAC address field in assets in this model',
    'view_deleted'              => 'View Deleted',
    'view_divisions'            => 'View Divisions',
    'create'					=> 'Create Division',
    'division_name'				=> 'Division Name',
    'division_code'				=> 'Division Code',
    'about_division' 			=> 'About Division',
    'about_divisions'  			=> 'Divisions help you organize your department. Some example divisions might be &quot;Human Resource&quot;, &quot;Finance&quot;, and so on, but you can use divisions any way that makes sense for you.',
    
);
