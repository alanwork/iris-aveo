<?php

return array(

    'does_not_exist' => 'Division does not exist.',
    'assoc_users'	 => 'This division is currently associated with at least one department and cannot be deleted. Please update your department to no longer reference this division and try again. ',

    'create' => array(
        'error'   => 'Division was not created, please try again.',
        'success' => 'Division created successfully.'
    ),

    'update' => array(
        'error'   => 'Division was not updated, please try again',
        'success' => 'Division updated successfully.'
    ),

    'delete' => array(
        'confirm'   => 'Are you sure you wish to delete this Division?',
        'error'   => 'There was an issue deleting the Division. Please try again.',
        'success' => 'The Division was deleted successfully.'
    )

);
