<?php

return array(

    'id'      		=> 'ID',
    'city'   		=> 'City',
    'state'   		=> 'State',
    'country'   	=> 'Country',
    'create'		=> 'Create Location',
    'update'		=> 'Update Location',
    'name'			=> 'Location Name',
    'address'		=> 'Address',
    'zip'			=> 'Postal Code',
    'locations'     => 'Locations',
    'code'          => 'Code',
    'is_ktw'        => 'Kampung Tanpa Wayar',
    'is_pi1m'        => 'Pusat Internet 1 Malaysia',
    'is_fiber'      => 'Fiber',
    'cluster'       => 'Cluster',
    'site'		    => 'Site',
);
