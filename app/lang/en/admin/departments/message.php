<?php

return array(

    'does_not_exist' => 'Department does not exist.',
    'assoc_users'	 => 'This division is currently associated with at least one department and cannot be deleted. Please update your department to no longer reference this division and try again. ',

    'create' => array(
        'error'   => 'Department was not created, please try again.',
        'success' => 'Department created successfully.'
    ),

    'update' => array(
        'error'   => 'Department was not updated, please try again',
        'success' => 'Department updated successfully.'
    ),

    'delete' => array(
        'confirm'   => 'Are you sure you wish to delete this Department?',
        'error'   => 'There was an issue deleting the Department. Please try again.',
        'success' => 'The Department was deleted successfully.'
    )

);
