<?php

return array(

    'create'                => 'Create Department',
    'created_at'            => 'Created at',
    'name'                  => 'Department Name',
    'numassets'             => 'Assets',
    'update'                => 'Update Department',
    'view'                  => 'View Department',
    'update'                => 'Update Department',
    'clone'             => 'Clone Department',
    'edit'              => 'Edit Department',
    'title'                 => 'Departments',
    'code'                  => 'Department Code',
    'div_code'              => 'Division Code', 
    'division'              => 'Division',    
);

