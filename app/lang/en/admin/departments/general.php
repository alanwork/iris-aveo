<?php

return array(

    'deleted'  					=> 'This department has been deleted. <a href="/hardware/departments/:department_id/restore">Click here to restore it</a>.',
    'restore'                   => 'Restore Department',
	'show_mac_address'			=> 'Show MAC address field in assets in this model',
    'view_deleted'              => 'View Deleted',
    'view_departments'          => 'View Departments',
    'create'					=> 'Create Department',
    'department_name'			=> 'Department Name',
    'department_code'			=> 'Department Code',
    'about_department' 			=> 'About Department',
    'about_departments'  		=> 'Departments help you organize your department. Some example departments might be &quot;Human Resource&quot;, &quot;Finance&quot;, and so on, but you can use departments any way that makes sense for you.',
    'select_division'           => 'Select a Division',
    'update'                    => 'Update Department',
    
);
