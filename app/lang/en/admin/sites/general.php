<?php

return array(

    'deleted'  					=> 'This site has been deleted. <a href="/hardware/sites/:site_id/restore">Click here to restore it</a>.',
    'restore'                   => 'Restore Site',
	'show_mac_address'			=> 'Show MAC address field in assets in this model',
    'view_deleted'              => 'View Deleted',
    'view_sites'                => 'View Sites',
    'create'					=> 'Create Site',
    'site_name'			        => 'Site Name',
    'site_code'			        => 'Site Code',
    'about_site' 			    => 'About Site',
    'about_sites'  		        => 'Sites help you organize your site. Some example sites might be &quot;Human Resource&quot;, &quot;Finance&quot;, and so on, but you can use sites any way that makes sense for you.',
    'update'                    => 'Update Site',
    'select_location'           => 'Location',
    'site_type'                 => 'Type',
    
);
