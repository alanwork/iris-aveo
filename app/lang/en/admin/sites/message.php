<?php

return array(

    'does_not_exist' => 'Site does not exist.',
    'assoc_users'	 => 'This division is currently associated with at least one site and cannot be deleted. Please update your site to no longer reference this division and try again. ',

    'create' => array(
        'error'   => 'Site was not created, please try again.',
        'success' => 'Site created successfully.'
    ),

    'update' => array(
        'error'   => 'Site was not updated, please try again',
        'success' => 'Site updated successfully.'
    ),

    'delete' => array(
        'confirm'   => 'Are you sure you wish to delete this Site?',
        'error'   => 'There was an issue deleting the Site. Please try again.',
        'success' => 'The Site was deleted successfully.'
    )

);
