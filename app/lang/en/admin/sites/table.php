<?php

return array(

    'create'                => 'Create Site',
    'created_at'            => 'Created at',
    'name'                  => 'Site Name',
    'numassets'             => 'Assets',
    'update'                => 'Update Site',
    'view'                  => 'View Site',
    'update'                => 'Update Site',
    'clone'             => 'Clone Site',
    'edit'              => 'Edit Site',
    'title'                 => 'Sites',
    'code'                  => 'Site Code',
    'div_code'              => 'Division Code', 
    'division'              => 'Division', 
    'site_type'             => 'Site Type',   
    'location_id'           => 'Location',
    'asset_accessory'       => 'Assets & Accesories',
    'details'               => 'Details',
);

