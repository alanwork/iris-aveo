<?php

return array(

    'does_not_exist' => 'Type does not exist.',
    'assoc_users'	 => 'This Type is currently associated with one or more assets and cannot be deleted. Please delete the assets, and then try deleting again. ',


    'create' => array(
        'error'   => 'Type was not created, please try again.',
        'success' => 'Type created successfully.'
    ),

    'update' => array(
        'error'   => 'Type was not updated, please try again',
        'success' => 'Type updated successfully.'
    ),

    'delete' => array(
        'confirm'   => 'Are you sure you wish to delete this asset Type?',
        'error'   => 'There was an issue deleting the Type. Please try again.',
        'success' => 'The Type was deleted successfully.'
    ),
    
    'restore' => array(
        'error'   		=> 'Type was not restored, please try again',
        'success' 		=> 'Type restored successfully.'
    ),

);
