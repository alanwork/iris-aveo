<?php

return array(

    'create'				=> 'Create Asset Type',
    'created_at' 			=> 'Created at',
    'eol'	 				=> 'EOL',
    'modelnumber'   		=> 'Type Code.',
    'name'      			=> 'Asset Type Name',
    'numassets' 			=> 'Assets',
    'title'					=> 'Asset Types',
    'update'				=> 'Update Asset Type',
    'view'					=> 'View Asset Type',
    'update'				=> 'Update Asset Type',
    'clone'				=> 'Clone Type',
    'edit'				=> 'Edit Type',
);
