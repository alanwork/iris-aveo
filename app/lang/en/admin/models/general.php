<?php

return array(

    'deleted'  					=> 'This type has been deleted. <a href="/hardware/models/:model_id/restore">Click here to restore it</a>.',
    'restore'                   => 'Restore Type',
	'show_mac_address'			=> 'Show MAC address field in assets in this model',
    'view_deleted'              => 'View Deleted',
    'view_models'               => 'View Types',

);
