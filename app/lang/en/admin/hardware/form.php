<?php

return array(

    'bulk_update'       => 'Bulk Update Assets',
    'bulk_update_help'  => 'This form allows you to update multiple assets at once. Only fill in the fields you need to change. Any fields left blank will remain unchanged. ',
    'bulk_update_warn'  => 'You are about to edit the properties of :asset_count assets.',
    'checkedout_to'     => 'Assigned To',
    'checkout_date'     => 'Assigned Date',
    'checkin_date'      => 'Checkin Date',
    'checkout_to'       => 'Checkout to',
    'cost'              => 'Purchase Cost',
    'create'            => 'Create Asset',
    'date'              => 'Purchase Date',
    'depreciates_on'    => 'Depreciates On',
    'depreciation'      => 'Depreciation',
    'default_location'  => 'Location',
    'eol_date'          => 'EOL Date',
    'eol_rate'          => 'EOL Rate',
    'expires'           => 'Expires',
    'fully_depreciated' => 'Fully Depreciated',
    'help_checkout'     => 'If you wish to assign this asset immediately, you should select "Ready to Deploy" from the status list above, or unexpected things may happen. ',
    'mac_address'       => 'MAC Address',
    'manufacturer'      => 'Manufacturer',
    'model'             => 'Type',
    'months'            => 'months',
    'name'              => 'Asset Name',
    'notes'             => 'Notes',
    'order'             => 'Order Number',
    'qr'                => 'QR Code',
    'requestable'       => 'Users may request this asset',
    'select_statustype' => 'Select Status Type',
    'serial'            => 'Serial',
    'status'            => 'Status',
    'supplier'          => 'Supplier',
    'tag'               => 'Asset Tag',
    'update'            => 'Update Asset ',
    'warranty'          => 'Warranty',
    'years'             => 'years',
    'department'        => 'Department',
    'category'          => 'Category',
    'updatestatus'      => 'Asset Status Update',
    'qrcode'            => 'QR Code',
    'print'             => 'Print Asset',
    'start_date'        => 'Start Date',
    'end_date'          => 'End Date',
    'assign_flag'       => 'Assign',
    'print_assign'      => 'Print Assigned Asset',
    'print_history'     => 'Print Asset Movement',
    'action_type'       => 'Action Type',
    'asset_type'        => 'Asset Type',
    'delivery'          => 'Delivery Order',
    'delete_date'       => 'Deletion Date',
    'site'              => 'Site',
    'justify'           => 'Justification',

)
;
