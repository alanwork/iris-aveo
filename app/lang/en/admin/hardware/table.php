<?php

return array(

    'asset_tag'   	=> 'Asset Tag',
    'asset_model'       => 'Type',
    'book_value'  	=> 'Value',
    'change' 		=> 'Person',
    'checkout_date' => 'Checkout Date',
    'checkoutto' 	=> 'Checked Out',
    'diff' 			=> 'Diff',
    'dl_csv' 		=> 'Download CSV',
    'eol' 			=> 'EOL',
    'id'      		=> 'ID',
    'location' 		=> 'Location',
    'purchase_cost'	=> 'Cost',
    'purchase_date'	=> 'Purchased',
    'serial'   		=> 'Serial',
    'status'   		=> 'Status',
    'title'         => 'Asset ',
    'asset_price'   => 'Price ',
    'recipient'     => 'Recipient',
    'note'      	=> 'Note ',
    'checkout_site' => 'Site',
    'checkedout_site' => 'Checkout to Site',
    'checkedout_user' => 'Checkout to User',

    

);
