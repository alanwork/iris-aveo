<?php

return array(
	'archived'  				=> 'Archived',
    'asset'  					=> 'Asset',
    'checkin'                   => 'Checkin Asset',
    'checkout'                  => 'Assign Asset to User',
    'checkout_site'  			=> 'Assign Asset to Site',
    'clone'  					=> 'Clone Asset',
    'deployable'  				=> 'Deployable',
    //'deleted'                   => 'This asset has been deleted. <a href="/hardware/:asset_id/restore">Click here to restore it</a>.',
    'deleted'  					=> 'This asset has been deleted. Click here to restore it.',
    'edit'  					=> 'Edit Asset',
    'model_deleted'  			=> 'This Assets type has been deleted. You must restore the model before you can restore the Asset.<br/> <a href="/hardware/models/:model_id/restore">Click here to restore the type</a>.',
    'requestable'				=> 'Requestable',
    'restore'  					=> 'Restore Asset',
    'pending'  					=> 'Pending',
    'undeployable'  			=> 'Undeployable',
    'view'  					=> 'View Asset',
    'department'                => "Location Assets",
    'delete'                    => "Delete Asset",
    'dismissed'                 => "Dismissed",
    'dopage'                    => "Delivery Order",
    
);
