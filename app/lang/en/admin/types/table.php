<?php

return array(

    'create'                => 'Create Asset Type',
    'created_at'            => 'Created at',
    'code'                  => 'Asset Type Code.',
    'name'                  => 'Asset Type Name',
    'numassets'             => 'Assets',
    'update'                => 'Update Asset Type',
    'view'                  => 'View Asset Type',
    'update'                => 'Update Type',
    'clone'             => 'Clone Asset Type',
    'edit'              => 'Edit Asset Type',
    'title'                 => 'Asset Types',
);