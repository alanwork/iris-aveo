<?php

return array(

    'does_not_exist' => 'Asset Type does not exist.',
    'assoc_users'	 => 'This asset type is currently associated with at least one asset and cannot be deleted. Please update your asset to no longer reference this asset type and try again. ',

    'create' => array(
        'error'   => 'Asset Type was not created, please try again.',
        'success' => 'Asset Type created successfully.'
    ),

    'update' => array(
        'error'   => 'Asset Type was not updated, please try again',
        'success' => 'Asset Type updated successfully.'
    ),

    'delete' => array(
        'confirm'   => 'Are you sure you wish to delete this Asset Type?',
        'error'   => 'There was an issue deleting the Asset Type. Please try again.',
        'success' => 'The Asset Type was deleted successfully.'
    )

);
