<?php

return array(

    'deleted'  					=> 'This asset type has been deleted. <a href="/hardware/divisions/:division_id/restore">Click here to restore it</a>.',
    'restore'                   => 'Restore Asset Type',
	'show_mac_address'			=> 'Show MAC address field in assets in this model',
    'view_deleted'              => 'View Deleted',
    'view_divisions'            => 'View Asset Types',
    'create'					=> 'Create Asset Type',
    'type_name'				    => 'Type Name',
    'type_code'				    => 'Division Code',
    'about_type' 			    => 'About Asset Type',
    'about_types'  			    => 'Types help you organize your asset type. Some example type might be &quot;Desktop&quot;, &quot;Laptop&quot;, and so on, but you can use types any way that makes sense for you.',
    
);
