<?php
namespace Aveo;

class UserProvider extends \Cartalyst\Sentry\Users\Eloquent\Provider
{
	/**
	* @return \Cartalyst\Sentry\Users\UserInterface
	*/
	public function findByEmail($email)
	{
		$model     = $this->createModel();
		//$model = new \User;

		$query = $model->newQuery();

		$query->where('email', '=', $email);

		$user = $query->first();

		return $user;
	}
}