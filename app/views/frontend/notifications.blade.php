@if ($errors->any())
<div class="col-md-12">
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <i class="fa fa-exclamation-circle"></i>
        <strong>Error: </strong>
         Please check the form below for errors
    </div>
</div>

@endif

@if ($message = Session::get('success'))
<div class="col-md-12">
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <i class="fa fa-check"></i>
        <strong>Success: </strong>
        {{ $message }}
    </div>
</div>
@endif

@if ($message = Session::get('error'))
<div class="col-md-12">
    <div class="alert alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <i class="fa fa-exclamation-circle"></i>
        <strong>Error: </strong>
        {{ $message }}
    </div>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="col-md-12">
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <i class="fa fa-warning"></i>
        <strong>Warning: </strong>
        {{ $message }}
    </div>
</div>
@endif

@if ($message = Session::get('info'))
<div class="col-md-12">
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <i class="fa fa-info-circle"></i>
        <strong>Info: </strong>
        {{ $message }}
    </div>
</div>
@endif


@if ($message = Session::get('duplicates'))
<div class="col-md-12">
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <i class="fa fa-check"></i>
        <strong>Duplicates : </strong>
        {{ $message }}
    </div>
</div>
@endif

@if ($message = Session::get('invalid_type'))
<div class="col-md-12">
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <i class="fa fa-check"></i>
        <strong>Invalid Type : </strong>
        {{ $message }}
    </div>
</div>
@endif

@if ($message = Session::get('invalid_location'))
<div class="col-md-12">
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <i class="fa fa-check"></i>
        <strong>Invalid Location : </strong>
        {{ $message }}
    </div>
</div>
@endif

@if ($message = Session::get('invalid_model'))
<div class="col-md-12">
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <i class="fa fa-check"></i>
        <strong>Invalid Asset Type : </strong>
        {{ $message }}
    </div>
</div>
@endif

@if ($message = Session::get('invalid_department'))
<div class="col-md-12">
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <i class="fa fa-check"></i>
        <strong>Invalid Department Code : </strong>
        {{ $message }}
    </div>
</div>
@endif

@if ($message = Session::get('invalid_supplier'))
<div class="col-md-12">
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <i class="fa fa-check"></i>
        <strong>Invalid Supplier Code : </strong>
        {{ $message }}
    </div>
</div>
@endif

