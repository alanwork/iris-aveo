@extends('backend/layouts/default')

@section('title0')
            @lang('admin/hardware/general.department')
@stop

{{-- Page title --}}
@section('title')
    @yield('title0') :: @parent
@stop

{{-- Page content --}}
@section('content')

<div class="row header">
    <div class="col-md-12">
        <h3>@yield('title0')</h3>
        <a href="{{ route('hardwareAccount') }}" class="btn btn-default pull-right"><span class="fa fa-upload"></span> @lang('general.upload')</a>
    </div>
</div>

<div class="row form-wrapper">
<?php $spanrows = 8; ?>
@if (count($assets) > 0)
<div class="table-responsive">
<table id="example">

    <thead>
        <tr role="row">
            <th class="col-md-1" bSortable="true">@lang('admin/hardware/table.asset_tag')</th>
            <th class="col-md-3" bSortable="true">@lang('admin/hardware/table.asset_model')</th>
            @if (Setting::getSettings()->display_asset_name)
            <th class="col-md-3" bSortable="true">@lang('general.name')</th>
            <?php $spanrows++; ?>
            @endif
            <th class="col-md-2" bSortable="true">@lang('admin/hardware/table.serial')</th>
            <th class="col-md-2" bSortable="true">@lang('general.status')</th>
            <th class="col-md-2" bSortable="true">@lang('admin/hardware/table.location')</th>
            @if (Input::get('Deployed') && Setting::getSettings()->display_checkout_date)
            <th class="col-md-2" bSortable="true">@lang('admin/hardware/table.checkout_date')</th>
            <?php $spanrows++; ?>
            @endif
            @if (Setting::getSettings()->display_eol)
            <th class="col-md-2">@lang('admin/hardware/table.eol')</th>
            <?php $spanrows++; ?>
            @endif
        </tr>
    </thead>
    <tbody>
        @foreach ($assets as $asset)
    <tr>
            <td>
                @if ($asset->asset_tag)
                <a href="{{ route('asset-details', $asset->id) }}">
                {{{ $asset->asset_tag }}}
                </a>
                @endif
            </td>
            <td>
                @if ($asset->model)
                 {{{ $asset->model->name }}}
                @endif
            </td>

            @if (Setting::getSettings()->display_asset_name)
                <td>{{{ $asset->name }}}</td>
            @endif

            <td>{{{ $asset->serial }}}</td>


                <td>
                    @if (Input::get('Pending'))
                        @lang('general.pending')
                    @elseif (Input::get('RTD'))
                        @lang('general.ready_to_deploy')
                    @elseif (Input::get('Undeployable'))
                        @if ($asset->assetstatus)
                            {{{ $asset->assetstatus->name }}}
                        @endif
                    @else
                        @if ($asset->assigneduser)
                            
                            {{{ $asset->assigneduser->fullName() }}}
                            
                        @else
                            @if ($asset->assetstatus)
                            {{{ $asset->assetstatus->name }}}
                            @endif
                        @endif
                    @endif
                </td>
            <td>
                @if ($asset->assigneduser && $asset->assetloc)
                       {{{ $asset->assetloc->name }}}
                @elseif ($asset->defaultLoc)
                        {{{ $asset->defaultLoc->name }}}
                @endif

            </td>
            @if (Input::get('Deployed') && Setting::getSettings()->display_checkout_date)
                <td>
                    @if (count($asset->assetlog) > 0)
                        {{{ $asset->assetlog->first()->created_at }}}
                    @endif
                </td>
            @endif
            @if (Setting::getSettings()->display_eol)
                <td>
                @if ($asset->model->eol)
                    {{{ $asset->eol_date() }}}
                @endif
                </td>
            @endif
        </tr>
        @endforeach

    </tbody>
</table>
</div>
@else
<div class="col-md-9">
    <div class="alert alert-info alert-block">
        <i class="fa fa-info-circle"></i>
        @lang('general.no_results')
    </div>
</div>

</div>
@endif
@stop
