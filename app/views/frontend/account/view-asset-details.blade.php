@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
@lang('admin/hardware/general.view') {{ $asset->asset_tag }} ::
@parent
@stop

{{-- Page content --}}
@section('content')

<div class="row header">
    <div class="col-md-12">
     <h3 class="name">
        @lang('admin/hardware/general.view')
        {{{ $asset->asset_tag }}}
        @if ($asset->name)
        ({{{ $asset->name }}})
        @endif
    </h3>
</div>
</div>

<div class="user-profile">
<div class="row profile">
<div class="col-md-9 bio">

        @if ($asset->serial)
            <div class="col-md-12" style="padding-bottom: 5px;"><strong>@lang('admin/hardware/form.serial'): </strong>
            <em>{{{ $asset->serial }}}</em></div>

        @endif

        @if ($asset->mac_address!='')
            <div class="col-md-12" style="padding-bottom: 5px;"><strong>@lang('admin/hardware/form.mac_address'):</strong>
            {{{ $asset->mac_address }}}
            </div>
        @endif

        @if ($asset->purchase_date)
            <div class="col-md-12" style="padding-bottom: 5px;"><strong>@lang('admin/hardware/form.date'): </strong>
            {{{ $asset->purchase_date }}} </div>
        @endif

        @if ($asset->order_number)
            <div class="col-md-12" style="padding-bottom: 5px;"><strong>@lang('admin/hardware/form.order'):</strong>
            {{{ $asset->order_number }}} </div>
        @endif

        @if ($asset->supplier_id)
            <div class="col-md-6" style="padding-bottom: 5px;"><strong>@lang('admin/hardware/form.supplier'): </strong>
            {{{ $asset->supplier->name }}}</div>
        @endif

        @if ($asset->warranty_months)
            <div class="col-md-12" style="padding-bottom: 5px;"><strong>@lang('admin/hardware/form.warranty'):</strong>
            {{{ $asset->warranty_months }}}
            @lang('admin/hardware/form.months')
            </div>
            <div class="col-md-12 {{{ $asset->warrantee_expires() < date("Y-m-d H:i:s") ? 'ui-state-highlight' : '' }}}"  style="padding-bottom: 5px;"><strong>@lang('admin/hardware/form.expires'):</strong>
            {{{ $asset->warrantee_expires() }}}</div>
        @endif

        @if ($asset->depreciation)
            <div class="col-md-12" style="padding-bottom: 5px;"><strong>@lang('admin/hardware/form.depreciation'): </strong>
            {{ $asset->depreciation->name }}
                ({{{ $asset->depreciation->months }}}
                @lang('admin/hardware/form.months')
                )</div>
            <div class="col-md-12" style="padding-bottom: 5px;"><strong>@lang('admin/hardware/form.fully_depreciated'): </strong>
             @if ($asset->time_until_depreciated()->y > 0)
                {{{ $asset->time_until_depreciated()->y }}}
                @lang('admin/hardware/form.years'), 
             @endif
           {{{ $asset->time_until_depreciated()->m }}}
            @lang('admin/hardware/form.months')
               ({{{ $asset->depreciated_date()->format('Y-m-d') }}})
             </div>
        @endif


        @if ($asset->model->eol)
            <div class="col-md-12" style="padding-bottom: 5px;">
            <strong>@lang('admin/hardware/form.eol_rate'): </strong>
            {{{ $asset->model->eol }}}
            @lang('admin/hardware/form.months') </div>
            <div class="col-md-12" style="padding-bottom: 5px;">
            <strong>@lang('admin/hardware/form.eol_date'): </strong>
            {{{ $asset->eol_date() }}}
            @if ($asset->months_until_eol())
                 (
                 @if ($asset->months_until_eol()->y > 0) {{{ $asset->months_until_eol()->y }}}
                  @lang('general.years'),
                 @endif

                {{{ $asset->months_until_eol()->m }}}
                @lang('general.months')
                )
            @endif
            </div>
        @endif





<div class="col-md-12">
		<div class="col-md-12">
</div>
        </div>
</div>
        <!-- side address column -->
        <div class="col-md-3 col-xs-12 address pull-right">

        	<!-- Asset notes -->
@if ($asset->notes)

		<h6>@lang('admin/hardware/form.notes'):</h6>
		 <div class="break-word">{{ nl2br(e($asset->notes)) }}</div>

@endif

            @if ($qr_code->display)
            <h6>@lang('admin/hardware/form.qr')</h6>
            <ul>
                <li>
                    <img src="{{{ $qr_code->url }}}" />
                </li>
            <br>
            <a href="{{ route('view/getSingleQR', $asset->id) }}" class="btn btn-primary btn-sm" target="_blank" >Print this QR Code</a>
            </ul>

            @endif
			
			
            @if (($asset->assigneduser) && ($asset->assigned_to > 0) && ($asset->deleted_at==''))
                <h6><br>@lang('admin/hardware/form.checkedout_to')</h6>
                <ul>

                    {{--<li><img src="{{{ $asset->assigneduser->gravatar() }}}" class="img-circle" style="width: 100px; margin-right: 20px;" /><br /><br /></li>--}}
                    <li>{{{ $asset->assigneduser->first_name }}} {{{ $asset->assigneduser->last_name }}}<br /><br /></li>
                    <li><a href="{{ route('view/user', $asset->assigned_to) }}">{{ $asset->assigneduser->fullName() }}</a></li>


                    @if (isset($asset->assetloc->address))
                        <li>{{{ $asset->assetloc->address }}}
                        @if (isset($asset->assetloc->address2)) {{{ $asset->assetloc->address2 }}}
                        @endif
                        </li>
                        @if (isset($asset->assetloc->city))
                            <li>{{{ $asset->assetloc->city }}}, {{{ $asset->assetloc->state }}} {{{ $asset->assetloc->zip }}}</li>
                        @endif

                    @endif

                    @if (isset($asset->assigneduser->email))
                        <li><br /><i class="fa fa-envelope-o"></i> <a href="mailto:{{{ $asset->assigneduser->email }}}">{{{ $asset->assigneduser->email }}}</a></li>
                    @endif

                    @if ((isset($asset->assigneduser->phone)) && ($asset->assigneduser->phone!=''))
                        <li><i class="fa fa-phone"></i> {{{ $asset->assigneduser->phone }}}</li>
                    @endif


                    </ul>

			 @endif

            @if (($asset->status_id ) && ($asset->status_id > 0))
			<!-- Status Info -->

                @if ($asset->assetstatus)
                    <h6><br>{{{ $asset->assetstatus->name }}}
                    @lang('admin/hardware/general.asset')</h6>

                    <ul>
	                    
                    	 @if (($asset->assetstatus->deployable=='1') && ($asset->assigned_to > 0) && ($asset->deleted_at==''))
                         @if (Sentry::getUser()->hasAccess('admin'))
                    	<li><br /><a href="{{ route('checkin/hardware', $asset->id) }}" class="btn btn-primary btn-sm">@lang('admin/hardware/general.checkin')</a></li>
                        @endif
                    	@elseif ((($asset->assetstatus->deployable=='1') &&  (($asset->assigned_to=='') || ($asset->assigned_to==0))) && ($asset->deleted_at==''))
                        @if (Sentry::getUser()->hasAccess('admin'))
                    	<li><br /><a href="{{ route('checkout/hardware', $asset->id) }}" class="btn btn-info btn-sm">@lang('admin/hardware/general.checkout')</a></li>
                        @endif
						@elseif  (($asset->deleted_at!='') && ($asset->model->deleted_at==''))

						<li><br /><a href="{{ route('restore/hardware', $asset->id) }}" class="btn-flat large info ">@lang('admin/hardware/general.restore')</a></li>

                    	@endif

                    </ul>
					@if ($asset->assetstatus->notes)
                    <div class="col-md-12">
						<div class="alert alert-info alert-block">
							<i class="fa fa-info-circle"></i>
							{{{ $asset->assetstatus->notes }}}

						</div>
                    </div>
                    @endif

                 @endif
            @endif

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="uploadFileModal" tabindex="-1" role="dialog" aria-labelledby="uploadFileModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="uploadFileModalLabel">Upload File</h4>
      </div>
      {{ Form::open([
      'method' => 'POST',
      'route' => ['upload/asset', $asset->id],
      'files' => true, 'class' => 'form-horizontal' ]) }}
      <div class="modal-body">

		<p>Allowed filetypes are png, gif, jpg, doc, docx, pdf, and txt.</p>

		 <div class="form-group col-md-12">
		 <div class="input-group col-md-12">
		 	<input class="col-md-12 form-control" type="text" name="notes" id="notes" placeholder="Notes">
		</div>
		</div>
		<div class="form-group col-md-12">
		 <div class="input-group col-md-12">
			{{ Form::file('assetfile[]', ['multiple' => 'multiple']) }}
		</div>
		</div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">@lang('button.cancel')</button>
        <button type="submit" class="btn btn-primary btn-sm">@lang('button.upload')</button>
      </div>
      {{ Form::close() }}
    </div>
  </div>
</div>

@stop
