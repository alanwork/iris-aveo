@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
@lang('admin/divisions/table.title') ::
@parent
@stop

{{-- Page content --}}
@section('content')

<div class="row header">
    <div class="col-md-12">
        <a href="{{ route('create/division') }}" class="btn btn-success pull-right"><i class="fa fa-plus icon-white"></i>  @lang('general.create')</a>
        <h3>@lang('admin/divisions/table.title')</h3>
        @if(Input::get('Deleted'))
            <a href="{{ URL::to('admin/settings/divisions') }}" class="btn btn-default pull-right"><i class="fa fa-trash"></i>  @lang('admin/divisions/general.view_divisions')</a>
        @else
            <a href="{{ URL::to('admin/settings/divisions?Deleted=true') }}" class="btn btn-default pull-right"><i class="fa fa-trash"></i>  @lang('admin/models/general.view_deleted')</a>
        @endif
    </div>
</div>

<div class="row form-wrapper">
<table id="example">
    <thead>
        <tr role="row">
            <th class="col-md-3">@lang('admin/divisions/table.title')</th>
            <th class="col-md-2">@lang('admin/divisions/table.code')</th>
            <!-- <th class="col-md-1">@lang('admin/divisions/table.numassets')</th>
            <th class="col-md-2">@lang('general.depreciation')</th>
            <th class="col-md-2">@lang('general.category')</th>
            <th class="col-md-2">@lang('general.eol')</th> -->
            <th class="col-md-2 actions">@lang('table.actions')</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($divisions as $division)
        <tr>
            <!-- <a href="{{ route('view/division', $division->id) }}"> -->
            <td>{{{ $division->name }}}</a></td>
            <td>{{{ $division->code }}}</td>
            

            <td>
            @if($division->deleted_at=="")
                <a href="{{ route('update/divisions', $division->id) }}" class="btn btn-warning btn-sm" title="Edit Division"><i class="fa fa-pencil icon-white"></i></a>
                <a data-html="false" class="btn delete-asset btn-danger btn-sm" data-toggle="modal" href="{{ route('delete/division', $division->id) }}" data-content="@lang('admin/divisions/message.delete.confirm')"
                data-title="@lang('general.delete')
                {{ htmlspecialchars($division->name) }}?" onClick="return false;" title="Remove Division"><i class="fa fa-trash icon-white"></i></a>
            @else
                <a href="{{ route('restore/division', $division->id) }}" class="btn btn-warning btn-sm" title="Restore Division"><i class="fa fa-recycle icon-white"></i></a>
            @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="table-responsive">{{ $divisions->appends(Request::except('page'))->links() }}</div>
@stop
