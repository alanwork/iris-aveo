@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
@lang('admin/divisions/table.view')
{{{ $model->model_tag }}} ::
@parent
@stop

{{-- Page content --}}
@section('content')


<div class="row header">
    <div class="col-md-12">
        <div class="btn-group pull-right">
           <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">@lang('button.actions')
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                @if ($division->deleted_at=='')
                    <li><a href="{{ route('update/division', $division->id) }}">@lang('admin/divisions/table.edit')</a></li>
                    <li><a href="{{ route('clone/division', $division->id) }}">@lang('admin/divisions/table.clone')</a></li>
                    <li><a href="{{ route('create/division', $division->id) }}">@lang('admin/hardware/form.create')</a></li>
                @else
                    <li><a href="{{ route('restore/division', $division->id) }}">@lang('admin/divisions/general.restore')</a></li>
                @endif
            </ul>
        </div>
        <h3>

            @lang('admin/divisions/table.view') -
            {{{ $division->name }}}

        </h3>
    </div>
</div>


@stop
