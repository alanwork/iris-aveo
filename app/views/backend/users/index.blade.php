@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
@lang('admin/users/table.viewusers') ::
@parent
@stop

{{-- Page content --}}
@section('content')

<div class="row header">
    <div class="col-md-12">
	    
        <a href="{{ route('create/user') }}" class="btn btn-success pull-right"><i class="fa fa-plus icon-white"></i>  @lang('general.create')</a>
        <a href="{{ route('import/user') }}" class="btn btn-default pull-right"><span class="fa fa-upload"></span> @lang('general.upload')</a>
        @if (Input::get('onlyTrashed'))
            <a class="btn btn-default pull-right" href="{{ URL::to('admin/users') }}" style="margin-right: 5px;">Show Current Users</a>
        @else
            <a class="btn btn-default pull-right" href="{{ URL::to('admin/users?onlyTrashed=true') }}" style="margin-right: 5px;"><i class="fa fa-trash"></i> @lang('admin/models/general.view_deleted')</a>
        @endif

        <h3>
        @if (Input::get('onlyTrashed'))
            @lang('general.deleted')
        @else
            @lang('general.current')
        @endif

    </h3>
    </div>
</div>

<div class="row form-wrapper">

@if ($users->getTotal() > 0)
 {{ Form::open([
      'method' => 'GET',
      'route' => ['users'],
      'class' => 'form-horizontal' ]) }}

<div class="row-fluid table users-list">
<div class="table-responsive">
<table id="example">

<div style="float: right">
    @if(Input::get('onlyTrashed'))
        {{ Form::hidden('onlyTrashed','true')}}
    @endif
    {{ Form::text('querySearch', Input::old('querySearch'), array('placeholder' => 'Search by Name..')) }}
    {{ Form::button('<i class="fa fa-search"></i>', array('type' => 'submit', 'name' => 'submit', 'title' => 'Search By Asset Tag')) }}

</div>

    <thead>
        <tr role="row">
            <th class="col-md-3">@lang('admin/users/table.name')</th>
            <th class="col-md-2">@lang('admin/users/table.email')</th>
            <th class="col-md-2">@lang('admin/users/table.manager')</th>
            <th class="col-md-2">@lang('admin/users/table.department')</th>
            <th class="col-md-1">@lang('general.assets')</th>
            <th class="col-md-1">@lang('general.licenses')</th>
            <th class="col-md-1">@lang('admin/users/table.activated')</th>
            <th class="col-md-2 actions">@lang('table.actions')</th>
        </tr>
    </thead>
    <tbody>

        @foreach ($users as $user)
        <tr>
            <td nowrap="nowrap">
            {{--@if ($user->avatar)
				<img src="/uploads/avatars/{{{ $user->avatar }}}" class="img-circle avatar hidden-phone" style="max-width: 45px;" />
			@else
				<img src="{{ $user->gravatar() }}" class="img-circle avatar hidden-phone" style="max-width: 45px;" />
			@endif--}}
            @if(Input::get('onlyTrashed'))
                {{{ $user->fullName() }}}
            @else
                <a href="{{ route('view/user', $user->id) }}" class="name">{{{ $user->fullName() }}}</a>
            @endif
            

            </td>
            <td>{{{ $user->email }}}</td>
            <td>
            @if ($user->manager) {{{ $user->manager->fullName() }}}
            @endif
            </td>
            <td> @if ($user->department) {{{ $user->department->name }}} @endif</td>
            <td>{{{ $user->assets->count() }}}</td>
            <td>{{{ $user->licenses->count() }}}</td>
            <td>{{ $user->isActivated() ? '<i class="fa fa-check"></i>' : ''}}</td>
            
            <td nowrap="nowrap">
	            
	            <!-- If the user account is suspended - show the UNSUSPEND button.  Do NOT evaluate if soft deleted! -->
				@if (is_null($user->deleted_at))
					@if ($user->accountStatus()=='suspended')
			                <a href="{{ route('unsuspend/user', $user->id) }}" class="btn btn-warning btn-sm" title="Unsuspend User"><span class="fa fa-time icon-white"></span></a>
					@endif
				@endif
            

                @if ( ! is_null($user->deleted_at))
                <a href="{{ route('restore/user', $user->id) }}" class="btn btn-warning btn-sm" title="Edit User"><i class="fa fa-recycle icon-white"></i></a>
                @else
                <a href="{{ route('update/user', $user->id) }}" class="btn btn-warning btn-sm" title="Edit User"><i class="fa fa-pencil icon-white"></i></a>

                @if ((Sentry::getId() !== $user->id) && (!Config::get('app.lock_passwords')))
                <a data-html="false" class="btn btn-danger btn-sm" data-toggle="modal" href="{{ route('delete/user', $user->id) }}" data-content="Are you sure you wish to delete this user?" data-title="Delete {{ htmlspecialchars($user->first_name) }}?" title="Remove User"><i class="fa fa-trash icon-white"></i></a>

                @else
                <span class="btn delete-asset btn-danger disabled" title="Restore User"><i class="fa fa-trash icon-white"></i></span>
                @endif
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>
</div>
<!-- {{
    Datatable::table()
        ->addColumn(Lang::get('name'))
        ->addColumn(Lang::get('email'))
        ->addColumn('Assets')
        ->addColumn('Licenses')
        ->addColumn(Lang::get('activated'))
        ->setUrl(route('api.users'))
        ->render()
}} -->

@else


<div class="col-md-6">
    <div class="alert alert-warning alert-block">
        <i class="fa fa-warning"></i>
        @lang('general.no_results')

    </div>
</div>
@endif
<div class="table-responsive">{{ $users->appends(Request::except('page'))->links() }}</div>
 {{ Form::close() }}

@stop
