@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
@lang('admin/hardware/general.delete') ::

@parent
@stop

{{-- Page content --}}
@section('content')
<div class="page-header">

        <div class="pull-right">
            <a href="{{ URL::previous() }}" class="btn-flat gray"><i class="fa fa-arrow-left icon-white"></i>  @lang('general.back')</a>
        </div>
    <h3>@lang('admin/hardware/general.delete')</h3>
</div>

<form class="form-horizontal" method="post" action="" autocomplete="off">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

         <div class="row form-wrapper">
            <div class="col-md-10 column">

            <!-- First Name -->
            <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                <label for="checkout_at" class="col-md-2 control-label">@lang('general.first_name')</label>
                <div class="input-group col-md-3">
                    <p class="form-control-static">{{ $user->first_name }}</p>
                </div>
            </div>             

            <!-- Last Name -->
            <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                <label for="checkout_at" class="col-md-2 control-label">@lang('general.last_name')</label>
                <div class="input-group col-md-3">
                    <p class="form-control-static">{{ $user->last_name }}</p>
                </div>
            </div>              

			<!-- Email -->
            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="checkout_at" class="col-md-2 control-label">@lang('admin/users/table.email')</label>
                <div class="input-group col-md-3">
                    <p class="form-control-static">{{ $user->email }}</p>
                </div>
            </div>  

            <!-- Delete Date -->
            <div class="form-group {{ $errors->has('delete_at') ? ' has-error' : '' }}">
                <label for="checkout_at" class="col-md-2 control-label">@lang('admin/hardware/form.delete_date')</label>
                <div class="input-group col-md-3">
                    <input type="date" class="datepicker form-control" data-date-format="yyyy-mm-dd" placeholder="Date" name="delete_at" id="delete_at" value="">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                {{ $errors->first('delete_at', '<br><span class="alert-msg"><i class="fa fa-times"></i> :message</span>') }}
                </div>
            </div>           

            <!-- Note -->
            <div class="form-group {{ $errors->has('note') ? 'error' : '' }}">
                <label for="note" class="col-md-2 control-label">@lang('admin/hardware/form.justify')</label>
                <i class='fa fa-asterisk'></i>
                <div class="col-md-7">
                    <textarea class="col-md-6 form-control" id="note" name="note"></textarea>
                    {{ $errors->first('note', '<br><span class="alert-msg"><i class="fa fa-times"></i> :message</span>') }}
                </div>
            </div>
        </div>
          </div>

	<!-- Form actions -->
		<div class="form-group">
		<label class="col-md-3 control-label"></label>
			<div class="col-md-7">
				<a class="btn btn-link" href="{{ URL::previous() }}">@lang('button.cancel')</a>
				<button type="submit" class="btn btn-success"><i class="fa fa-check icon-white"></i> @lang('general.delete')</button>
			</div>
		</div>

</form>

@stop
