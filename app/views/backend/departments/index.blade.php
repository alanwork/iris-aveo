@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
@lang('admin/departments/table.title') ::
@parent
@stop

{{-- Page content --}}
@section('content')

<div class="row header">
    <div class="col-md-12">
         
        <a href="{{ route('create/department') }}" class="btn btn-success pull-right"><i class="fa fa-plus icon-white"></i>  @lang('general.create')</a>
        <a href="{{ route('import/department') }}" class="btn btn-default pull-right"><span class="fa fa-upload"></span> @lang('general.upload')</a>
        <h3>@lang('admin/departments/table.title')</h3>
        @if(Input::get('Deleted'))
            <a href="{{ URL::to('admin/settings/departments') }}" class="btn btn-default pull-right"><i class="fa fa-trash"></i>  @lang('admin/departments/general.view_departments')</a>
        @else
            <a href="{{ URL::to('admin/settings/departments?Deleted=true') }}" class="btn btn-default pull-right"><i class="fa fa-trash"></i>  @lang('admin/models/general.view_deleted')</a>
        @endif
    </div>
</div>

<div class="row form-wrapper">
<table id="example">
    <thead>
        <tr role="row">
            <th class="col-md-3">@lang('admin/departments/table.title')</th>
            <th class="col-md-2">@lang('admin/departments/table.code')</th>
            <th class="col-md-2">@lang('admin/departments/table.division')</th>
            <!-- <th class="col-md-1">@lang('admin/departments/table.numassets')</th>
            <th class="col-md-2">@lang('general.depreciation')</th>
            <th class="col-md-2">@lang('general.category')</th>
            <th class="col-md-2">@lang('general.eol')</th> -->
            <th class="col-md-2 actions">@lang('table.actions')</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($departments as $department)
        <tr>
            <!-- <a href="{{ route('view/department', $department->id) }}"> -->
            <td>{{{ $department->name }}}</a></td>
            <td>{{{ $department->code }}}</td>
            <td>
                @if ($department->division) 
                {{{ $department->division->name }}}
                @endif
            </td>
            <td>
            @if($department->deleted_at=="")
                <a href="{{ route('update/departments', $department->id) }}" class="btn btn-warning btn-sm" title="Edit Department"><i class="fa fa-pencil icon-white"></i></a>
                <a data-html="false" class="btn delete-asset btn-danger btn-sm" data-toggle="modal" href="{{ route('delete/department', $department->id) }}" data-content="@lang('admin/departments/message.delete.confirm')"
                data-title="@lang('general.delete')
                {{ htmlspecialchars($department->name) }}?" onClick="return false;" title="Remove Department"><i class="fa fa-trash icon-white"></i></a>
            @else
                <a href="{{ route('restore/department', $department->id) }}" class="btn btn-warning btn-sm" title="Restore Department"><i class="fa fa-recycle icon-white"></i></a>
            @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="table-responsive">{{ $departments->appends(Request::except('page'))->links() }}</div>
@stop
