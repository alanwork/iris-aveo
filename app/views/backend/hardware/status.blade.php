@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
    @if ($asset->id)
    	@lang('admin/hardware/form.updatestatus') ::
    @else
    	@lang('admin/hardware/form.create') ::
    @endif
@parent
@stop

{{-- Page content --}}

@section('content')

<div class="row header">
    <div class="col-md-12">
            <a href="{{ URL::previous() }}" class="btn-flat gray pull-right right"><i class="fa fa-arrow-left icon-white"></i> @lang('general.back')</a>
        <h3>
        @if ($asset->id)
        	@lang('admin/hardware/form.updatestatus')
        @else
            @lang('admin/hardware/form.create')
        @endif
        </h3>
    </div>
</div>

<div class="row form-wrapper">
            <!-- left column -->
            <div class="col-md-12 column">

			 @if ($asset->id)
				 <form class="form-horizontal" method="post" action="{{ route('updatestatus/hardware',$asset->id) }}" autocomplete="off" role="form">
			 @else
				 <form class="form-horizontal" method="post" action="{{ route('savenew/hardware') }}" autocomplete="off" role="form">
			 @endif

            <!-- CSRF Token -->
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" name="model_id" id="model_id" value="{{ $asset->model_id }}" />
            <input type="hidden" name="serial" id="serial" value=" {{ $asset->serial }}" />
            <input type="hidden" name="dept_id" id="dept_id" value=" {{ $asset->dept_id }}" />
            <!-- Asset Tag -->
            <div class="form-group {{ $errors->has('asset_tag') ? ' has-error' : '' }}" >
                <label for="asset_tag" class="col-md-2 control-label">@lang('admin/hardware/form.tag')
                 <i class='fa fa-asterisk'></i></label>
                 </label>
                    <div class="col-md-7">
                        @if  ($asset->id)
                            <input class="form-control" type="text" name="asset_tag" id="asset_tag" value="{{{ Input::old('asset_tag', $asset->asset_tag) }}}" disabled/>
                        @else
                            <input class="form-control" type="text" name="asset_tag" id="asset_tag" value="{{{ Input::old('asset_tag', Asset::autoincrement_asset()) }}}" disabled/>
                        @endif

                        {{ $errors->first('asset_tag', '<span class="alert-msg"><i class="fa fa-times"></i> :message</span>') }}
                    </div>
            </div>

            <!-- Asset Name -->
            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col-md-2 control-label">@lang('admin/hardware/form.name')</label>
                    <div class="col-md-7">
                        <input class="form-control" type="text" name="name" id="name" value="{{{ Input::old('name', $asset->name) }}}" disabled/>
                        {{ $errors->first('name', '<br><span class="alert-msg"><i class="fa fa-times"></i> :message</span>') }}
                    </div>
            </div>

            <!-- QR Code -->
            <div class="form-group {{ $errors->has('qr_code') ? ' has-error' : '' }}">
                <label for="name" class="col-md-2 control-label">@lang('admin/hardware/form.qrcode')</label>
                    <div class="col-md-7">
                        <img src="{{{ $qr_code->url }}}" />
                    </div>
            </div>


            <!-- Status -->
            <?php //var_dump($statuslabel_admin_list); ?>
            @if (Sentry::getUser()->hasAccess('finance'))
            <div class="form-group {{ $errors->has('status_id') ? ' has-error' : '' }}">
                <label for="status_id" class="col-md-2 control-label">@lang('admin/hardware/form.status') <i class='fa fa-asterisk'></i></label>
                    <div class="col-md-7">
                        
                        {{ Form::select('status_id', $statuslabel_list , Input::old('status_id', $asset->status_id), 
                        array('class'=>'select2', 'style'=>'width:350px'))
                         }}
                        {{ $errors->first('status_id', '<br><span class="alert-msg"><i class="fa fa-times"></i> :message</span>') }}
                    </div>
            </div>
            @elseif (Sentry::getUser()->hasAccess('admin'))
            <div class="form-group {{ $errors->has('status_id') ? ' has-error' : '' }}">
                <label for="status_id" class="col-md-2 control-label">@lang('admin/hardware/form.status') <i class='fa fa-asterisk'></i></label>
                    <div class="col-md-7">
                        
                        {{ Form::select('status_id', $statuslabel_admin_list , Input::old('status_id', $asset->status_id), 
                        array('class'=>'select2', 'style'=>'width:350px'))
                         }}
                        {{ $errors->first('status_id', '<br><span class="alert-msg"><i class="fa fa-times"></i> :message</span>') }}
                    </div>
            </div>            
            @endif


            <!-- Form actions -->
                <div class="form-group">
                <label class="col-md-2 control-label"></label>
                    <div class="col-md-7">
                        <a class="btn btn-link" href="{{ URL::to('hardware') }}">@lang('button.cancel')</a>
                        <button type="submit" class="btn btn-success"><i class="fa fa-check icon-white"></i> @lang('general.save')</button>
                    </div>
                </div>

        </form>
    </div>
</div>
@stop
