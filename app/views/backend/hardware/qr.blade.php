@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
    @yield('title0') :: @parent
@stop

{{-- Page content --}}
@section('content')
<!-- MAIN CONTENT -->
<script src="{{ asset('assets/js/webcam/main.js') }}"></script>
<script src="{{ asset('assets/js/webcam/html5-qrcode.min.js') }}"></script>    
    <div id="main_content_wrap" class="outer">
      <section id="main_content" class="inner center">
       

<div class="row header">
	<div class="col-md-12">
		<h3>Asset - QR Reader </h3>
	</div>

</div>

<div class="row form-wrapper" id="reader" style="width: 100%; text-align: center;">
	Allow access to your webcam and place your QR code in front of the camera. The QR data will be displayed automatically<br>
	<video id="html5_qrcode_video" height="200px" width="300px" ></video>
	<canvas id="qr-canvas" width="298px" height="248px" style="display:none;"></canvas>
	<h6 class="center">Result</h6>
	<span id="read" class="center"></span>
	<input type="hidden" id="server_route" value='<?php echo Request::root(); ?>'>
</div>


<!---<h6 class="center">Read Error (Debug only)</h6>
<span class="center">Will constantly show a message, can be ignored</span>
<span id="read_error" class="center"></span>

<br>
<h6 class="center">Video Error</h6>-->
@stop

@stop