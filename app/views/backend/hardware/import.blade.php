@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
Create a Asset ::
@parent
@stop

{{-- Page content --}}
@section('content')

<div class="page-header">
    <h3>
        Import Assets

        <div class="pull-right">
            <a href="{{ route('hardware') }}" class="btn-flat gray pull-right"><i class="fa fa-arrow-circle-left icon-white"></i>  @lang('general.back')</a>

        </div>
    </h3>
</div>

<script type="text/javascript" src="{{ Config::get('app.cdn.default') }}/js/pGenerator.jquery.js"></script>

<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

    <!-- Tabs Content -->
    <div class="tab-content">
        <!-- General tab -->
        <div class="tab-pane active" id="tab-general">
        <br>
			@if (Session::get('message'))
			<p class="alert-danger">
				You have an error in your CSV file:<br />
				{{ Session::get('message') }}
			</p>
			@endif

			<p>
				Upload a CSV file with one or more assets.  Asset Tag will be auto-generated.  The CSV should have the 
				<strong>name</strong> , 
				<strong>Model No</strong> , 
				<strong>Serial</strong> , 
				<strong>Purchase Date</strong> ,  
				<strong>Purchase Cost</strong> ,  
				<strong>Supplier Code</strong> ,  
				<strong>Location Code</strong> , 
				<strong>Dept Code</strong> .
			</p>

            <div class="form-group {{ $errors->first('asset_import_csv', 'has-error') }}">
                <label for="first_name" class="col-sm-2 control-label">@lang('admin/users/general.usercsv')</label>
				<div class="col-sm-5">
					<input type="file" name="asset_import_csv" id="asset_import_csv">
				</div>
            </div>
            
            <!-- Has Headers -->
			<div class="form-group">
				<div class="col-sm-2 ">
				</div>
				<div class="col-sm-5">
					{{ Form::checkbox('has_headers', '1', Input::old('has_headers')) }} This CSV has a header row
				</div>
			</div>

			

        </div>
    </div>

    <!-- Form Actions -->
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-4">
            <a class="btn btn-link" href="{{ route('hardware') }}">@lang('button.cancel')</a>
            <button type="submit" class="btn btn-default">@lang('button.submit')</button>
        </div>
    </div>

</form>

<script>
$(document).ready(function(){

    $('#generate-password').pGenerator({
        'bind': 'click',
        'passwordElement': '#password',
        'displayElement': '#password-display',
        'passwordLength': 10,
        'uppercase': true,
        'lowercase': true,
        'numbers':   true,
        'specialChars': false,

    });
});

</script>
@stop