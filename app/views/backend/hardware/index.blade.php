@extends('backend/layouts/default')

@section('title0')
    @if (Input::get('Pending') || Input::get('Undeployable') || Input::get('Deleted') || Input::get('Requestable') || Input::get('RTD')  || Input::get('Deployed') || Input::get('Archived'))
        @if (Input::get('Pending'))
            @lang('general.pending')
        @elseif (Input::get('RTD'))
            @lang('general.ready_to_deploy')
        @elseif (Input::get('Undeployable'))
            @lang('general.undeployable')
        @elseif (Input::get('Deployed'))
            @lang('general.deployed')
         @elseif (Input::get('Requestable'))
            @lang('admin/hardware/general.requestable')
        @elseif (Input::get('Archived'))
            @lang('general.archived')
         @elseif (Input::get('Deleted'))
            @lang('general.deleted')
        @endif
    @else
            @lang('general.all')
    @endif

    @lang('general.assets')
@stop

{{-- Page title --}}
@section('title')
    @yield('title0') :: @parent
@stop

{{-- Page content --}}
@section('content')

{{ $querySearch }}
<div class="row header">
    <div class="col-md-12">
        <a href="{{ route('create/hardware') }}" class="btn btn-success pull-right" ><i class="fa fa-plus icon-white"></i> @lang('general.create')</a>
        @if (!Input::get('Deleted'))
        @if(Sentry::getUser()->hasAccess('finance') || Sentry::getUser()->hasAccess('admin'))
        <a href="{{ route('import/hardware') }}" class="btn btn-default pull-right"><span class="fa fa-upload"></span> @lang('general.upload')</a>
        @endif
        @endif
        <h3>@yield('title0')</h3>
    </div>
</div>

<div class="row form-wrapper">
    
<?php $spanrows = 8; ?>

{{-- 
@if ($assets->count() > 0)
--}}

 {{ Form::open([
      'method' => 'GET',
      'route' => ['hardware/bulkedit'],
	  'class' => 'form-horizontal' ]) }}



<div class="table-responsive">
<table id="example">
<div style="float: right">
    Status : 
    {{ Form::select('statuslabel_list', $statuslabel_list, $selectedstatusid)}}
    &nbsp;
    {{ Form::text('querySearch', Input::old('querySearch'), array('placeholder' => 'Search by Asset Tag..')) }}
    {{ Form::button('<i class="fa fa-search"></i>', array('type' => 'submit', 'name' => 'submit', 'title' => 'Search By Asset Tag')) }}

</div>

    <thead>
        <tr role="row">
	        <th class="col-md-1" bSortable="false"></th>
            <th class="col-md-1" bSortable="true">@lang('admin/hardware/table.asset_tag')</th>
            <th class="col-md-3" bSortable="true">@lang('admin/hardware/table.asset_model')</th>
            @if (Setting::getSettings()->display_asset_name)
                <th class="col-md-3" bSortable="true">@lang('general.name')</th>
                <?php $spanrows++; ?>
            @endif
            <th class="col-md-2" bSortable="true">@lang('admin/hardware/table.serial')</th>
            <th class="col-md-2" bSortable="true">@lang('general.status')</th>


            <th class="col-md-2" bSortable="true">@lang('admin/hardware/table.purchase_cost')</th>

            @if(Sentry::getUser()->hasAccess('admin') || Sentry::getUser()->hasAccess('finance') || Sentry::getUser()->hasAccess('superadmin'))
                <th class="col-md-2" bSortable="true">@lang('general.updatestatus')</th>
            @endif
                
            {{--<th class="col-md-2" bSortable="true">@lang('admin/hardware/table.location')</th>--}}
            @if (Input::get('Deployed') && Setting::getSettings()->display_checkout_date)
                
                 <th class="col-md-2" bSortable="true">@lang('admin/hardware/table.checkout_date')</th>
                 
                <?php $spanrows++; ?>


            @endif
            @if (Setting::getSettings()->display_eol)
           	<th class="col-md-2">@lang('admin/hardware/table.eol')</th>
           	<?php $spanrows++; ?>
            @endif
            
                <th class="col-md-1" <?php if (!Sentry::getUser()->hasAccess('admin')) echo 'style="display: none"'; ?>>@lang('admin/hardware/table.change')</th>
                <th class="col-md-2" bSortable="true">@lang('admin/hardware/table.checkout_site')</th>
                
            @if(Sentry::getUser()->hasAccess('finance'))
            <th class="col-md-2 actions" bSortable="false">@lang('table.actions')</th>
            @endif
        </tr>
    </thead>
    <tfoot>
    <tr>
      <td colspan="{{{ $spanrows }}}"><button class="btn btn-default" id="bulkEdit" disabled>Bulk Edit</button></td>
    </tr>
  </tfoot>
    <tbody>

        @foreach ($assets as $asset)
        <tr>
	        <td><input type="checkbox" name="edit_asset[{{ $asset->id }}]" class="one_required"></td>
            <td>
                @if ($asset->asset_tag)
                <a href="{{ route('view/hardware', $asset->id) }}"> {{{ $asset->asset_tag }}} </a>
                @endif
            </td>
            <td>
                @if ($asset->model)
                <a href="{{ route('view/model', $asset->model->id) }}"> {{{ $asset->model->name }}} </a>
                @endif
            </td>

            @if (Setting::getSettings()->display_asset_name)
                <td><a href="{{ route('view/hardware', $asset->id) }}">{{{ $asset->name }}}</a></td>
            @endif

            <td>{{{ $asset->serial }}}</td>


                <td>
                    @if (Input::get('Pending'))
                        @lang('general.pending')
                    @elseif (Input::get('RTD'))
                        @lang('general.ready_to_deploy')
                    @elseif (Input::get('Undeployable'))
                        @if ($asset->assetstatus)
                        	{{{ $asset->assetstatus->name }}}
                        @endif
                    @else

                    	@if ($asset->assigneduser)
                            @if (Sentry::getUser()->hasAccess('admin') || Sentry::getUser()->hasAccess('superuser'))
							<a href="{{ route('view/user', $asset->assigned_to) }}">
							{{{ $asset->assigneduser->fullName() }}}
							</a>
                            @elseif (Sentry::getUser()->hasAccess('finance'))
                            {{{ $asset->assigneduser->fullName() }}}
                            @endif
                        @elseif ($asset->site_id)
                            {{{ $asset->site->name }}}
						@else
							@if ($asset->assetstatus)
                        	{{{ $asset->assetstatus->name }}}
                        	@endif
						@endif
                    @endif
                </td> 
                <td>
            @if(Sentry::getUser()->hasAccess('finance') || Sentry::getUser()->hasAccess('superadmin') || ( $asset->user_id == Sentry::getUser()->id ))                    
                    {{ number_format($asset->purchase_cost,2) }}
            @else
                
            @endif
                </td>               
                @if(Sentry::getUser()->hasAccess('admin') || Sentry::getUser()->hasAccess('finance'))
                <td style=" text-align: center;">
                    @if((Sentry::getUser()->hasAccess('admin')) && ( $asset->assetstatus->id == 1))
                    <a href="{{ route('updatestatus/hardware', $asset->id) }}" class="btn btn-primary btn-sm">@lang('general.update')</a>
                    @elseif (Sentry::getUser()->hasAccess('finance') )
                    <a href="{{ route('updatestatus/hardware', $asset->id) }}" class="btn btn-primary btn-sm">@lang('general.update')</a>
                    @else
                        Cannot Update
                    @endif
                </td>
                @endif
               
            {{--
            <td>
                @if ($asset->assigneduser && $asset->assetloc)
                    	<a href="{{ route('update/location', $asset->assetloc->id) }}">{{{ $asset->assetloc->name }}}</a>
                @elseif ($asset->defaultLoc)
                    	<a href="{{ route('update/location', $asset->defaultLoc->id) }}">{{{ $asset->defaultLoc->name }}}</a>

                @endif

            </td>
            --}}
			@if (Input::get('Deployed') && Setting::getSettings()->display_checkout_date)
	            <td>
	                @if (count($asset->assetlog) > 0)
                        {{{ $asset->assetlog->first()->created_at }}}
	                @endif
	            </td>
            @endif
            @if (Setting::getSettings()->display_eol)
				<td>
				@if ($asset->model->eol)
					{{{ $asset->eol_date() }}}
				@endif
           		</td>
            @endif


            <td <?php if (!Sentry::getUser()->hasAccess('admin')) echo 'style="display: none"'; ?> >
            @if (($asset->site_id == ''))
                @if (($asset->assetstatus) && (($asset->assetstatus->deployable == 1 ) && ($asset->deleted_at=='')) )
    				@if (($asset->assigned_to !='') && ($asset->assigned_to > 0))

    					<a href="{{ route('checkin/hardware', $asset->id) }}" class="btn btn-primary btn-sm">@lang('general.checkin')</a>
    				@else
    					<a href="{{ route('checkout/hardware', $asset->id) }}" class="btn btn-info btn-sm">@lang('general.checkout')</a>
    				@endif
                @endif
            @else
                @lang('admin/hardware/table.checkedout_site')
            @endif
            </td>

            <td <?php if (!Sentry::getUser()->hasAccess('admin')) echo 'style="display: none"'; ?> >
            @if (($asset->assigned_to == ''))
                @if (($asset->assetstatus) && (($asset->assetstatus->deployable == 1 ) && ($asset->deleted_at=='')) && ($asset->assigned_to == ''))
                    @if (($asset->site_id !='') && ($asset->site_id > 0))

                        <a href="{{ route('checkinSite/hardware', $asset->id) }}" class="btn btn-primary btn-sm">@lang('general.checkin')</a>
                    @else
                        <a href="{{ route('checkoutSite/hardware', $asset->id) }}" class="btn btn-info btn-sm">@lang('general.checkout')</a>
                    @endif
                @endif
            @else
                @lang('admin/hardware/table.checkedout_user')
            @endif
            </td>            
            @if(Sentry::getUser()->hasAccess('finance'))
            <td nowrap="nowrap">

            @if ($asset->deleted_at=='')
             	<a href="{{ route('update/hardware', $asset->id) }}" class="btn btn-warning btn-sm" title="Edit Asset"><i class="fa fa-pencil icon-white"></i></a>
            	 <a  class="btn btn-danger btn-sm"  href="{{ route('delete/hardware', $asset->id) }}" data-content="@lang('admin/hardware/message.delete.confirm')"
                data-title="@lang('general.delete')
                 {{ htmlspecialchars($asset->asset_tag) }}?" title="Remove Asset"><i class="fa fa-trash icon-white"></i></a>
        	@else
                @if ($asset->model->deleted_at=='')
        		 <a href="{{ route('restore/hardware', $asset->id) }}" class="btn delete-asset btn-warning btn-sm" onClick="return false;" data-content="@lang('admin/hardware/message.restore.confirm')"
                data-title="@lang('general.restore')
                 {{ htmlspecialchars($asset->asset_tag) }}?" title="Restore Asset"><i class="fa fa-recycle icon-white"></i></a>
                @endif
        	@endif

            </td>
            @endif
        </tr>
        @endforeach

    </tbody>
    
</table>
<div class="table-responsive">{{ $assets->appends(Request::except('page'))->links() }}</div>
 {{ Form::close() }}

</div>

<?php //echo $assets->links(); ?>
<script>
	$(function() {
		
	    $('input.one_required').change(function() {
		    
	        var check_checked = $('input.one_required:checked').length;
	        console.warn(check_checked);
	        if (check_checked > 0) {
	            $('#bulkEdit').removeAttr('disabled');
	        }
	        else {
	            $('#bulkEdit').attr('disabled', 'disabled');
	        }
	    });
	});
</script>
{{-- 
@else
<div class="col-md-9">
    <div class="alert alert-info alert-block">
        <i class="fa fa-info-circle"></i>
        @lang('general.no_results')
    </div>
</div>

</div>

@endif
--}}

@stop
