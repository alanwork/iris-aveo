@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
@lang('admin/hardware/general.dopage') {{ $do_overview->delivery_order }} ::
@parent
@stop

{{-- Page content --}}
@section('content')

<div class="row header">
    <div class="col-md-12">
     <h3 class="name">
        @lang('admin/hardware/general.dopage')
        {{{ $do_overview->delivery_order }}}
    </h3>
    <a href="{{ URL::previous() }}" class="btn-flat gray pull-right"><i class="fa fa-arrow-left icon-white"></i>  @lang('general.back')</a>        </div>
</div>
</div>

<div class="user-profile">
<div class="row profile">
<div class="col-md-12 bio">

        <!-- checked out assets table -->

        <table class="table table-hover table-fixed break-word">
            <thead>
                <tr>
                    <th class="col-md-3">@lang('general.name')</th>
                    <th class="col-md-3">@lang('general.asset_tag')</th> 
                    <th class="col-md-2"><span class="line"></span>@lang('general.date')</th>
                    <th class="col-md-2"><span class="line"></span>@lang('admin/hardware/table.recipient')</th>
                    <th class="col-md-2"><span class="line"></span>@lang('admin/hardware/table.note')</th>
                </tr>
            </thead>
            <tbody>
            @if (count($do) > 0)
                @foreach ($do as $log)
                 @if ($log->assetlog)
                <tr>   
                    <td>{{{ $log->assetlog->name }}}</td>
                    <td>{{{ $log->assetlog->asset_tag }}}</td>
                    <td>{{{ $log->created_at }}}</td>
                    <td>
                            @if($log->userlog)
                            @if (($log->userlog->deleted_at=='') && ($log->checkedout_to != '') && ($log->checkedout_to!=0))
                                <a href="{{ route('view/user', $log->checkedout_to) }}">
                                {{{ $log->userlog->fullName() }}}
                                 </a>
                            @endif

                            @elseif ($log->sitelog)
                                {{{ $log->sitelog->name }}}
                            @endif

                    </td>
                    <td>
                        @if ($log->note) {{{ $log->note }}}
                        @endif
                    </td>
                </tr>
                
                @endif
                @endforeach
                @endif
            </tbody>
        </table>


        </div>
</div>

<p></p><br>

<div class="row profile">
<div class="col-md-12 bio">

        <!-- checked out assessories table -->

        <table class="table table-hover table-fixed break-word">
            <thead>
                <tr>
                    <th class="col-md-3">@lang('general.accessory') @lang('general.name')</th>
                    <th class="col-md-2"><span class="line"></span>@lang('general.quantity')</th>
                    <th class="col-md-2"><span class="line"></span>@lang('general.date')</th>
                    <th class="col-md-2"><span class="line"></span>@lang('admin/hardware/table.recipient')</th>
                    <th class="col-md-2"><span class="line"></span>@lang('admin/hardware/table.note')</th>
                </tr>
            </thead>
            <tbody>
            @if (count($do) > 0)
                @foreach ($do as $log)
                 @if($log->accessorylog)
                <tr>   
                    <td>
                        {{{ $log->accessorylog->name }}}
                    </td>
                    <td>
                    @if ($log->accessory_quantity) 
                        {{{ $log->accessory_quantity }}}
                    @else
                        1
                    @endif                    
                    </td>
                    <td>{{{ $log->created_at }}}</td> 
                    <td>
                            @if($log->userlog)
                                @if (($log->userlog->deleted_at=='') && ($log->checkedout_to != '') && ($log->checkedout_to!=0))
                                    <a href="{{ route('view/user', $log->checkedout_to) }}">
                                    {{{ $log->userlog->fullName() }}}
                                     </a>
                                @endif
                            

                            @elseif ($log->sitelog)
                                {{{ $log->sitelog->name }}}
                            @endif
                    </td>
                    <td>
                        @if ($log->note) {{{ $log->note }}}
                        @endif
                    </td>
                </tr>
                
                @endif
                @endforeach
                @endif
            </tbody>
        </table>
        </div>

        <a href="{{ route('getDOPrint', $do_overview->delivery_order) }}" class="btn btn-success" target="blank"><i class="fa fa-check icon-white"></i> @lang('general.print_do')</a>        
    </div>


</div>

@stop
