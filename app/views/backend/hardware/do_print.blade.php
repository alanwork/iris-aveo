
{{-- Page title --}}
@section('title')
@lang('admin/hardware/general.dopage') {{ $do_overview->delivery_order }} ::
@parent
@stop

{{-- Page content --}}
<html lang="en">
    <head>

        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8" />
        <title>
            @section('title')
             {{{ Setting::getSettings()->site_name }}}
            @show
        </title>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">


         <!-- bootstrap -->
        <link href="{{ asset('assets/css/bootstrap/bootstrap.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/bootstrap/bootstrap-overrides.css') }}" type="text/css" rel="stylesheet" />

        <!-- global styles -->
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/compiled/layout.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/compiled/elements.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/compiled/icons.css') }}">

        <!-- libraries -->
        <link rel="stylesheet" href="{{ asset('assets/css/lib/jquery-ui-1.10.2.custom.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('assets/css/lib/font-awesome.min.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('assets/css/lib/icon.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('assets/css/lib/morris.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('assets/css/lib/select2.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('assets/css/lib/bootstrap.datepicker.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('assets/css/compiled/index.css') }}" type="text/css" media="screen" />
        <link rel="stylesheet" href="{{ asset('assets/css/compiled/user-list.css') }}" type="text/css" media="screen" />
        <link rel="stylesheet" href="{{ asset('assets/css/compiled/user-profile.css') }}" type="text/css" media="screen" />
        <link rel="stylesheet" href="{{ asset('assets/css/compiled/form-showcase.css') }}" type="text/css" media="screen" />
        <link rel="stylesheet" href="{{ asset('assets/css/lib/jquery.dataTables.css') }}" type="text/css" media="screen" />
        <link rel="stylesheet" href="{{ asset('assets/css/compiled/dataTables.responsive.css') }}" type="text/css" media="screen" />

        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/compiled/print.css') }}" media="print" />



        <!-- open sans font -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

        <!-- global header javascripts -->
        <script src="{{ asset('assets/js/jquery-latest.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/js/dataTables.responsive.js') }}"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

        <script>        
            window.snipeit = {
                settings: {
                    "per_page": {{{ Setting::getSettings()->per_page }}}
                }
            };
        </script>



        @if (Setting::getSettings()->load_remote=='1')
        <!-- open sans font -->
        <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        @endif

        <!--[if lt IE 9]>
          <script src="{{ asset('assets/js/html5.js') }}"></script>
        <![endif]-->

        <style>

        @section('styles')
        h3 {
            margin-left: -15px;
        }

        @show

        @if (Setting::getSettings()->header_color)
            .navbar-inverse {
                background-color: {{{ Setting::getSettings()->header_color }}};
                background: -webkit-linear-gradient(top,  {{{ Setting::getSettings()->header_color }}} 0%,{{{ Setting::getSettings()->header_color }}} 100%);
                border-color: {{{ Setting::getSettings()->header_color }}};
            }
        @endif

        </style>


    </head>

    <body style="margin-left: 10px;">

<img style="padding: 0 11px" src="{{URL::to('/')}}/uploads/{{{ Setting::getSettings()->logo }}}">
<div style="width:200px;">
NuSuara Technologies Sdn. Bhd. (599840 –M)
Unit No.2-19-01 Block 2
VSQ@PJ City Centre
Jalan Utara
46200 Petaling Jaya
Selangor Darul Ehsan
Malaysia
</div>
<br>
<div>
TEL +603-74518080
</div >
<br>
<div class="row header">
    <div class="col-md-12">
     <h3 class="name" style="margin-left: 0px;">
        @lang('admin/hardware/general.dopage') {{{ $do_overview->delivery_order }}}
    </h3>
</div>
</div>

<div class="user-profile">
<div class="row profile">
<div class="col-md-12 bio">

        <!-- checked out assets table -->

        <table class="table table-hover table-fixed break-word">
            <thead>
                <tr>
                    <th class="col-md-3">@lang('general.asset_tag')</th>
                    <th class="col-md-3">@lang('general.name')</th>
                    <th class="col-md-2"><span class="line"></span>@lang('general.date')</th>
                    <th class="col-md-2"><span class="line"></span>@lang('admin/hardware/table.recipient')</th>
                    <th class="col-md-2"><span class="line"></span>@lang('admin/hardware/table.note')</th>
                </tr>
            </thead>
            <tbody>
            @if (count($do) > 0)
                @foreach ($do as $log)
                 @if ($log->assetlog)
                <tr>   
                    <td>
                        {{{ $log->assetlog->asset_tag }}}
                    </td>
                    <td>{{{ $log->assetlog->name }}}</td>
                    <td>{{{ $log->created_at }}}</td>
                    <td>
                            @if($log->userlog)
                            @if (($log->userlog->deleted_at=='') && ($log->checkedout_to != '') && ($log->checkedout_to!=0))
                                <a href="{{ route('view/user', $log->checkedout_to) }}">
                                {{{ $log->userlog->fullName() }}}
                                 </a>
                            @endif

                            @elseif ($log->sitelog)
                                {{{ $log->sitelog->name }}}
                            @endif
                    </td>
                    <td>
                        @if ($log->note) {{{ $log->note }}}
                        @endif
                    </td>
                </tr>
                
                @endif
                @endforeach
                @endif
            </tbody>
        </table>


        </div>
</div>

<p></p><br>

<div class="row profile">
<div class="col-md-12 bio">

        <!-- checked out accessories table -->

        <table class="table table-hover table-fixed break-word">
            <thead>
                <tr>
                    <th class="col-md-3">@lang('general.accessory') @lang('general.name')</th>
                    <th class="col-md-2"><span class="line"></span>@lang('general.quantity')</th>
                    <th class="col-md-2"><span class="line"></span>@lang('general.date')</th>
                    <th class="col-md-2"><span class="line"></span>@lang('admin/hardware/table.recipient')</th>
                    <th class="col-md-2"><span class="line"></span>@lang('admin/hardware/table.note')</th>
                </tr>
            </thead>
            <tbody>
            @if (count($do) > 0)
                @foreach ($do as $log)
                 @if($log->accessorylog)
                <tr>   
                    <td>
                        {{{ $log->accessorylog->name }}}
                    </td>
                    <td>
                    @if ($log->accessory_quantity) 
                        {{{ $log->accessory_quantity }}}
                    @else
                        1
                    @endif    
                    </td>
                    <td>{{{ $log->created_at }}}</td>
                    <td>
                            @if($log->userlog)
                                @if (($log->userlog->deleted_at=='') && ($log->checkedout_to != '') && ($log->checkedout_to!=0))
                                    
                                    {{{ $log->userlog->fullName() }}}
                                    
                                @endif
                            

                            @elseif ($log->sitelog)
                                {{{ $log->sitelog->name }}}
                            @endif
                    </td>
                    <td>
                        @if ($log->note) {{{ $log->note }}}
                        @endif
                    </td>
                </tr>
                
                @endif
                @endforeach
                @endif
            </tbody>
        </table>
        </div>       
    </div>


</div>

    <!-- scripts -->
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.knob.js') }}"></script>
    <script src="{{ asset('assets/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.uniform.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.datepicker.js') }}"></script>
    <script src="{{ asset('assets/js/theme.js') }}"></script>
    <script src="{{ asset('assets/js/snipeit.js') }}"></script>

     <script>
    $(document).ready(function() {
        $('table.display').dataTable();
    });

    $(window).load(function() {
        // Animate loader off screen
        $("#content").fadeOut("slow");;
    });

    $(window).on('beforeunload', function(){
         console.log("beforeUnload event!");
         $("#content").delay(9000).fadeOut(5000);
     }); 

    </script>

    </body>
</html>
