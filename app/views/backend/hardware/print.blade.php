@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')

    	@lang('admin/hardware/form.print') ::
@parent
@stop

{{-- Page content --}}

@section('content')

<div class="row header">
    <div class="col-md-12">
            <a href="{{ URL::previous() }}" class="btn-flat gray pull-right right"><i class="fa fa-arrow-left icon-white"></i> @lang('general.back')</a>
        <h3>
            @lang('admin/hardware/form.print')
        </h3>
    </div>
</div>

<div class="row form-wrapper">
            <!-- left column -->
            <div class="col-md-12 column">

				 <form class="form-horizontal" method="post" action="{{ route('print/hardware') }}" autocomplete="off" role="form" target="_blank">

            <!-- Department -->
            <div class="form-group {{ $errors->has('dept_id') ? ' has-error' : '' }}">
                <label for="parent" class="col-md-2 control-label">@lang('admin/hardware/form.department')
                 <i class='fa fa-asterisk'></i></label>
                 </label>
                <div class="col-md-7">
                        {{ Form::select('dept_id', $dept_list , '', array('class'=>'select2', 'style'=>'min-width:400px')) }}
                    {{ $errors->first('dept_id', '<br><span class="alert-msg"><i class="fa fa-times"></i> :message</span>') }}
                </div>
            </div>


            <!-- Model -->
            <div class="form-group {{ $errors->has('model_id') ? ' has-error' : '' }}">
                <label for="parent" class="col-md-2 control-label">@lang('admin/hardware/form.model')
                 <i class='fa fa-asterisk'></i></label>
                 </label>
                <div class="col-md-7">
                        {{ Form::select('model_id', $model_list , '', array('class'=>'select2', 'style'=>'min-width:400px')) }}
                    {{ $errors->first('model_id', '<br><span class="alert-msg"><i class="fa fa-times"></i> :message</span>') }}
                </div>
            </div>

            <!-- Purchase Date -->
            <div class="form-group {{ $errors->has('purchase_date') ? ' has-error' : '' }}">
                <label for="purchase_date" class="col-md-2 control-label">@lang('admin/hardware/form.date')</label>
                <div class="input-group col-md-3">
                    <input type="date" class="datepicker form-control" data-date-format="yyyy-mm-dd" placeholder="Select Date" name="purchase_date" id="purchase_date" value="">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                {{ $errors->first('purchase_date', '<br><span class="alert-msg"><i class="fa fa-times"></i> :message</span>') }}
                </div>
            </div>

            <!-- Supplier -->
            <div class="form-group {{ $errors->has('supplier_id') ? ' has-error' : '' }}">
                <label for="supplier_id" class="col-md-2 control-label">@lang('admin/hardware/form.supplier')</label>
                <div class="col-md-7">
                    {{ Form::select('supplier_id', $supplier_list ,'' , array('class'=>'select2', 'style'=>'min-width:350px')) }}
                    {{ $errors->first('supplier_id', '<br><span class="alert-msg"><i class="fa fa-times"></i> :message</span>') }}
                </div>
            </div>

            <!-- Status -->
            <div class="form-group {{ $errors->has('status_id') ? ' has-error' : '' }}">
                <label for="status_id" class="col-md-2 control-label">@lang('admin/hardware/form.status') <i class='fa fa-asterisk'></i></label>
                    <div class="col-md-7">
                        
                        {{ Form::select('status_id', $statuslabel_list ,'' , array('class'=>'select2', 'style'=>'width:350px'))
                         }}
                        {{ $errors->first('status_id', '<br><span class="alert-msg"><i class="fa fa-times"></i> :message</span>') }}
                    </div>
            </div>

            <!-- Default Location -->
            <div class="form-group {{ $errors->has('status_id') ? ' has-error' : '' }}">
                <label for="status_id" class="col-md-2 control-label">@lang('admin/hardware/form.default_location')</label>
                    <div class="col-md-7">
                        {{ Form::select('rtd_location_id', $location_list ,'', array('class'=>'select2', 'style'=>'width:350px')) }}
                        {{ $errors->first('status_id', '<br><span class="alert-msg"><i class="fa fa-times"></i> :message</span>') }}
                    </div>
            </div>


            <!-- Form actions -->
                <div class="form-group">
                <label class="col-md-2 control-label"></label>
                    <div class="col-md-7">
                        <a class="btn btn-link" href="{{ URL::previous() }}">@lang('button.cancel')</a>
                        <button type="submit" class="btn btn-success"><i class="fa fa-check icon-white"></i> @lang('general.print')</button>
                    </div>
                </div>

        </form>
    </div>
</div>
<script language="javascript">
    $('#select_category').click(function(){
        alert($('#select_category').val());
        
    });
</script>
@stop
