@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')

    	@lang('admin/hardware/form.print_history') ::
@parent
@stop

{{-- Page content --}}

@section('content')

<div class="row header">
    <div class="col-md-12">
            <a href="{{ URL::previous() }}" class="btn-flat gray pull-right right"><i class="fa fa-arrow-left icon-white"></i> @lang('general.back')</a>
        <h3>
            @lang('admin/hardware/form.print_history')
        </h3>
    </div>
</div>

<div class="row form-wrapper">
            <!-- left column -->
            <div class="col-md-12 column">

                 <!-- <form class="form-horizontal" method="post" action="{{ route('reports/history') }}" autocomplete="off" role="form" target="_blank"> -->
				 <form class="form-horizontal" method="post" action="{{ route('reports/history') }}" autocomplete="off" role="form">

            <!-- Start Date -->
            <div class="form-group {{ $errors->has('start_date') ? ' has-error' : '' }}">
                <label for="start_date" class="col-md-2 control-label">@lang('admin/hardware/form.start_date')</label>
                <div class="input-group col-md-3">
                    <input type="date" class="datepicker form-control" data-date-format="yyyy-mm-dd" placeholder="Select Date" name="start_date" id="start_date" value="">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                {{ $errors->first('start_date', '<br><span class="alert-msg"><i class="fa fa-times"></i> :message</span>') }}
                </div>
            </div>

            <!-- End Date -->
            <div class="form-group {{ $errors->has('end_date') ? ' has-error' : '' }}">
                <label for="end_date" class="col-md-2 control-label">@lang('admin/hardware/form.end_date')</label>
                <div class="input-group col-md-3">
                    <input type="date" class="datepicker form-control" data-date-format="yyyy-mm-dd" placeholder="Select Date" name="end_date" id="end_date" value="">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                {{ $errors->first('end_date', '<br><span class="alert-msg"><i class="fa fa-times"></i> :message</span>') }}
                </div>
            </div>

            <!-- Form actions -->
                <div class="form-group">
                <label class="col-md-2 control-label"></label>
                    <div class="col-md-7">
                        <a class="btn btn-link" href="{{ URL::previous() }}">@lang('button.cancel')</a>
                        <button type="submit" class="btn btn-success"><i class="fa fa-check icon-white"></i> @lang('general.print')</button>
                    </div>
                </div>

        </form>
    </div>
</div>
<script language="javascript">
    $('#select_category').click(function(){
        alert($('#select_category').val());
        
    });
</script>
@stop
