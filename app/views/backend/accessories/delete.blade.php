@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
     @lang('admin/hardware/general.delete') ::
@parent
@stop

{{-- Page content --}}
@section('content')

<div class="row header">
    <div class="col-md-12">
        <a href="{{ URL::previous() }}" class="btn-flat gray pull-right">
        <i class="fa fa-arrow-left icon-white"></i>  @lang('general.back')</a>
        <h3> @lang('admin/hardware/general.delete')</h3>
    </div>
</div>

<div class="row form-wrapper">
<!-- left column -->
<div class="col-md-10 column">

<form class="form-horizontal" method="post" action="" autocomplete="off">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

            @if ($accessory->name)
            <!-- accessory name -->
            <div class="form-group">
            <label class="col-sm-2 control-label">@lang('admin/accessories/general.accessory_name')</label>
                <div class="col-md-6">
                  <p class="form-control-static">{{{ $accessory->name }}}</p>
                </div>
            </div>
            @endif

			<!-- Delete Date -->
            <div class="form-group {{ $errors->has('delete_at') ? ' has-error' : '' }}">
                <label for="checkout_at" class="col-md-2 control-label">@lang('admin/hardware/form.delete_date')</label>
                <div class="input-group col-md-3">
                    <input type="date" class="datepicker form-control" data-date-format="yyyy-mm-dd" placeholder="Date" name="delete_at" id="delete_at" value="{{{ Input::old('checkout_at', date('Y-m-d')) }}}">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                {{ $errors->first('delete_at', '<br><span class="alert-msg"><i class="fa fa-times"></i> :message</span>') }}
                </div>
            </div>           

            <!-- Note -->
            <div class="form-group {{ $errors->has('note') ? 'error' : '' }}">
                <label for="note" class="col-md-2 control-label">@lang('admin/hardware/form.justify')</label>
                <i class='fa fa-asterisk'></i>
                <div class="col-md-7">
                    <textarea class="col-md-6 form-control" id="note" name="note">{{{ Input::old('note', $accessory->note) }}}</textarea>
                    {{ $errors->first('note', '<br><span class="alert-msg"><i class="fa fa-times"></i> :message</span>') }}
                </div>
            </div>

            <!-- Form actions -->
            <div class="form-group">
            <label class="col-md-2 control-label"></label>
                <div class="col-md-7">
                    <a class="btn btn-link" href="{{ URL::previous() }}"> @lang('button.cancel')</a>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check icon-white"></i> @lang('general.delete')</button>
                </div>
            </div>



</form>

</div>
</div>
@stop
