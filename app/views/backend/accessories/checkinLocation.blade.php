@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
     @lang('admin/hardware/general.checkin') ::
@parent
@stop

{{-- Page content --}}
@section('content')

<div class="row header">
    <div class="col-md-12">
        <a href="{{ URL::previous() }}" class="btn-flat gray pull-right"><i class="fa fa-arrow-left icon-white"></i>  @lang('general.back')</a>
        <h3> @lang('general.checkin')</h3>
    </div>
</div>

<div class="row form-wrapper">
<!-- left column -->
<div class="col-md-10 column">

<form class="form-horizontal" method="post" action="" autocomplete="off">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

            

			@if ($accessory->name)
            <!-- accessory name -->
            <div class="form-group">
            <label class="col-sm-2 control-label">@lang('admin/hardware/form.name')</label>
                <div class="col-md-6">
                  <p class="form-control-static"></p>{{{ $accessory->name }}}
                </div>
            </div>
            @endif

            <!-- Site -->

            <div class="form-group {{ $errors->has('assigned_to') ? ' has-error' : '' }}">
                <label for="assigned_to" class="col-sm-2 control-label">@lang('admin/hardware/form.default_location')
                 <i class='icon-asterisk'></i></label>
                <div class="col-md-6">
                  <p class="form-control-static">{{{ $sitename->name }}}</p>
                </div>                 
            </div>            

            <!-- Quantity -->
            <div class="form-group">
            <label class="col-sm-2 control-label">@lang('admin/accessories/general.quantity')</label>
                <div class="col-md-7">
                 <input class="form-control" type="text" name="quantity" id="quantity" value="0" />
            {{ $errors->first('quantity', '<span class="alert-msg"><i class="fa fa-times"></i> :message</span>') }}
                </div>
            </div>             

            <!-- Note -->
            <div class="form-group {{ $errors->has('note') ? 'error' : '' }}">
                <label for="note" class="col-md-2 control-label">@lang('admin/hardware/form.notes')</label>
                <div class="col-md-7">
                    <textarea class="col-md-6 form-control" id="note" name="note">{{{ Input::old('note', $accessory->note) }}}</textarea>
                    {{ $errors->first('note', '<span class="alert-msg"><i class="fa fa-times"></i> :message</span>') }}
                </div>
            </div>
            <!-- Form actions -->
                <div class="form-group">
                <label class="col-md-2 control-label"></label>
                    <div class="col-md-7">
                        <a class="btn btn-link" href="{{ URL::previous() }}">@lang('button.cancel')</a>
                        <button type="submit" class="btn btn-success"><i class="fa fa-check icon-white"></i>@lang('general.checkin')</button>
                    </div>
                </div>

</form>
</div>
</div>

@stop
