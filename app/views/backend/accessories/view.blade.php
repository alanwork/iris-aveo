@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')

 {{{ $accessory->name }}}
 @lang('general.accessory') ::
@parent
@stop

{{-- Page content --}}
@section('content')


<div class="row header">
    <div class="col-md-12">
        <div class="btn-group pull-right">
           <a href="{{ URL::previous() }}" class="btn-flat gray pull-right"><i class="fa fa-arrow-left icon-white"></i>  @lang('general.back')</a>        </div>
        <h3>
            {{{ $accessory->name }}}
 @lang('general.accessory')

        </h3>
    </div>
</div>

<div class="user-profile">
<div class="row profile">
<div class="col-md-9 bio">
                            <!-- checked out accessories table -->
                            @if ($accessory->users->count() > 0)
                           <table id="example">
                            <thead>
                                <tr role="row">
                                        <th class="col-md-11">@lang('general.user')</th>
                                        <th class="col-md-1">@lang('table.actions')</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($accessory->users as $accessory_users)
                                    <tr>
                                        <td>

                                        <a href="{{ route('view/user', $accessory_users->id) }}">
                                        {{{ $accessory_users->fullName() }}}
                                        
                                        </a>
                                       
                                        </td>

                                        <td>
                                            <a href="{{ route('checkin/accessory', $accessory_users->pivot->id) }}" class="btn-flat info">Checkin</a>
                                        </td>

                                    </tr>
                                    @endforeach


                                </tbody>
                            </table>

                            @else
                            <div class="col-md-9">
                                <div class="alert alert-info alert-block">
                                    <i class="fa fa-info-circle"></i>
                                    @lang('general.no_results')
                                </div>
                            </div>
                            @endif

                        </div>


<div class="row">&nbsp;</div>
<div class="row">&nbsp;</div>

<div class="user-profile">
<div class="row profile">
<div class="col-md-9 bio">
<p></p>

                            <!-- checked out accessories table -->
                            @if ($accessory->sites->count() > 0)
                           <table id="example">
                            <thead>
                                <tr role="row">
                                        <th class="col-md-11">@lang('general.location')</th>
                                        <th class="col-md-1" style="text-align: left;">@lang('general.quantity')</th>
                                        <th class="col-md-1">@lang('table.actions')</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($accessory->locations as $accessory_locations)
                                    <tr>
                                        <td>
                                            <?php //var_dump($accessory_locations); ?>
                                        <a href="{{ route('sites', $accessory_locations->id) }}">
                                        {{{ $accessory_locations->name }}}
                                        
                                        </a>
                                       
                                        </td>
                                        <td> {{ $accessory_locations->pivot->quantity }}</td>                                        
                                        <td>
                                            <a href="{{ route('checkinLocation/accessory', $accessory_locations->pivot->id) }}" class="btn-flat info">Checkin</a>
                                        </td>

                                    </tr>
                                    @endforeach


                                </tbody>
                            </table>

                            @else
                            <div class="col-md-9">
                                <div class="alert alert-info alert-block">
                                    <i class="fa fa-info-circle"></i>
                                    @lang('general.no_results')
                                </div>
                            </div>
                            @endif

<div class="row">&nbsp;</div>
<div class="row">&nbsp;</div>
<div class="row">History</div>
<div class="row">&nbsp;</div>
<div class="row">&nbsp;</div>

<table class="table table-hover table-fixed break-word">
            <thead>
                <tr>
                    <th class="col-md-3">@lang('general.date')</th>
                    <th class="col-md-2"><span class="line"></span>@lang('general.admin')</th>
                    <th class="col-md-2"><span class="line"></span>@lang('table.actions')</th>
                    <th class="col-md-2"><span class="line"></span>@lang('general.user')</th>
                    <th class="col-md-3"><span class="line"></span>@lang('general.notes')</th>
                    <th class="col-md-3"><span class="line"></span>@lang('general.quantity')</th>
                    <th class="col-md-3"><span class="line"></span>@lang('general.do_number')</th>
                </tr>
            </thead>
            <tbody>
            @if (count($accessory->assetlog) > 0)
                @foreach ($accessory->assetlog as $log)
                 
                <tr>
                    <td>{{ $log->id }}  {{{ $log->created_at }}}</td>
                    <td>
                        @if (isset($log->user_id))
                        {{{ $log->adminlog->fullName() }}}
                        @endif
                    </td>
                    <td>{{ $log->action_type }}</td>
                    <td>
                        @if ((isset($log->checkedout_to)) && ($log->checkedout_to!=0) && ($log->checkedout_to!=''))

                            @if ($log->userlog->deleted_at=='')
                                <a href="{{ route('view/user', $log->checkedout_to) }}">
                                {{{ $log->userlog->fullName() }}}
                                 </a>
                            @else
                                <del>{{{ $log->userlog->fullName() }}}</del>
                            @endif

                        @elseif ($log->locationlog)
                                <a href="{{ route('locations', $log->location_id) }}">
                                {{{ $log->locationlog->name }}}
                                 </a>                            
                        @endif
                    </td>
                    <td>
                        @if ($log->note) {{{ $log->note }}}
                        @endif
                    </td>                    
                    <td>
                        @if ($log->accessory_quantity) {{{ $log->accessory_quantity }}}
                        @elseif ($log->checkedout_to) 1
                        @endif
                    </td>
                    <td><a href="{{ route('getDODetail', $log->delivery_order) }}"> {{ $log->delivery_order}} </a></td>
                </tr>
       
                @endforeach
                @endif

            </tbody>
        </table>

                        </div>

                        
<!-- side address column -->
<div class="col-md-3 col-xs-12 address pull-right">
    <br /><br />
    <h6>@lang('admin/accessories/general.about_accessories_title')</h6>
    <p>@lang('admin/accessories/general.about_accessories_text') </p>

</div>

@stop
