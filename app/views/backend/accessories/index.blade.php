@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
@lang('general.accessories') ::
@parent
@stop

{{-- Page content --}}
@section('content')

<div class="row header">
    <div class="col-md-12">
        <a href="{{ route('create/accessory') }}" class="btn btn-success pull-right"><i class="fa fa-plus icon-white"></i> @lang('general.create')</a>
        <h3>@lang('general.accessories')</h3>
        @if(Input::get('Deleted'))
            <a href="{{ URL::to('admin/accessories') }}" class="btn btn-default pull-right"><i class="fa fa-trash"></i>  @lang('admin/accessories/general.view_accessories')</a>
        @else
            <a href="{{ URL::to('admin/accessories?Deleted=true') }}" class="btn btn-default pull-right"><i class="fa fa-trash"></i>  @lang('admin/models/general.view_deleted')</a>
        @endif

    </div>
</div>

<div class="user-profile">
<div class="row form-wrapper">
<div class="col-md-9 bio">
       {{ Form::open([
      'method' => 'GET',
      'route' => ['accessories'],
      'class' => 'form-horizontal' ]) }}
        <div class="table-responsive">
		<table id="example">
        <div style="float: right">
            @if(Input::get('Deleted'))
                {{ Form::hidden('Deleted','true')}}
            @endif            
            {{ Form::text('querySearch', Input::old('querySearch'), array('placeholder' => 'Search by Accessories Name..')) }}
            {{ Form::button('<i class="fa fa-search"></i>', array('type' => 'submit', 'name' => 'submit', 'title' => 'Search By Accessories Name')) }}

        </div>
        <thead>
            <tr role="row">
                <th class="col-md-5" bSortable="true">@lang('admin/accessories/table.title')</th>
                <th class="col-md-2" bSortable="true">@lang('admin/accessories/general.total')</th>
                <th class="col-md-2" bSortable="true">@lang('admin/accessories/general.remaining')</th>
                <th class="col-md-2" bSortable="true">@lang('admin/accessories/general.checkout_user')</th>
                <th class="col-md-2" bSortable="true">@lang('admin/accessories/general.checkout_site')</th>
                <th class="col-md-3 actions" bSortable="true">@lang('table.actions')</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($accessories as $accessory)
            <tr>
                
                @if(Input::get('Deleted'))
                <td>{{{ $accessory->name }}}</td>
                @else
                <td><a href="{{ route('view/accessory', $accessory->id) }}">{{{ $accessory->name }}}</a></td>
                @endif
                <td>{{{ $accessory->qty }}} </td>
                {{--<td>{{{ $accessory->numRemaining() }}} </td>--}}
                <td>{{{ $accessory->remainQuantity }}} </td>
                <td>
                @if($accessory->deleted_at=="")
                @if(Sentry::getUser()->hasAccess('admin'))                    
                <a href="{{ route('checkout/accessory', $accessory->id) }}" class="btn btn-info btn-sm"{{ (($accessory->numRemaining() > 0 ) ? '' : ' disabled') }}>@lang('general.checkout')</a>
                @endif
                @endif                    
                </td>
                <td>
                @if($accessory->deleted_at=="")
                @if(Sentry::getUser()->hasAccess('admin'))                    
                <a href="{{ route('checkoutlocation/accessory', $accessory->id) }}" class="btn btn-info btn-sm"{{ (($accessory->numRemaining() > 0 ) ? '' : ' disabled') }}>@lang('general.checkout')</a>
                @endif
                @endif
                </td>                
                <td>
                @if($accessory->deleted_at=="")
                <a href="{{ route('update/accessory', $accessory->id) }}" class="btn btn-warning btn-sm" title="Edit Accessory"><i class="fa fa-pencil icon-white"></i></a>
<a data-html="false" class="btn btn-danger btn-sm" data-toggle="" href="{{ route('delete/accessory', $accessory->id) }}" data-content="@lang('admin/accessories/message.delete.confirm')"
data-title="@lang('general.delete') {{{ htmlspecialchars($accessory->name) }}}?" title="Delete Accessory"><i class="fa fa-trash icon-white"></i></a>
                @else
                    <a href="{{ route('restore/accessory', $accessory->id) }}" class="btn btn-warning btn-sm" title="Restore Accessory"><i class="fa fa-recycle icon-white"></i></a>
                @endif                
                </td>
            </tr>
            @endforeach
        </tbody>
        </table>
<div class="table-responsive">{{ $accessories->appends(Request::except('page'))->links() }}</div>
 {{ Form::close() }}
    </div>
    </div>


<!-- side address column -->
<div class="col-md-3 col-xs-12 address pull-right">
    <br /><br />
    <h6>@lang('admin/accessories/general.about_accessories_title')</h6>
    <p>@lang('admin/accessories/general.about_accessories_text') </p>

</div>
</div>
</div>
@stop
