@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
@lang('admin/sites/table.title') ::
@parent
@stop

{{-- Page content --}}
@section('content')

<div class="row header">
    <div class="col-md-12">
         
        <a href="{{ route('create/site') }}" class="btn btn-success pull-right"><i class="fa fa-plus icon-white"></i>  @lang('general.create')</a>
        <a href="{{ route('import/site') }}" class="btn btn-default pull-right"><span class="fa fa-upload"></span> @lang('general.upload')</a>
        <h3>@lang('admin/sites/table.title')</h3>
        @if(Input::get('Deleted'))
            <a href="{{ URL::to('admin/settings/sites') }}" class="btn btn-default pull-right"><i class="fa fa-trash"></i>  @lang('admin/sites/general.view_sites')</a>
        @else
            <a href="{{ URL::to('admin/settings/sites?Deleted=true') }}" class="btn btn-default pull-right"><i class="fa fa-trash"></i>  @lang('admin/models/general.view_deleted')</a>
        @endif
    </div>
</div>

<div class="row form-wrapper">
<table id="example">
    <thead>
        <tr role="row">
            <th class="col-md-3">@lang('admin/sites/table.title')</th>
            <th class="col-md-2">@lang('admin/sites/table.code')</th>
            <th class="col-md-2">@lang('admin/sites/table.site_type')</th>
            <th class="col-md-2">@lang('admin/sites/table.location_id')</th>
            <th class="col-md-2">@lang('admin/sites/table.asset_accessory')</th>
            <!-- <th class="col-md-1">@lang('admin/sites/table.numassets')</th>
            <th class="col-md-2">@lang('general.depreciation')</th>
            <th class="col-md-2">@lang('general.category')</th>
            <th class="col-md-2">@lang('general.eol')</th> -->
            <th class="col-md-2 actions">@lang('table.actions')</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($sites as $site)
        <tr>
            
            <td>{{{ $site->name }}}</td>
            <td>{{{ $site->code }}}</td>
            <td>{{{ $site->site_type }}}</td>
            <td>
                <?php //var_dump($site->locations); ?>
                @if ($site->locations) 
                {{{ $site->locations->name }}}
                @endif
            </td>
            <td><a href="{{ route('view/site', $site->id) }}" class="btn btn-info btn-sm">@lang('admin/sites/table.details')</a></td>
            <td>
            @if($site->deleted_at=="")
                <a href="{{ route('update/site', $site->id) }}" class="btn btn-warning btn-sm" title="Edit Site"><i class="fa fa-pencil icon-white"></i></a>
                <a data-html="false" class="btn delete-asset btn-danger btn-sm" data-toggle="modal" href="{{ route('delete/site', $site->id) }}" data-content="@lang('admin/sites/message.delete.confirm')"
                data-title="@lang('general.delete')
                {{ htmlspecialchars($site->name) }}?" onClick="return false;" title="Remove Site"><i class="fa fa-trash icon-white"></i></a>
            @else
                <a href="{{ route('restore/site', $site->id) }}" class="btn btn-warning btn-sm" title="Restore Site"><i class="fa fa-recycle icon-white"></i></a>
            @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="table-responsive">{{ $sites->appends(Request::except('page'))->links() }}</div>
@stop
