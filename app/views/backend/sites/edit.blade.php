@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
    @if ($site->id)
        @lang('admin/sites/general.update') ::
    @else
        @lang('admin/sites/general.create') ::
    @endif
@parent
@stop

{{-- Page content --}}
@section('content')


<div class="row header">
    <div class="col-md-12">
        <a href="{{ URL::previous() }}" class="btn-flat gray pull-right"><i class="fa fa-arrow-left icon-white"></i> @lang('general.back')</a>
        <h3>
        @if ($site->id)
            @lang('admin/sites/general.update')
        @else
            @lang('admin/sites/general.create')
        @endif
</h3>
    </div>
</div>

<div class="user-profile">
<div class="row profile">
<div class="col-md-9 bio">

                        <form class="form-horizontal" method="post" action="" autocomplete="off">
                        <!-- CSRF Token -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />



                        <!-- Name -->
                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                            <div class="col-md-3">
                                {{ Form::label('name', Lang::get('admin/sites/general.site_name')) }}
                                <i class='fa fa-asterisk'></i>
                            </div>                        
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="name" id="name" value="{{{ Input::old('name', $site->name) }}}" />
                                {{ $errors->first('name', '<br><span class="alert-msg"><i class="fa fa-times"></i> :message</span>') }}
                            </div>
                        </div>

                        <!-- Code -->
                        <div class="form-group {{ $errors->has('code') ? ' has-error' : '' }}">
                            <div class="col-md-3">
                                {{ Form::label('code', Lang::get('admin/sites/general.site_code')) }}
                                <i class='fa fa-asterisk'></i>
                            </div>                        
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="code" id="code" value="{{{ Input::old('code', $site->code) }}}" />
                                {{ $errors->first('code', '<br><span class="alert-msg"><i class="fa fa-times"></i> :message</span>') }}
                            </div>
                        </div> 

                        <!-- Location -->
                        <div class="form-group {{ $errors->has('loc_id') ? ' has-error' : '' }}">
                            <label for="parent" class="col-md-3">@lang('admin/sites/general.select_location')
                             <i class='fa fa-asterisk'></i></label>
                             </label>
                            <div class="col-md-9">
                                @if (isset($selected_loc))
                                    {{ Form::select('loc_id', $loc_list , $selected_loc->id, array('class'=>'select2', 'style'=>'min-width:400px')) }}

                                @else
                                    {{ Form::select('loc_id', $loc_list , Input::old('loc_id', $site->location_id), array('class'=>'select2', 'style'=>'min-width:400px')) }}
                                @endif
                                {{ $errors->first('loc_id', '<br><span class="alert-msg"><i class="fa fa-times"></i> :message</span>') }}
                            </div>
                        </div>

                        <!-- Site Type -->
                        <div class="form-group {{ $errors->has('site_type') ? ' has-error' : '' }}">
                            <label for="parent" class="col-md-3">@lang('admin/sites/general.site_type')
                             <i class='fa fa-asterisk'></i></label>
                             </label>
                            <div class="col-md-9">
                                @if (isset($site_type))
                                    {{ Form::select('site_type', $type_list , $selected_type->code, array('class'=>'select2', 'style'=>'min-width:400px')) }}

                                @else
                                    {{ Form::select('site_type', $type_list , Input::old('site_type', $site->site_type), array('class'=>'select2', 'style'=>'min-width:400px')) }}
                                @endif
                                {{ $errors->first('site_type', '<br><span class="alert-msg"><i class="fa fa-times"></i> :message</span>') }}
                            </div>
                        </div>
                        <hr>
                        <!-- Form actions -->
                        <div class="form-group">
                       
                            <div class="col-md-7 col-md-offset-3">
                                <a class="btn btn-link" href="{{ URL::previous() }}">@lang('button.cancel')</a>
                                <button type="submit" class="btn btn-success"><i class="fa fa-check icon-white"></i> @lang('general.save')</button>
                            </div>
                        </div>
                    </form>
                    <br><br><br><br><br>
                    </div>

                    <!-- side address column -->
                    <div class="col-md-3 col-xs-12 address pull-right">
                        <br /><br />
                        <h6>@lang('admin/sites/general.about_site')</h6>
                        <p>@lang('admin/sites/general.about_sites') </p>

                    </div>
</div>
</div>

@stop
