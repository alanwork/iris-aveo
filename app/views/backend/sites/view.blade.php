@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
@lang('admin/sites/table.view')
{{{ $site->name }}} ::
@parent
@stop

{{-- Page content --}}
@section('content')


<div class="row header">
    <div class="col-md-12">
        <div class="btn-group pull-right">
           <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">@lang('button.actions')
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                @if ($site->deleted_at=='')
                    <li><a href="{{ route('update/site', $site->id) }}">@lang('admin/sites/table.edit')</a></li>
                    <li><a href="{{ route('clone/site', $site->id) }}">@lang('admin/sites/table.clone')</a></li>
                    <li><a href="{{ route('create/hardware', $site->id) }}">@lang('admin/hardware/form.create')</a></li>
                @else
                    <li><a href="{{ route('restore/site', $site->id) }}">@lang('admin/sites/general.restore')</a></li>
                @endif
            </ul>
        </div>
        <h3>

            @lang('admin/sites/table.view') -
            {{{ $site->name }}}

        </h3>
    </div>
</div>

<div class="user-profile">
<div class="row profile">
<div class="col-md-9 bio">
    
    @if ($site->deleted_at!='')
			<div class="alert alert-warning alert-block">
				<i class="fa fa-warning"></i>
				@lang('admin/sites/general.deleted', array('site_id' => $site->id))

			</div>

		@endif


                            <!-- checked out sites table -->
                            @if (count($site->assets) > 0)
                           <table id="example">
                            <thead>
                                <tr role="row">
                                        <th class="col-md-3">@lang('general.name')</th>
                                        <th class="col-md-3">@lang('general.asset_tag')</th>
                                        <th class="col-md-3">@lang('admin/hardware/table.serial')</th>
                                        <th class="col-md-2">@lang('table.actions')</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($site->assets as $siteassets)
                                    <tr>
                                        <td><a href="{{ route('view/hardware', $siteassets->id) }}">{{{ $siteassets->name }}}</a></td>
                                        <td><a href="{{ route('view/hardware', $siteassets->id) }}">{{{ $siteassets->asset_tag }}}</a></td>
                                        <td>
                                        @if ($siteassets->serial)
                                        
                                        {{{ $siteassets->serial }}}
                                        
                                        @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('checkinSite/hardware', $siteassets->id) }}" class="btn-flat info">Checkin</a>
                                        </td>

                                    </tr>
                                    @endforeach


                                </tbody>
                            </table>

                            @else
                            <div class="col-md-9">
                                <div class="alert alert-info alert-block">
                                    <i class="fa fa-info-circle"></i>
                                    @lang('general.no_results')
                                </div>
                            </div>
                            @endif

                        </div>

<!-- accessories -->

<div class="col-md-9 bio">
    
                            <!-- checked out accessories table -->
                            @if (count($site->accessoriesLocation) > 0)
                           <table id="example">
                            <thead>
                                <tr role="row">
                                        <th class="col-md-3">@lang('general.name')</th>
                                        <th class="col-md-3">@lang('general.quantity')</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($site->accessoriesLocation as $siteaccessory)
                                    <tr>
                                        <td><a href="{{ route('view/accessory', $siteaccessory->accessory_id) }}">{{{ $siteaccessory->accname }}}</a></td>
                                        <td>{{{ $siteaccessory->quantity }}}</a></td>
                                    </tr>
                                    @endforeach


                                </tbody>
                            </table>

                            @else
                            <div class="col-md-9">
                                <div class="alert alert-info alert-block">
                                    <i class="fa fa-info-circle"></i>
                                    @lang('general.no_results')
                                </div>
                            </div>
                            @endif

                        </div>
                    <!-- side address column -->
                    <div class="col-md-3 col-xs-12 address pull-right">
                    <h6>More Info:</h6>
                               <ul>


                                @if ($site->name)
                                <li>@lang('general.name'):
                                {{ $site->name }}</li>
                                @endif

                                @if ($site->code)
                                <li>@lang('general.code'):
                                {{ $site->code }}</li>
                                @endif

                                @if ($site->site_type)
                                <li>@lang('admin/sites/table.site_type'): {{$site->site_type}} </li>
                                @endif
                                   
                                @if  ($site->deleted_at!='')
                                   <li><br /><a href="{{ route('restore/site', $site->id) }}" class="btn-flat large info ">@lang('admin/sites/general.restore')</a></li>

                    	@endif

                            </ul>

                    </div>
@stop
