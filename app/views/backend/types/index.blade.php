@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
@lang('admin/types/table.title') ::
@parent
@stop

{{-- Page content --}}
@section('content')

<div class="row header">
    <div class="col-md-12">
        <a href="{{ route('create/type') }}" class="btn btn-success pull-right"><i class="fa fa-plus icon-white"></i>  @lang('general.create')</a>
        <h3>@lang('admin/types/table.title')</h3>
    </div>
</div>

<div class="row form-wrapper">
<table id="example">
    <thead>
        <tr role="row">
            <th class="col-md-3">@lang('admin/types/table.title')</th>
            <th class="col-md-2">@lang('admin/types/table.code')</th>
            <!-- <th class="col-md-1">@lang('admin/types/table.numassets')</th>
            <th class="col-md-2">@lang('general.depreciation')</th>
            <th class="col-md-2">@lang('general.category')</th>
            <th class="col-md-2">@lang('general.eol')</th> -->
            <th class="col-md-2 actions">@lang('table.actions')</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($types as $type)
        <tr>
            <!-- <a href="{{ route('view/type', $type->id) }}"> -->
            <td>{{{ $type->name }}}</a></td>
            <td>{{{ $type->code }}}</td>
            

            <td>
            @if($type->deleted_at=="")
                <a href="{{ route('update/types', $type->id) }}" class="btn btn-warning btn-sm" title="Edit Asset Type"><i class="fa fa-pencil icon-white"></i></a>
                <a data-html="false" class="btn delete-asset btn-danger btn-sm" data-toggle="modal" href="{{ route('delete/type', $type->id) }}" data-content="@lang('admin/types/message.delete.confirm')"
                data-title="@lang('general.delete')
                {{ htmlspecialchars($type->name) }}?" onClick="return false;"><i class="fa fa-trash icon-white"></i></a>
            @else
                <a href="" class="btn btn-warning btn-sm"><i class="fa fa-recycle icon-white"></i></a>
            @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="table-responsive">{{ $types->appends(Request::except('page'))->links() }}</div>
@stop
