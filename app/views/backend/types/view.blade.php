@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
@lang('admin/types/table.view')
{{{ $model->model_tag }}} ::
@parent
@stop

{{-- Page content --}}
@section('content')


<div class="row header">
    <div class="col-md-12">
        <div class="btn-group pull-right">
           <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">@lang('button.actions')
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                @if ($type->deleted_at=='')
                    <li><a href="{{ route('update/type', $type->id) }}">@lang('admin/types/table.edit')</a></li>
                    <li><a href="{{ route('clone/type', $type->id) }}">@lang('admin/types/table.clone')</a></li>
                    <li><a href="{{ route('create/type', $type->id) }}">@lang('admin/hardware/form.create')</a></li>
                @else
                    <li><a href="{{ route('restore/type', $type->id) }}">@lang('admin/types/general.restore')</a></li>
                @endif
            </ul>
        </div>
        <h3>

            @lang('admin/types/table.view') -
            {{{ $type->name }}}

        </h3>
    </div>
</div>


@stop
