<?php
    $presenter = new Illuminate\Pagination\BootstrapPresenter($paginator);
?>

<div class="pagination" style="width: 100%">
    <!-- <ul class="pull left"> -->
        <!-- <li> -->
        Showing
        <?php echo $paginator->getFrom(); ?>
        -
        <?php echo $paginator->getTo(); ?>
        of
        <?php echo $paginator->getTotal(); ?>
        items
        <!-- </li> -->
    <!-- </ul> -->

    <ul class="pull-right">
        <div class="pagination"><?php echo $presenter->render(); ?></div>
    </ul>
</div>
