<?php
namespace Aveo;

function Login()
{
	// do sso login again.
    $row = \Iris\Unity::refresh();

    // unfortunately sentry authentication is strongly coupled.
    \Sentry::login(\User::where('email', '=', $row['userEmail'])->first());
}

function Logout(){
	\Sentry::logout();
}

?>