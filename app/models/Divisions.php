<?php

class Divisions extends Elegant
{
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];
    protected $table = 'divisions';

    /**
    * Category validation rules
    */
    public $rules = array(
        'name'   => 'required|alpha_space|min:3|max:255|unique:divisions,name,{id}',
    );
 
     public function has_departments()
    {
        return $this->hasMany('Departments', 'div_id')->count();
    }

    public function departments()
    {
        return $this->hasMany('Departments', 'div_id');
    }

    public function scopeDeleted($query)
    {
        return $query->whereNotNull('deleted_at');
    }
}
