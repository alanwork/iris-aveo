<?php

class State extends Elegant
{
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];
    protected $table = 'state';

    /**
    * Category validation rules
    */
    public $rules = array(
        'StateName'   => 'required|alpha_space|min:3|max:255|unique:departments,name,{id}',
    );

    public function scopeDeleted($query)
    {
        return $query->whereNotNull('deleted_at');
    }  
}
