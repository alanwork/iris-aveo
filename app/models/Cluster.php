<?php

class Cluster extends Elegant
{
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];
    protected $table = 'cluster';

    /**
    * Category validation rules
    */
    public $rules = array(
        'ClusterName'   => 'required|alpha_space|min:3|max:255|unique:cluster,name,{id}',
    );

    public function scopeDeleted($query)
    {
        return $query->whereNotNull('deleted_at');
    }
   
}
