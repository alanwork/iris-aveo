<?php

class Departments extends Elegant
{
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];
    protected $table = 'departments';

    /**
    * Category validation rules
    */
    public $rules = array(
        'name'   => 'required|alpha_space|min:3|max:255|unique:departments,name,{id}',
    );

    public function scopeDeleted($query)
    {
        return $query->whereNotNull('deleted_at');
    }

    public function division()
    {
        return $this->belongsTo('Divisions','div_id');
    }

    public function User()
    {
        return $this->hasMany('User', 'dept_id');
    }    
}
