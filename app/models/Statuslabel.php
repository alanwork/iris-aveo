<?php

class Statuslabel extends Elegant
{
	use SoftDeletingTrait;
    protected $dates = ['deleted_at'];
    protected $table = 'status_labels';


    protected $rules = array(
        'name'  => 'required|alpha_space|min:2|max:100|unique:status_labels,name,{id}',
        'statuslabel_types' => 'required|in:deployable,pending,archived,undeployable,dismissed',
    );

    public function has_assets()
    {
        return $this->hasMany('Asset', 'status_id')->count();
    }

    public function getStatuslabelType() {

    	if ($this->pending == 1) {
			return 'pending';
		} elseif ($this->archived == 1) {
			return 'archived';
		} elseif (($this->archived == 0) && ($this->deployable == 0) && ($this->pending == 0) && ($this->dismissed == 0)) {
			return 'undeployable';
		} 
		  elseif (($this->archived == 0) && ($this->deployable == 0) && ($this->pending == 0) && ($this->dismissed == 1)) {
		  	return 'dismissed';
		}
		else {
			return 'deployable';
		}
    }

    public static function getStatuslabelTypesForDB($type) {
		if ($type == 'pending') {
			$statustype['pending'] = 1;
			$statustype['deployable'] = 0;
			$statustype['archived'] = 0;
			$statustype['dismissed'] = 0;

		} elseif ($type == 'deployable') {
			$statustype['pending'] = 0;
			$statustype['deployable'] = 1;
			$statustype['archived'] = 0;
			$statustype['dismissed'] = 0;

		} elseif ($type == 'archived') {
			$statustype['pending'] = 0;
			$statustype['deployable'] = 0;
			$statustype['archived'] = 1;
			$statustype['dismissed'] = 0;

		} elseif ($type == 'undeployable') {
			$statustype['pending'] = 0;
			$statustype['deployable'] = 0;
			$statustype['archived'] = 0;
			$statustype['dismissed'] = 0;

		} elseif ($type == 'dismissed') {
			$statustype['pending'] = 0;
			$statustype['deployable'] = 0;
			$statustype['archived'] = 0;
			$statustype['dismissed'] = 1;
		}

		return $statustype;
    }
}
