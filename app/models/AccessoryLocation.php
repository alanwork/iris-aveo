<?php

class AccessoryLocation extends Elegant
{
    use SoftDeletingTrait;
    //protected $dates = ['deleted_at'];
    protected $table = 'accessories_locations';

    /**
    * Category validation rules
    */
    public $rules = array(
        //'ClusterName'   => 'required|alpha_space|min:3|max:255|unique:cluster,name,{id}',
    );

    public function site()
    {
        return $this->belongsTo('Site','assigned_to')->withTrashed();
    }

    public function accessory()
    {
        return $this->belongsTo('Accessory','accesory_id')->withTrashed();
    }   
}
