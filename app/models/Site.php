<?php

class Site extends Elegant
{
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];
    protected $table = 'sites';

    /**
    * Category validation rules
    */
    public $rules = array(
        'name'   => 'required|alpha_space|min:3|max:255|unique:sites,name,{id}',
        'code'   => 'required',
        'site_type'     => 'required',
        'loc_id'   => 'required',
    );


   
    public function locations()
    {
        return $this->belongsTo('Location','location_id')->withTrashed();
    }       

    public function accessoriesLocation()
    {
        return $this->hasMany('AccessoryLocation','assigned_to')->withTrashed();
    }    

    // public function accessoriesLocation()
    // {
    //     return $this->belongsToMany('AccessoryLocation')->withTrashed();
    // }   

    public function scopeDeleted($query)
    {
        return $query->whereNotNull('deleted_at');
    }

    public function assets(){
        return $this->hasMany('Asset', 'site_id')->withTrashed();
    }

    // public function accessoriesLocation(){
    //     return $this->hasManyThrough('Accessory', 'AccessoryLocation', 'accessory_id', 'assigned_to');
    // }    
}
