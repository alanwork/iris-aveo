<?php
namespace Aveo;

use Cartalyst\Sentry\Cookies\CookieInterface;
use Cartalyst\Sentry\Cookies\NativeCookie;
use Cartalyst\Sentry\Groups\Eloquent\Provider as GroupProvider;
use Cartalyst\Sentry\Groups\ProviderInterface as GroupProviderInterface;
use Cartalyst\Sentry\Hashing\NativeHasher;
use Cartalyst\Sentry\Sessions\NativeSession;
use Cartalyst\Sentry\Sessions\SessionInterface;
use Cartalyst\Sentry\Throttling\Eloquent\Provider as ThrottleProvider;
use Cartalyst\Sentry\Throttling\ProviderInterface as ThrottleProviderInterface;
use Cartalyst\Sentry\Users\LoginRequiredException;
use Cartalyst\Sentry\Users\PasswordRequiredException;
use Cartalyst\Sentry\Users\Eloquent\Provider as UserProvider;
use Cartalyst\Sentry\Users\ProviderInterface as UserProviderInterface;
use Cartalyst\Sentry\Users\UserInterface;
use Cartalyst\Sentry\Users\UserNotFoundException;
use Cartalyst\Sentry\Users\UserNotActivatedException;

class Sentry extends \Cartalyst\Sentry\Sentry
{
	public function __construct(
		UserProviderInterface $userProvider = null,
		GroupProviderInterface $groupProvider = null,
		ThrottleProviderInterface $throttleProvider = null,
		SessionInterface $session = null,
		CookieInterface $cookie = null,
		$ipAddress = null
	)
	{
		$this->userProvider     = $userProvider ?: new \Aveo\UserProvider(new NativeHasher);
		$this->groupProvider    = $groupProvider ?: new GroupProvider;
		$this->throttleProvider = $throttleProvider ?: new ThrottleProvider($this->userProvider);

		$this->session          = $session ?: new NativeSession;
		$this->cookie           = $cookie ?: new NativeCookie;

		if (isset($ipAddress))
		{
			$this->ipAddress = $ipAddress;
		}
	}

	public function authenticate(array $credentials, $remember = false)
	{
		// We'll default to the login name field, but fallback to a hard-coded
		// 'login' key in the array that was passed.
		$loginName = $this->userProvider->getEmptyUser()->getLoginName();
		$loginCredentialKey = (isset($credentials[$loginName])) ? $loginName : 'login';

		if (empty($credentials[$loginCredentialKey]))
		{
			//throw new LoginRequiredException("The [$loginCredentialKey] attribute is required.");
		}

		//if (empty($credentials['password']))
		//{
		//	throw new PasswordRequiredException('The password attribute is required.');
		//}

		// If the user did the fallback 'login' key for the login code which
		// did not match the actual login name, we'll adjust the array so the
		// actual login name is provided.
		if ($loginCredentialKey !== $loginName)
		{
			$credentials[$loginName] = $credentials[$loginCredentialKey];
			unset($credentials[$loginCredentialKey]);
		}

		// If throttling is enabled, we'll firstly check the throttle.
		// This will tell us if the user is banned before we even attempt
		// to authenticate them
		if ($throttlingEnabled = $this->throttleProvider->isEnabled())
		{
			if ($throttle = $this->throttleProvider->findByUserLogin($credentials[$loginName], $this->ipAddress))
			{
				$throttle->check();
			}
		}

		try
		{
			// $user = $this->userProvider->findByCredentials($credentials);
			$user = $this->userProvider->findByEmail($credentials['email']);
		}
		catch (UserNotFoundException $e)
		{
			if ($throttlingEnabled and isset($throttle))
			{
				$throttle->addLoginAttempt();
			}

			throw $e;
		}

		if ($throttlingEnabled and isset($throttle))
		{
			$throttle->clearLoginAttempts();
		}

		$user->clearResetPassword();

		$this->login($user, $remember);

		return $this->user;
	}
}


