<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsAssetLogs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('asset_logs', function(Blueprint $table)
		{
			//
			$table->string('delivery_order', 50)->nullable()->default(NULL);
			$table->integer('accessory_quantity')->nullable()->default(NULL);
			$table->integer('user_deleted_id')->nullable()->default(NULL);
			$table->integer('site_id')->nullable()->default(NULL);

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('asset_logs', function(Blueprint $table)
		{
			//
			$table->dropColumn('delivery_order');
			$table->dropColumn('accessory_quantity');
			$table->dropColumn('user_deleted_id');
			$table->dropColumn('site_id');
		});
	}

}
