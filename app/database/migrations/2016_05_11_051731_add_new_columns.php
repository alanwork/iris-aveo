<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('locations', function(Blueprint $table)
		{
			//
			$table->string('code',255);
			$table->string('is_ktw',2);
			$table->string('is_pi1m',2);
			$table->string('is_fiber',2);
			$table->integer('cluster_id');
			$table->integer('state_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('locations', function(Blueprint $table)
		{
			//
			$table->dropColumn('code');
			$table->dropColumn('is_ktw');
			$table->dropColumn('is_pi1m');
			$table->dropColumn('is_fiber');
			$table->dropColumn('cluster_id');
			$table->dropColumn('state_id');
		});
	}

}
