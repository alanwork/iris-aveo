<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
         Schema::create('departments', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code');
            $table->integer('div_id')->nullable()->default(NULL);
            $table->timestamps();
        });		

        // Schema::table('departments', function ($table) {
        //     $table->softDeletes();
        // });  		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('departments');
	}

}
