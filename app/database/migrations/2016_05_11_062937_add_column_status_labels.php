<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStatusLabels extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('status_labels', function(Blueprint $table)
		{
			//
			$table->integer('dismissed');
			$table->string('code', 255);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('status_labels', function(Blueprint $table)
		{
			//
			$table->dropColumn('dismissed');
			$table->dropColumn('code');

		});
	}

}
