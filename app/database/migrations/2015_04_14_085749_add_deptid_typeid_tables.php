<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeptidTypeidTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('users', function ($table) {
			$table->integer('dept_id')->nullable();
		});

		Schema::table('assets', function ($table) {
			$table->integer('dept_id')->nullable();
			$table->integer('type_id')->nullable();
		});		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('users', function ($table) {
			$table->dropColumn('dept_id');
		});

		Schema::table('assets', function ($table) {
			$table->dropColumn('dept_id');
			$table->dropColumn('type_id');
		});		
	}

}
