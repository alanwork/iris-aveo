# ************************************************************
# Sequel Pro SQL dump
# Version 4135
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.38)
# Database: snipeit
# Generation Time: 2015-06-29 07:28:10 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table accessories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `accessories`;

CREATE TABLE `accessories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  `requestable` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `accessories` WRITE;
/*!40000 ALTER TABLE `accessories` DISABLE KEYS */;

INSERT INTO `accessories` (`id`, `name`, `category_id`, `user_id`, `qty`, `requestable`, `created_at`, `updated_at`, `deleted_at`, `location_id`, `model_id`)
VALUES
	(1,'Logitech Keyboards',0,3,100,0,'2015-04-28 08:38:19','2015-06-01 02:42:31',NULL,NULL,7),
	(2,'testacc',0,3,10,0,'2015-06-01 02:48:52','2015-06-09 01:32:09','2015-06-09 01:32:09',NULL,1),
	(3,'Bluetooth keyboard',0,10,25,0,'2015-06-01 02:49:32','2015-06-01 02:49:32',NULL,NULL,7),
	(4,'Headset Bose',0,12,20,0,'2015-06-01 04:08:52','2015-06-01 04:22:23',NULL,NULL,5),
	(5,'test accessory',0,10,100,0,'2015-06-05 10:09:10','2015-06-05 10:11:15','2015-06-05 10:11:15',NULL,2);

/*!40000 ALTER TABLE `accessories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table accessories_locations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `accessories_locations`;

CREATE TABLE `accessories_locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `accessory_id` int(3) DEFAULT NULL,
  `assigned_to` int(3) DEFAULT NULL,
  `quantity` int(5) DEFAULT NULL,
  `user_id` int(5) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `accessories_locations` WRITE;
/*!40000 ALTER TABLE `accessories_locations` DISABLE KEYS */;

INSERT INTO `accessories_locations` (`id`, `accessory_id`, `assigned_to`, `quantity`, `user_id`, `created_at`, `updated_at`)
VALUES
	(1,1,5,21,10,'2015-06-05 04:37:03','2015-06-05 01:57:54'),
	(2,1,1,22,10,'2015-06-17 04:19:28','0000-00-00 00:00:00'),
	(3,4,5,3,NULL,'2015-06-05 05:06:19','0000-00-00 00:00:00'),
	(6,3,2,15,10,'2015-06-10 09:15:16','0000-00-00 00:00:00'),
	(7,1,25,5,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(8,1,73,3,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `accessories_locations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table accessories_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `accessories_users`;

CREATE TABLE `accessories_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `accessory_id` int(11) DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `accessories_users` WRITE;
/*!40000 ALTER TABLE `accessories_users` DISABLE KEYS */;

INSERT INTO `accessories_users` (`id`, `user_id`, `accessory_id`, `assigned_to`, `created_at`, `updated_at`)
VALUES
	(1,NULL,1,8,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(2,NULL,3,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(3,NULL,3,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(4,NULL,3,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(5,NULL,3,11,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(6,NULL,3,11,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(8,NULL,1,11,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(9,NULL,1,11,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(10,NULL,3,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(11,NULL,3,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(12,NULL,3,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(13,NULL,3,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(14,NULL,1,11,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(15,NULL,1,11,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(16,NULL,1,11,'0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `accessories_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table asset_logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `asset_logs`;

CREATE TABLE `asset_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `action_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `asset_id` int(11) DEFAULT NULL,
  `checkedout_to` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `asset_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `filename` text COLLATE utf8_unicode_ci,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `requested_at` datetime DEFAULT NULL,
  `accepted_at` datetime DEFAULT NULL,
  `accessory_id` int(11) DEFAULT NULL,
  `accepted_id` int(11) DEFAULT NULL,
  `delivery_order` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `accessory_quantity` int(11) DEFAULT NULL,
  `user_deleted_id` int(3) DEFAULT NULL,
  `site_id` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `asset_logs` WRITE;
/*!40000 ALTER TABLE `asset_logs` DISABLE KEYS */;

INSERT INTO `asset_logs` (`id`, `user_id`, `action_type`, `asset_id`, `checkedout_to`, `location_id`, `created_at`, `asset_type`, `note`, `filename`, `updated_at`, `deleted_at`, `requested_at`, `accepted_at`, `accessory_id`, `accepted_id`, `delivery_order`, `accessory_quantity`, `user_deleted_id`, `site_id`)
VALUES
	(1,3,'checkout',17,3,2,'2015-04-28 08:01:22','hardware','checkout to alan',NULL,'2015-04-28 08:01:22',NULL,NULL,NULL,NULL,NULL,'DO001',NULL,NULL,NULL),
	(2,3,'checkout',NULL,3,2,'2015-04-28 08:38:30','accessory','',NULL,'2015-04-28 08:38:30',NULL,NULL,NULL,1,NULL,'DO001',NULL,NULL,NULL),
	(3,3,'checkin from',NULL,3,NULL,'2015-04-28 08:39:04','accessory',NULL,NULL,'2015-04-28 08:39:04',NULL,NULL,NULL,1,NULL,'DO001',NULL,NULL,NULL),
	(4,3,'checkout',31,8,5,'2015-05-15 08:15:55','hardware','checked out to ridhwan',NULL,'2015-05-15 08:15:55',NULL,NULL,NULL,NULL,NULL,'DO001',NULL,NULL,NULL),
	(5,11,'checkout',NULL,8,5,'2015-05-26 18:15:42','accessory','',NULL,'2015-05-26 18:15:42',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),
	(6,12,'update',32,NULL,NULL,'2015-05-31 05:58:32','hardware',NULL,NULL,'2015-05-31 05:58:32',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(7,12,'update',33,NULL,NULL,'2015-06-01 03:33:35','hardware',NULL,NULL,'2015-06-01 03:33:35',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(8,12,'update',28,NULL,NULL,'2015-06-01 03:33:35','hardware',NULL,NULL,'2015-06-01 03:33:35',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(9,12,'update',29,NULL,NULL,'2015-06-01 03:33:35','hardware',NULL,NULL,'2015-06-01 03:33:35',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(10,12,'update',34,NULL,NULL,'2015-06-01 03:36:46','hardware',NULL,NULL,'2015-06-01 03:36:46',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(11,12,'update',33,NULL,NULL,'2015-06-01 03:36:46','hardware',NULL,NULL,'2015-06-01 03:36:46',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(12,12,'update',34,NULL,NULL,'2015-06-01 03:37:22','hardware',NULL,NULL,'2015-06-01 03:37:22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(13,12,'update',33,NULL,NULL,'2015-06-01 03:37:22','hardware',NULL,NULL,'2015-06-01 03:37:22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(14,11,'checkout',35,9,5,'2015-06-01 03:59:44','hardware','Mutual Understanding',NULL,'2015-06-01 03:59:44',NULL,NULL,NULL,NULL,NULL,'DO001',NULL,NULL,NULL),
	(15,11,'checkin from',35,9,NULL,'2015-06-01 04:31:12','hardware','Abused by Luqman.',NULL,'2015-06-01 04:31:12',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(16,10,'checkout',NULL,2,NULL,'2015-06-01 04:56:21','accessory','',NULL,'2015-06-01 04:56:21',NULL,NULL,NULL,3,NULL,NULL,NULL,NULL,NULL),
	(17,10,'checkout',NULL,2,NULL,'2015-06-01 04:58:00','accessory','',NULL,'2015-06-01 04:58:00',NULL,NULL,NULL,3,NULL,NULL,NULL,NULL,NULL),
	(18,10,'checkout',NULL,2,NULL,'2015-06-01 04:59:34','accessory','',NULL,'2015-06-01 04:59:34',NULL,NULL,NULL,3,NULL,NULL,NULL,NULL,NULL),
	(19,10,'checkout',NULL,11,5,'2015-06-01 05:01:55','accessory','',NULL,'2015-06-01 05:01:55',NULL,NULL,NULL,3,NULL,NULL,NULL,NULL,NULL),
	(20,10,'checkout',NULL,11,5,'2015-06-01 05:03:05','accessory','',NULL,'2015-06-01 05:03:05',NULL,NULL,NULL,3,NULL,NULL,NULL,NULL,NULL),
	(21,10,'checkout',NULL,11,5,'2015-06-01 05:03:17','accessory','',NULL,'2015-06-01 05:03:17',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),
	(22,10,'checkin from',NULL,11,NULL,'2015-06-01 05:03:34','accessory',NULL,NULL,'2015-06-01 05:03:34',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),
	(23,10,'checkout',NULL,11,5,'2015-06-01 05:03:58','accessory','',NULL,'2015-06-01 05:03:58',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),
	(24,10,'checkout',NULL,11,5,'2015-06-01 05:04:04','accessory','',NULL,'2015-06-01 05:04:04',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),
	(25,11,'checkout',33,3,2,'2015-06-03 09:16:52','hardware','test DO',NULL,'2015-06-03 09:16:52',NULL,NULL,NULL,NULL,NULL,'DO001',NULL,NULL,NULL),
	(26,11,'update',11,NULL,NULL,'2015-06-04 02:20:13','hardware',NULL,NULL,'2015-06-04 02:20:13',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(27,11,'update',12,NULL,NULL,'2015-06-04 02:20:13','hardware',NULL,NULL,'2015-06-04 02:20:13',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(28,11,'checkout',NULL,5,NULL,'2015-06-04 04:23:29','accessory','',NULL,'2015-06-04 04:23:29',NULL,NULL,NULL,3,NULL,NULL,NULL,NULL,NULL),
	(29,11,'checkout',NULL,5,NULL,'2015-06-04 04:23:46','accessory','',NULL,'2015-06-04 04:23:46',NULL,NULL,NULL,3,NULL,NULL,NULL,NULL,NULL),
	(30,11,'checkout',NULL,11,5,'2015-06-04 04:27:36','accessory','',NULL,'2015-06-04 04:27:36',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),
	(31,11,'checkout',NULL,11,5,'2015-06-04 04:29:08','accessory','',NULL,'2015-06-04 04:29:08',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),
	(32,11,'checkout',NULL,5,NULL,'2015-06-04 04:46:42','accessory','',NULL,'2015-06-04 04:46:42',NULL,NULL,NULL,1,NULL,NULL,10,NULL,NULL),
	(33,11,'checkout',NULL,5,NULL,'2015-06-04 04:47:04','accessory','',NULL,'2015-06-04 04:47:04',NULL,NULL,NULL,1,NULL,NULL,10,NULL,NULL),
	(34,11,'checkout',NULL,5,NULL,'2015-06-04 04:48:18','accessory','',NULL,'2015-06-04 04:48:18',NULL,NULL,NULL,1,NULL,NULL,3,NULL,NULL),
	(35,11,'checkout',NULL,5,NULL,'2015-06-04 04:56:29','accessory','',NULL,'2015-06-04 04:56:29',NULL,NULL,NULL,1,NULL,NULL,4,NULL,NULL),
	(36,11,'checkout',NULL,5,NULL,'2015-06-04 06:24:19','accessory','',NULL,'2015-06-04 06:24:19',NULL,NULL,NULL,1,NULL,NULL,13,NULL,NULL),
	(37,11,'checkout',NULL,5,NULL,'2015-06-04 07:29:10','accessory','',NULL,'2015-06-04 07:29:10',NULL,NULL,NULL,1,NULL,NULL,16,NULL,NULL),
	(38,11,'checkout',NULL,5,NULL,'2015-06-04 08:02:07','accessory','',NULL,'2015-06-04 08:02:07',NULL,NULL,NULL,1,NULL,'DO9999',17,NULL,NULL),
	(39,11,'checkout',NULL,11,5,'2015-06-04 08:05:02','accessory','',NULL,'2015-06-04 08:05:02',NULL,NULL,NULL,1,NULL,'DO123',NULL,NULL,NULL),
	(40,10,'checkin from',NULL,NULL,NULL,'2015-06-05 01:41:30','accessory',NULL,NULL,'2015-06-05 01:41:30',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),
	(42,10,'checkin from',NULL,NULL,5,'2015-06-05 01:51:00','accessory',NULL,NULL,'2015-06-05 01:51:00',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),
	(43,10,'checkin from',NULL,NULL,5,'2015-06-05 01:51:39','accessory',NULL,NULL,'2015-06-05 01:51:39',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),
	(44,10,'checkout',NULL,NULL,5,'2015-06-05 01:57:11','accessory','',NULL,'2015-06-05 01:57:11',NULL,NULL,NULL,1,NULL,'DO100',18,NULL,5),
	(45,10,'checkin from',NULL,NULL,5,'2015-06-05 01:57:54','accessory',NULL,NULL,'2015-06-05 01:57:54',NULL,NULL,NULL,1,NULL,NULL,2,NULL,5),
	(46,10,'checkout',32,4,4,'2015-06-05 02:22:57','hardware','notes checkout',NULL,'2015-06-05 02:22:57',NULL,NULL,NULL,NULL,NULL,'DO12345',NULL,NULL,NULL),
	(47,10,'checkout',NULL,NULL,1,'2015-06-05 04:32:15','accessory','',NULL,'2015-06-05 04:32:15',NULL,NULL,NULL,1,NULL,'DO12345',10,NULL,1),
	(48,10,'checkout',NULL,NULL,5,'2015-06-05 04:37:03','accessory','',NULL,'2015-06-05 04:37:03',NULL,NULL,NULL,4,NULL,'DOHB001',21,NULL,5),
	(49,10,'checkout',NULL,NULL,5,'2015-06-05 04:43:57','accessory','',NULL,'2015-06-05 04:43:57',NULL,NULL,NULL,4,NULL,'DO11111',2,NULL,5),
	(50,10,'checkout',NULL,NULL,5,'2015-06-05 04:44:30','accessory','',NULL,'2015-06-05 04:44:30',NULL,NULL,NULL,4,NULL,'DO1919191',1,NULL,5),
	(51,10,'checkout',NULL,NULL,5,'2015-06-05 05:06:19','accessory','',NULL,'2015-06-05 05:06:19',NULL,NULL,NULL,4,NULL,'123',3,NULL,5),
	(52,10,'checkout',NULL,NULL,1,'2015-06-05 07:02:53','accessory','',NULL,'2015-06-05 07:02:53',NULL,NULL,NULL,3,NULL,'DO111233',1,NULL,1),
	(53,10,'deleteAsset',35,NULL,NULL,'2015-06-05 07:18:04','hardware','try delete',NULL,'2015-06-05 07:18:04',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(54,10,'deleteAsset',30,NULL,NULL,'2015-06-05 07:19:28','hardware','try delete 2',NULL,'2015-06-05 07:19:28',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(55,10,'deleteAccessory',NULL,NULL,NULL,'2015-06-05 10:11:15','accessory','delete acc',NULL,'2015-06-05 10:11:15',NULL,NULL,NULL,5,NULL,NULL,NULL,NULL,NULL),
	(56,10,'deleteUser',NULL,NULL,NULL,'2015-06-08 03:48:49','user','delete maria',NULL,'2015-06-08 03:48:49',NULL,NULL,NULL,6,NULL,NULL,NULL,NULL,NULL),
	(57,10,'deleteUser',NULL,NULL,NULL,'2015-06-08 03:51:19','user','delete maria 2nd\r\n',NULL,'2015-06-08 03:51:19',NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,NULL),
	(58,10,'deleteAccessory',NULL,NULL,NULL,'2015-06-09 01:32:09','accessory','delete accessory testacc',NULL,'2015-06-09 01:32:09',NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,NULL),
	(59,10,'checkout',26,NULL,NULL,'2015-06-09 04:50:40','hardware','checkout to site Felda Ayer Hitam',NULL,'2015-06-09 04:50:40',NULL,NULL,NULL,NULL,NULL,'DO123',NULL,NULL,80),
	(60,10,'checkin from',26,NULL,NULL,'2015-06-09 05:06:26','hardware','return from felda',NULL,'2015-06-09 05:06:26',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(61,10,'checkout',26,NULL,NULL,'2015-06-09 05:12:46','hardware','test bandar baru teluk intan',NULL,'2015-06-09 05:12:46',NULL,NULL,NULL,NULL,NULL,'DO112233',NULL,NULL,90),
	(62,10,'checkin from',26,NULL,NULL,'2015-06-10 02:27:23','hardware','test checkin asset',NULL,'2015-06-10 02:27:23',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(63,10,'checkout',26,NULL,NULL,'2015-06-10 02:30:09','hardware','note felda',NULL,'2015-06-10 02:30:09',NULL,NULL,NULL,NULL,NULL,'DO123',NULL,NULL,80),
	(64,10,'checkout',29,NULL,NULL,'2015-06-10 02:32:49','hardware','note felde bukit jalor',NULL,'2015-06-10 02:32:49',NULL,NULL,NULL,NULL,NULL,'DO1234',NULL,NULL,80),
	(65,10,'checkin from',29,NULL,NULL,'2015-06-10 02:33:46','hardware','test checkin felda',NULL,'2015-06-10 02:33:46',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(66,10,'checkout',29,NULL,NULL,'2015-06-10 02:34:54','hardware','felda bukit tangga checkout',NULL,'2015-06-10 02:34:54',NULL,NULL,NULL,NULL,NULL,'DO123',NULL,NULL,73),
	(67,10,'checkin from',26,NULL,NULL,'2015-06-10 02:35:57','hardware','checkin',NULL,'2015-06-10 02:35:57',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,80),
	(68,10,'checkout',NULL,NULL,2,'2015-06-10 08:46:15','accessory','',NULL,'2015-06-10 08:46:15',NULL,NULL,NULL,3,NULL,'DO123',12,NULL,2),
	(70,10,'checkout',NULL,NULL,2,'2015-06-10 09:15:16','accessory','',NULL,'2015-06-10 09:15:16',NULL,NULL,NULL,3,NULL,'DO123',15,NULL,2),
	(71,10,'checkout',NULL,NULL,7,'2015-06-11 02:38:02','accessory','',NULL,'2015-06-11 02:38:02',NULL,NULL,NULL,3,NULL,'DO12345',1,NULL,7),
	(72,10,'checkin from',NULL,NULL,7,'2015-06-11 02:44:18','accessory',NULL,NULL,'2015-06-11 02:44:18',NULL,NULL,NULL,3,NULL,NULL,1,NULL,7),
	(73,10,'checkout',4,NULL,68,'2015-06-16 03:18:38','hardware','masjid checkout',NULL,'2015-06-16 03:18:38',NULL,NULL,NULL,NULL,NULL,'DO123',NULL,NULL,97),
	(74,10,'checkout',14,NULL,73,'2015-06-16 03:19:06','hardware','felda bukit tangga checkout\r\n',NULL,'2015-06-16 03:19:06',NULL,NULL,NULL,NULL,NULL,'DO123',NULL,NULL,73),
	(75,10,'checkout',NULL,NULL,NULL,'2015-06-17 04:19:28','accessory','',NULL,'2015-06-17 04:19:28',NULL,NULL,NULL,1,NULL,'DO123',22,NULL,1),
	(76,10,'checkout',NULL,NULL,NULL,'2015-06-17 04:19:57','accessory','',NULL,'2015-06-17 04:19:57',NULL,NULL,NULL,1,NULL,'DO123',5,NULL,25),
	(77,10,'checkout',NULL,NULL,NULL,'2015-06-17 07:11:29','accessory','',NULL,'2015-06-17 07:11:29',NULL,NULL,NULL,1,NULL,'DO123',3,NULL,73);

/*!40000 ALTER TABLE `asset_logs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table asset_uploads
# ------------------------------------------------------------

DROP TABLE IF EXISTS `asset_uploads`;

CREATE TABLE `asset_uploads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `asset_id` int(11) NOT NULL,
  `filenotes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table assets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assets`;

CREATE TABLE `assets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `asset_tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model_id` int(11) DEFAULT NULL,
  `serial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `purchase_date` date DEFAULT NULL,
  `purchase_cost` decimal(13,4) NOT NULL DEFAULT '0.0000',
  `order_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `physical` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `warranty_months` int(3) DEFAULT NULL,
  `depreciate` tinyint(1) NOT NULL DEFAULT '0',
  `supplier_id` int(11) DEFAULT NULL,
  `requestable` tinyint(4) NOT NULL DEFAULT '0',
  `rtd_location_id` int(11) DEFAULT NULL,
  `mac_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dept_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `site_id` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `assets` WRITE;
/*!40000 ALTER TABLE `assets` DISABLE KEYS */;

INSERT INTO `assets` (`id`, `name`, `asset_tag`, `model_id`, `serial`, `purchase_date`, `purchase_cost`, `order_number`, `assigned_to`, `notes`, `user_id`, `created_at`, `updated_at`, `physical`, `deleted_at`, `status_id`, `archived`, `warranty_months`, `depreciate`, `supplier_id`, `requestable`, `rtd_location_id`, `mac_address`, `dept_id`, `type_id`, `site_id`)
VALUES
	(3,'Alison MBP','NNY287958796',1,'WS905869046069','2013-10-02',2435.9900,'987698576946',NULL,'',1,'2014-11-09 06:50:15','2014-11-09 06:50:15',1,NULL,2,0,NULL,0,NULL,0,NULL,NULL,1,NULL,NULL),
	(4,'Brady MBP','NNY78795566',2,'WS9078686069','2012-01-02',1999.9900,'657756',NULL,'',1,'2014-11-09 06:50:15','2015-06-16 03:18:38',1,NULL,1,0,NULL,0,NULL,0,5,NULL,2,NULL,97),
	(5,'Deborah MBP','NNY65756756775',2,'WS9078686069','2012-01-02',699.9900,'657756',NULL,'',2,'2014-11-09 06:50:15','2014-11-09 06:50:15',1,NULL,2,0,NULL,0,NULL,0,NULL,NULL,1,NULL,NULL),
	(6,'Sara MBP','NNY6897856775',2,'WS87897998Q','2012-01-02',1999.9900,'657756',2,'',2,'2014-11-09 06:50:15','2014-11-09 06:50:15',1,NULL,1,0,NULL,0,NULL,0,5,NULL,1,NULL,NULL),
	(7,'Ben MBP','NNY67567775',2,'WS89080890','2012-01-02',1999.9900,'657756',2,'',2,'2014-11-09 06:50:15','2014-11-09 06:50:15',1,NULL,1,0,NULL,0,NULL,0,5,NULL,1,NULL,NULL),
	(8,'Broken Laptop','NNY6756756775',2,'WS89080890','2012-01-02',1999.9900,'657756',NULL,'',2,'2014-11-09 06:50:15','2015-05-27 04:41:42',1,NULL,3,0,NULL,0,NULL,0,NULL,NULL,1,NULL,NULL),
	(9,'Maybe Broke-Ass Laptop','NNY6755667775',2,'WS45689080890','2012-01-02',1999.9900,'657756',NULL,'',1,'2014-11-09 06:50:15','2014-11-09 06:50:15',1,NULL,2,0,NULL,0,NULL,0,NULL,NULL,1,NULL,NULL),
	(10,'Completely Facacta Laptop','NNY6564567775',2,'WS99689080890','2012-01-02',1999.9900,'657756',NULL,'',1,'2014-11-09 06:50:15','2014-11-09 06:50:15',1,NULL,4,0,NULL,0,NULL,0,NULL,NULL,1,NULL,NULL),
	(11,'Drunken Shanenigans Laptop','NNY6564567775',2,'WS99689080890','2012-01-02',1999.9900,'657756',NULL,'',1,'2014-11-09 06:50:15','2014-11-09 06:50:15',1,NULL,2,0,NULL,0,NULL,0,NULL,NULL,1,NULL,NULL),
	(12,'Noah MBP','NNY98056775',2,'WS909098888','2011-12-20',699.9900,'657756',NULL,'',2,'2014-11-09 06:50:15','2014-11-09 06:50:15',1,NULL,2,0,24,0,NULL,0,5,NULL,2,NULL,NULL),
	(13,'','N4-OE-0005-2015',5,' 155263795','2015-04-23',2000.0000,'PO12345',NULL,'Notes Description',3,'2015-04-15 02:49:05','2015-06-09 02:19:02',1,NULL,10,0,12,0,0,0,1,'',2,NULL,NULL),
	(14,'Inspiron Games','N4-OE-0001-2015',5,'55555555','2015-04-15',0.0000,'PO11923',NULL,'nothing to note',3,'2015-04-15 04:52:29','2015-06-16 03:19:06',1,NULL,1,0,NULL,0,NULL,1,1,'',1,NULL,73),
	(15,'Head Quarters Inspiron Games','N4-OE-0002-2015',5,'88776566',NULL,0.0000,'',3,'',3,'2015-04-15 04:55:31','2015-05-26 17:05:53',1,NULL,1,0,NULL,0,NULL,0,0,'',1,NULL,NULL),
	(16,'MacBook GUI Department','OPS-GUI-LTP-0002',3,'888888','2015-04-15',0.0000,'',NULL,'asset macbook note',3,'2015-04-15 07:50:43','2015-04-15 07:50:43',1,NULL,2,0,12,0,0,0,NULL,'',2,NULL,NULL),
	(17,'GamesMonitorDepartment','N4-OE-0003-2015',5,'9988778','2015-04-16',0.0000,'PO762548',3,'notes games',3,'2015-04-16 07:58:57','2015-05-26 17:31:17',1,NULL,1,0,6,0,NULL,0,3,'',1,NULL,NULL),
	(26,'Shanen MBPs','HQ-IT-LTP-0001',4,'WS90585666669','0002-10-13',2435.9900,'98800000',NULL,'',3,'2015-04-22 04:01:21','2015-06-10 02:45:16',1,NULL,1,0,NULL,0,1,0,5,'',3,NULL,NULL),
	(27,'Michael MBP','HQ-IT-LTP-0002',2,' WS905823226669','0002-10-13',2435.9900,'98800000',NULL,'',3,'2015-04-22 04:01:21','2015-06-09 02:17:20',1,NULL,9,0,NULL,0,1,0,5,NULL,5,NULL,NULL),
	(28,'Alan MBP','RND-GMS-LTP-0001',4,' WS0001','2013-03-15',2000.8900,'123456780',NULL,'',3,'2015-04-22 04:01:21','2015-06-09 02:17:36',1,NULL,5,0,NULL,0,2,0,4,NULL,1,NULL,NULL),
	(29,'Lucy MBP','HQ-FR-LTP-0001',4,'WS0002','2013-03-15',2000.8900,'123456780',NULL,'',3,'2015-04-22 04:01:21','2015-06-10 02:34:54',1,NULL,1,0,NULL,0,2,0,4,NULL,4,NULL,73),
	(30,'Ridhuan Macbook','HQ-FR-LTP-0002',1,'99999988888','2015-04-28',4500.0000,'PO123456',NULL,'ridhuan notes',3,'2015-04-28 08:24:23','2015-06-05 07:19:28',1,'2015-06-05 07:19:28',2,0,12,0,2,0,5,'',4,NULL,NULL),
	(31,'MacBook Pro HR001','N4-OE-0004-2015',7,'SN123456','2015-05-07',7000.0000,'PO123456',8,'New MacBook Pro Laptop',3,'2015-05-15 08:13:01','2015-05-30 08:08:03',1,NULL,1,0,12,0,1,0,5,'',3,NULL,NULL),
	(32,'Macbook Pro Retina 13in','N1-OE-0001-2015',6,'C02P57ZQG3QH','2015-05-20',3773.0000,'TEST 123',4,'',11,'2015-05-30 10:04:06','2015-06-05 02:22:57',1,NULL,1,0,12,0,0,0,1,'',7,NULL,NULL),
	(33,'iPhone 4S','N1-OE0002-2015',7,'   C39GW37XDTDM','2015-05-01',2400.0000,'',3,'',11,'2015-05-31 14:08:48','2015-06-09 06:35:36',1,NULL,1,0,12,0,1,0,5,'',7,NULL,NULL),
	(34,'Laser Printer','N4-OE0006-2015',7,'SN-001-001-001','2015-04-01',152.0000,'PO-000001',NULL,'Batch CB 2014',12,'2015-06-01 03:28:57','2015-06-01 03:41:38',1,'2015-06-01 03:41:38',1,0,12,0,1,0,2,'',1,NULL,NULL),
	(35,'BMW 7 Series','N1-MV0001-2014',12,'  WQR 50','2014-08-04',300000.0000,'PO-000002',NULL,'For CEO Office',12,'2015-06-01 03:56:53','2015-06-05 07:18:04',1,'2015-06-05 07:18:04',7,0,60,0,NULL,0,0,'',7,NULL,NULL),
	(36,'Desk001','N1-FF0001-2015',1,'SN01','0001-06-15',300.0000,'PO123',NULL,'',10,'2015-06-01 04:46:03','2015-06-05 03:50:36',1,'2015-06-05 03:50:36',2,0,NULL,0,0,0,5,NULL,7,NULL,NULL),
	(37,'Desk002','N1-FF0002-2015',1,'SN02','2030-05-15',100.0000,'PO6677',NULL,'',10,'2015-06-01 04:46:04','2015-06-03 08:39:38',1,'2015-06-03 08:39:38',2,0,NULL,0,0,0,5,NULL,7,NULL,NULL),
	(38,'Desk003','N1-FF0001-2015',1,'SN03','2030-05-15',100.0000,'PO6677',NULL,'',10,'2015-06-12 04:38:49','2015-06-12 04:38:49',1,NULL,2,0,NULL,0,1,0,53,NULL,7,NULL,NULL),
	(39,'Desk004','N1-FF0002-2015',1,'SN04','2030-05-15',100.0000,'PO6677',NULL,'',10,'2015-06-12 04:38:49','2015-06-12 04:38:49',1,NULL,2,0,NULL,0,2,0,53,NULL,7,NULL,NULL);

/*!40000 ALTER TABLE `assets` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `eula_text` longtext COLLATE utf8_unicode_ci,
  `use_default_eula` tinyint(1) NOT NULL DEFAULT '0',
  `require_acceptance` tinyint(1) NOT NULL DEFAULT '0',
  `category_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'asset',
  `category_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`, `user_id`, `deleted_at`, `eula_text`, `use_default_eula`, `require_acceptance`, `category_type`, `category_code`)
VALUES
	(1,'Furniture and Fittings','2015-05-30 03:34:16','2015-05-30 03:34:16',10,NULL,'',0,0,'asset','FF'),
	(2,'Office Equipments','2015-05-30 03:34:39','2015-05-30 13:29:25',10,NULL,'',0,0,'asset','OE'),
	(3,'Computer Hardwares and Softwares','2015-05-30 03:34:59','2015-05-30 13:30:07',10,NULL,'',0,0,'asset','SH'),
	(4,'Capital Expenses','2015-05-30 03:35:14','2015-05-30 03:35:14',10,NULL,'',0,0,'asset','CE'),
	(5,'Motor Vehicles','2015-05-30 03:35:28','2015-06-01 04:07:40',10,NULL,'',0,0,'asset','MV'),
	(6,'Harta Dato&#039;','2015-06-01 03:54:14','2015-06-01 03:54:36',12,NULL,'',0,0,'asset','LC'),
	(7,'testcategory','2015-06-17 08:04:30','2015-06-17 08:04:51',10,NULL,'',0,0,'asset','TSTCAT');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cluster
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cluster`;

CREATE TABLE `cluster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `code` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `cluster` WRITE;
/*!40000 ALTER TABLE `cluster` DISABLE KEYS */;

INSERT INTO `cluster` (`id`, `name`, `created_at`, `user_id`, `deleted_at`, `code`)
VALUES
	(1,'Sabah Cluster A',NULL,NULL,NULL,'SCA'),
	(2,'Sabah Cluster B',NULL,NULL,NULL,'SCB'),
	(3,'Sabah Cluster C',NULL,NULL,NULL,'SCC'),
	(4,'Sarawak',NULL,NULL,NULL,'SRWK'),
	(5,'Semenanjung Cluster',NULL,NULL,NULL,'SMJC'),
	(6,'Semenanjung Cluster N','2015-01-29 11:20:52',1,NULL,'SMJN'),
	(7,'HQ','2015-03-12 13:00:06',1,NULL,'HQ');

/*!40000 ALTER TABLE `cluster` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table departments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `departments`;

CREATE TABLE `departments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `div_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;

INSERT INTO `departments` (`id`, `name`, `code`, `div_id`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'Pusat Internet 1 Malaysia','PIM',4,'2015-05-30 03:32:36','2015-05-30 07:58:48',NULL),
	(2,'Kampung Tanpa Wayar','KTW',4,'2015-05-30 03:32:51','2015-05-30 03:32:51',NULL),
	(3,'Fiber','FBR',4,'2015-05-30 03:33:04','2015-05-30 05:12:37',NULL),
	(4,'Celcom Enterprise','CEN',3,'2015-05-30 05:55:59','2015-05-30 07:58:00',NULL),
	(5,'Sabah Office Likas','SOL',2,'2015-05-30 05:56:38','2015-05-30 07:59:01',NULL),
	(6,'IVR','IVR',5,'2015-05-30 05:57:34','2015-05-30 07:58:33',NULL),
	(7,'Head Quarters','HQR',1,'2015-05-30 07:28:29','2015-05-30 07:58:19',NULL);

/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table depreciations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `depreciations`;

CREATE TABLE `depreciations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `months` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `depreciations` WRITE;
/*!40000 ALTER TABLE `depreciations` DISABLE KEYS */;

INSERT INTO `depreciations` (`id`, `name`, `months`, `created_at`, `updated_at`, `user_id`)
VALUES
	(1,'Computers',36,'2015-04-01 06:50:15','2015-04-01 06:50:15',1),
	(2,'Software',36,'2015-04-01 06:50:15','2015-04-01 06:50:15',1),
	(3,'Office Equipment',36,'2015-04-01 06:50:15','2015-04-01 06:50:15',1);

/*!40000 ALTER TABLE `depreciations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table divisions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `divisions`;

CREATE TABLE `divisions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `divisions` WRITE;
/*!40000 ALTER TABLE `divisions` DISABLE KEYS */;

INSERT INTO `divisions` (`id`, `name`, `code`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'Head Quarters','N1','2015-05-30 03:30:22','2015-05-30 05:10:27',NULL),
	(2,'Sabah Office, Likas','N2','2015-05-30 03:31:14','2015-05-30 05:08:21',NULL),
	(3,'Celcom Enterprise','N3','2015-05-30 03:31:27','2015-05-30 03:31:27',NULL),
	(4,'Universal Service Provider (USP)','N4','2015-05-30 03:31:52','2015-05-30 03:31:52',NULL),
	(5,'IVR','N5','2015-05-30 03:33:17','2015-05-30 03:33:17',NULL);

/*!40000 ALTER TABLE `divisions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `groups_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `name`, `permissions`, `created_at`, `updated_at`)
VALUES
	(1,'Admin','{\"admin\":1,\"reports\":1}','2015-04-13 07:10:26','2015-04-22 07:52:53'),
	(2,'Reporting','{\"reports\":1}','2015-04-13 07:10:26','2015-04-13 07:10:26'),
	(3,'Users','{\"users\":1}','2015-04-13 07:10:26','2015-04-13 07:10:26'),
	(4,'Finance','{\"reports\":1,\"finance\":1}','2015-04-20 08:03:33','2015-04-21 07:18:42'),
	(5,'Super Admin','{\"admin\":1,\"reports\":1,\"finance\":1}','2015-04-20 08:07:42','2015-04-22 01:19:21');

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `history`;

CREATE TABLE `history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `checkedout_to` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table license_seats
# ------------------------------------------------------------

DROP TABLE IF EXISTS `license_seats`;

CREATE TABLE `license_seats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `license_id` int(11) NOT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `asset_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `license_seats` WRITE;
/*!40000 ALTER TABLE `license_seats` DISABLE KEYS */;

INSERT INTO `license_seats` (`id`, `license_id`, `assigned_to`, `notes`, `user_id`, `created_at`, `updated_at`, `deleted_at`, `asset_id`)
VALUES
	(1,1,1,'',1,'2015-04-04 06:50:15','2015-04-01 06:50:15',NULL,NULL),
	(2,1,2,'',1,'2015-03-22 06:50:15','2015-03-19 06:50:15',NULL,NULL),
	(3,1,2,'',1,'2015-03-09 06:50:15','2015-03-06 06:50:15',NULL,NULL),
	(4,1,NULL,'',1,'2015-02-24 06:50:15','2015-02-21 06:50:15',NULL,NULL),
	(5,1,2,'',1,'2015-02-11 06:50:15','2015-02-08 06:50:15',NULL,NULL),
	(6,2,1,'',1,'2015-01-29 06:50:15','2015-01-26 06:50:15',NULL,NULL),
	(7,2,NULL,'',1,'2015-01-16 06:50:15','2015-01-13 06:50:15',NULL,NULL);

/*!40000 ALTER TABLE `license_seats` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table licenses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `licenses`;

CREATE TABLE `licenses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `serial` text COLLATE utf8_unicode_ci,
  `purchase_date` date DEFAULT NULL,
  `purchase_cost` decimal(13,4) DEFAULT NULL,
  `order_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seats` int(11) NOT NULL DEFAULT '1',
  `notes` text COLLATE utf8_unicode_ci,
  `user_id` int(11) NOT NULL,
  `depreciation_id` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `license_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `license_email` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `depreciate` tinyint(1) DEFAULT '0',
  `supplier_id` int(11) DEFAULT NULL,
  `expiration_date` date DEFAULT NULL,
  `purchase_order` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `termination_date` date DEFAULT NULL,
  `maintained` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `licenses` WRITE;
/*!40000 ALTER TABLE `licenses` DISABLE KEYS */;

INSERT INTO `licenses` (`id`, `name`, `serial`, `purchase_date`, `purchase_cost`, `order_number`, `seats`, `notes`, `user_id`, `depreciation_id`, `created_at`, `updated_at`, `deleted_at`, `license_name`, `license_email`, `depreciate`, `supplier_id`, `expiration_date`, `purchase_order`, `termination_date`, `maintained`)
VALUES
	(1,'Adobe Photoshop CS6','ZOMG-WtF-BBQ-SRSLY','2013-10-02',2435.9900,'987698576946',5,'',1,2,'2015-03-19 06:50:15','2015-03-19 06:50:15',NULL,'','',0,NULL,NULL,'',NULL,0),
	(2,'Git Tower','98049890394-340485934','2013-10-02',2435.9900,'987698576946',2,'',1,2,'2015-03-19 06:50:15','2015-03-19 06:50:15',NULL,'Alison Gianotto','snipe@snipe.net',0,NULL,NULL,'',NULL,0);

/*!40000 ALTER TABLE `licenses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table locations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `locations`;

CREATE TABLE `locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) NOT NULL,
  `address` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_ktw` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_pi1m` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_fiber` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cluster_id` int(3) DEFAULT NULL,
  `state_id` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;

INSERT INTO `locations` (`id`, `name`, `city`, `state`, `country`, `created_at`, `updated_at`, `user_id`, `address`, `address2`, `zip`, `deleted_at`, `code`, `is_ktw`, `is_pi1m`, `is_fiber`, `cluster_id`, `state_id`)
VALUES
	(1,'Kg Kopimpinan, Penampang','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1 Malaysia Kg. Kopimpinan, Dewan Raya Kanangan, 89507  Penampang,','','89507',NULL,'2200011','1','1',NULL,5,12),
	(2,'Kg Duvanson Ketiau, Putatan','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1 Malaysia,\r\rRumah Marcus Mojigoh,\r\rKg Duvanson Ketiau,\r\r88200 Pu','','88200',NULL,'2200012','1','1',NULL,5,12),
	(3,'Kg Kuala, Papar','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1 Malaysia\rKg Kuala, Lorong Perumahan 16\r89600 Papar,\rSabah','','89600',NULL,'2200018','1','1',NULL,5,12),
	(4,'Kg Biau, Bongawan Papar','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Kampung Biau,\r\r89700 Bongawan,\r\rPapar, Sabah.','','89700',NULL,'2200019','1','1',NULL,5,12),
	(5,'Kg. Lawa Kabajang, Beaufort ','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1 Malaysia,\r\rJalan Beaufort - Bintuka,\r\rTingkat 1, Bangunan Uruse','','89808',NULL,'2200022','1','1',NULL,5,12),
	(6,'Kg Bundu, Kuala Penyu','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1 Malaysia,\r\rKg Bundu, 89747 Kuala Penyu, \r\rSabah','','89747',NULL,'2200020','1','1',NULL,5,12),
	(7,'Pekan Kuala Penyu','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1 Malaysia terletak di bangunan Shoplot tapak tamu, lot 4 tingkat','','89740',NULL,'2200017','1','1',NULL,5,12),
	(8,'Pekan Menumbok','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet Satu Malaysia, \r\r89767 Menumbok Sabah.','','89767',NULL,'2200023','1','1',NULL,5,12),
	(9,'Pekan Putera Jaya','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pi1M Putera Jaya,\r\rS/HSE Ground Floor, Lot H12,\r\rTaman Putera Jaya, Telipok,\r\rJa','','',NULL,'2200119','1','1',NULL,5,12),
	(10,'Pekan Babagon','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Kampung Babagon, \r\rKm21 Jalan Penampang-Tambunan\r\r89507 Penampang,\r\rSabah','','89507',NULL,'2200123','1','1',NULL,5,12),
	(11,'Kg. Inobong','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Kampung Inobong, Penampang 89500, Kota Kinabalu, Sabah.','','89500',NULL,'2200116','1','1',NULL,5,12),
	(12,'Kg Muhibbah, Putatan','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'   Lorong 3, Taman Jumbo,\r\r   88200, Petagas,Putatan,\r\r   Sabah.','','88200',NULL,'2200122','1','1',NULL,5,12),
	(13,'Pekan Kg Langkuas','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1 Malaysia Kg Langkuas,\r\rJalan Papar Lama\r\rKinarut 89600 Papar Sa','','89600',NULL,'2200112','1','1',NULL,5,12),
	(14,'Kg Belatik','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Kg Belatik,\r\r89600 Kimanis Papar.','','89600',NULL,'2200111','1','1',NULL,5,12),
	(15,'Rumah Kebajikan OKU Kimanis','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Kompleks Pemulihan Orang Kurang Upaya Kimanis,\r\rWDT 82, 89600 Papar, Sabah.','','89600',NULL,'2200125','1','1',NULL,5,12),
	(16,'Pekan Kg. Bambangan','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'New Site','','',NULL,'2200124','1','1',NULL,5,12),
	(17,'Pekan Kg Lubok','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1 Malaysia Kg. Lubok Weston, 89800 Beaufort, Sabah','','89800',NULL,'2200110','1','1',NULL,5,12),
	(18,'Kuala Mengalong','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'KUALA MENGALONG\r\rJALAN SINDUMIN 89857 SIPITANG SABAH','','89857',NULL,'2200118','1','1',NULL,5,12),
	(19,'Lubok Temiang','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1Malaysia,\r\rKg. Lubok Temiang,\r\rZon Samja, Kg Lubok Temiang,\r\rLab','','',NULL,'2200115','1','1',NULL,5,12),
	(20,'Kg Batu Payung, Tawau','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1Malaysia Kg. Batu Payung,\r\rKg. Batu Payung,\r\rBatu 10, Jalan Tina','','91000',NULL,'2200026','1','1',NULL,5,12),
	(21,'Kg Airport, Kunak','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Kampung Airport Batu 10, Kunak, Sabah.','','',NULL,'2200010','1','1',NULL,5,12),
	(22,'Kg Hampilan, Kunak','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet Kampung Hampilan,\r\rJalan Kunak - Semporna,\r\rKampung Pangi,\r\r91207','','91207',NULL,'2200009','1','1',NULL,5,12),
	(23,'Kg Kadazan, Kunak','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'PI1M Kg. Kadazan,\r\rKg. Kadazan, Jalan Kunak-Tawau,\r\rP/S 550, 91207 Kunak, Sabah.','','91207',NULL,'2000009','1','1',NULL,5,12),
	(24,'Kg Lormalong, Kunak','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Jalan Lahad Datu - Kunak,\r\rKg Lormalong,\r\r91207, Kunak.\r\rSabah','','91207',NULL,'2000008','1','1',NULL,5,12),
	(25,'Pekan Kunak, Kunak','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1 Malaysia  Pekan Kunak,\r\rUnit 3, bangunan SEDCO,\r\r91207, Kunak.','','91207',NULL,'2000010','1','1',NULL,5,12),
	(26,'Pekan Tungku, Tungku','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1 Malaysia,\r\rBangunan Urusetia Lama Tungku,\r\r91109 Tungku, Lahad ','','91109',NULL,'2200027','1','1',NULL,5,12),
	(27,'Pekan Beluran, Beluran','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'New Site','','',NULL,'2200007','1','1',NULL,5,12),
	(28,'Kg Kuala Sapi, Beluran','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Bangunan Pejabat JKKK, Kg. Contoh Kuala Sapi, 90107 Beluran, Sabah.','','90107',NULL,'2000003','1','1',NULL,5,12),
	(29,'Kg Bintang Mas, Beluran','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'KOPERASI TANAH PEMBANGUNAN SG SAPI, KG BINTANG MAS, 90000 SANDAKAN ,SABAH','','90000',NULL,'2000001','1','1',NULL,5,12),
	(30,'Pekan Telupid, Beluran','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1 Malaysia (Pi1M) Pekan Telupid\r\rPejabat Daerah Kecil Telupid\r\rPe','','89320',NULL,'2000002','1','1',NULL,5,12),
	(31,'Kg Wonod, Beluran','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet Kampung Wonod\r\rp/s 22, 89320 Telupid, Sabah.','','89320',NULL,'2200008','1','1',NULL,5,12),
	(32,'Kg Linayukan, Tongod','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'New Site','','',NULL,'2000014','1','1',NULL,5,12),
	(33,'Kg Sogo Sogo, Tongod','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1Malaysia Kg. Sogo-Sogo, 89320 Telupid, Tongod, Sabah','','89320',NULL,'2000015','1','1',NULL,5,12),
	(34,'Pekan Tongod, Tongod','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Sebelah Perpustakaan Negeri Sabah Cawangan Tongod, 89300 Telupid, Sabah.','','89300',NULL,'2000013','1','1',NULL,5,12),
	(35,'Kg Balung Cocos, Tawau','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Kg. Balung Cocos, P/S 696,\r\r91008, Tawau,\r\rSabah','','91008',NULL,'2200025','1','1',NULL,5,12),
	(36,'Layung Industrial, Lahad Datu','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pi1m Layung Industrial,\r\rMDLD 8318 Blok A lot 12,\r\r1st Floor Layung Industrial,\r','','91100',NULL,'2200028','1','1',NULL,5,12),
	(37,'Kg Bugaya, Semporna','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'New Site','','',NULL,'2200117','1','1',NULL,5,12),
	(38,'Pejabat Daerah Kudat, Kudat (AM)','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'New Site','','',NULL,'2200016','1','1',NULL,5,12),
	(39,'Kg Tandek, Kota Marudu','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1 Malaysia, Pejabat Daerah Kota Marudu, Dewan Serbaguna Kg. Tande','','',NULL,'2200015','1','1',NULL,5,12),
	(40,'Kg Sg Damit, Tuaran','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1 Malaysia,Kg.Sungai Damit, 89208 Tuaran, Sabah','','89208',NULL,'2200014','1','1',NULL,5,12),
	(41,'Kg Malanggang Baru, Kiulu','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet Kg.Malangang Baru,Kiulu\r\r89257, Tamparuli\r\rSabah.','','89257',NULL,'2200013','1','1',NULL,5,12),
	(42,'Kg Mesilou, Ranau','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'New Site','','',NULL,'2200005','1','1',NULL,5,12),
	(43,'Kg. Desa Aman, Ranau','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'New Site','','',NULL,'2000011','1','1',NULL,5,12),
	(44,'Kg Lohan, Ranau','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'PI1M KG.LOHAN \r\rKG.LOHAN SKIM 1\r\r89307 RANAU\r\rSABAH','','89307',NULL,'2000012','1','1',NULL,5,12),
	(45,'Kg Bongkud, Ranau','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Kg. Bongkud 89308 Ranau, Sabah.','','89308',NULL,'2200006','1','1',NULL,5,12),
	(46,'Kg Toboh, Tambunan','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pi1M Kg Toboh\r\rPeti Surat 18, Kampung Toboh,\r\r89657, Tambunan, Sabah','','89657',NULL,'2200021','1','1',NULL,5,12),
	(47,'Kg Bingkor, Keningau','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Tingkat 1 Dewan Serbaguna Masyarakat Bingkor,\r\r89007 Keningau,\r\rSabah','','89007',NULL,'2000006','1','1',NULL,5,12),
	(48,'Kg Sook, Keningau','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Tingkat 1, Dewan Serbaguna Sook, 89008 Keningau, Sabah.','','89008',NULL,'2000007','1','1',NULL,5,12),
	(49,'Kg Apin Apin, Keningau','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1Malaysia Kg. Apin-Apin,\r\rTingkat 1, Dewan Masyarakat Apin-Apin,\r','','89008',NULL,'2200001','1','1',NULL,5,12),
	(50,'Kg Bunsit, Keningau','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1 Malaysia, Kg Bunsit, 89008 Bingkor,Keningau, Sabah','','89008',NULL,'2200002','1','1',NULL,5,12),
	(51,'Taman Sabana, Keningau','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Lebrien Marium (M) Pusat Internet 1 Malaysia Taman Sabana Lorong 489008, Keninga','','48900',NULL,'2200003','1','1',NULL,5,12),
	(52,'Kg Malima, Keningau','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Peti Surat 1214 Sook\r\r89008 Keningau\r\rSabah','','89008',NULL,'2200004','1','1',NULL,5,12),
	(53,'Kg Sawang (AM)','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'PUSAT INTERNET 1 MALAYSIA KG SAWANG,\r\rPETI SURAT 200, 89908 TENOM\r\rSABAH','','89908',NULL,'2200024','1','1',NULL,5,12),
	(54,'Kg Suang Punggur','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1Malaysia Kg Suang Punggur\r\rJalan Kuala Abai\r\r89150 Kota Belud.','','89150',NULL,'2200114','1','1',NULL,5,12),
	(55,'Pekan Kg Serusop, Tuaran','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1Malaysia Kampung Serusup Tuaran,\r\rPeti Surat 609,\r\r89008 Tuaran.','','89008',NULL,'2200120','1','1',NULL,5,12),
	(56,'Kg Pamilaan','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1 Malaysia Kampung Pamilaan,\r\rP/s 129 Pamilaan,\r\r89907 Tenom,\r\rSa','','89907',NULL,'2200121','1','1',NULL,5,12),
	(57,'Pekan Rampayan Laut','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Ostrich Village Lodge,\r\rKg Rampayan Laut,\r\r89150 Kota Belud,\r\rSabah.\r\r','','89150',NULL,'2200113','1','1',NULL,5,12),
	(58,'Rumah Benjamin','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'New Site','','',NULL,'2000004','1','1',NULL,5,13),
	(59,'Kg. Nanga Tada','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'New Site','','',NULL,'2200029','1','1',NULL,5,13),
	(60,'Kg. Muhibbah','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'New Site','','',NULL,'2200030','1','1',NULL,5,13),
	(61,'Felda Kahang Barat','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'PUSAT INTERNET 1 MALAYSIA\rBangunan Bilik Bacaan, \rJalan Mata Kucing,\rFelda Kahan','','86000',NULL,'2100005','1','1',NULL,5,1),
	(62,'Felda Ayer Hitam','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1 Malaysia, Jalan Rahmat, Felda Ayer Hitam, 86000 Kluang, Johor.','','86000',NULL,'2100004','1','1',NULL,5,1),
	(63,'Felda Kahang Timur','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1 Malaysia Felda Kahang Timur\r\rBalairaya Felda Kahang TImur\r\r8600','','86000',NULL,'2100006','1','1',NULL,5,1),
	(64,'Felda Bukit Tongkat','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'No 1 Jalan Anggerik,\r\rPusat Jalur Lebar, Felda Bukit Tongkat,\r\r86000, Kluang, Jo','','86000',NULL,'2100007','1','1',NULL,5,1),
	(65,'Felda Palong Timur 1','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1Malaysia Bangunan Jkkr\rFelda Palong TImur 1, Batu Anam \r85100 Se','','85100',NULL,'2100010','1','1',NULL,5,1),
	(66,'Felda Palong Timur 3','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet Felda Palong Timur 3,\r\rBangunan GPW, Jln Teratai 3,\r\rFelda Palong','','85100',NULL,'2100012','1','1',NULL,5,1),
	(67,'Felda Bukit Permai','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1 Malaysia\r\rBangunan Belia\r\rFelda Bukit Permai\r\r81850 Layang-Laya','','81850',NULL,'2100002','1','1',NULL,5,1),
	(68,'Felda Bukit Batu','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'PUSAT INTERNET 1MALAYSIA,\rBANGUNAN JKKK FELDA BUKIT BATU,\r81020 KULAIJAYA,\rJOHOR','','81020',NULL,'2100001','1','1',NULL,5,1),
	(69,'Felda Layang-Layang','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Bangunan Balairaya\r\rJalan Bayan\r\rFelda Layang-Layang\r\r86200 Simpang Renggam\r\rJoh','','86200',NULL,'2100008','1','1',NULL,5,1),
	(70,'Kg. Parit Hj. Idris','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Balai Raya, Kg Parit Hj Idris, \r\rPHI 7, Bukit Gambir,\r\r84800 Ledang, \r\rJohor','','84800',NULL,'2100009','1','1',NULL,5,1),
	(71,'Felda Inas Utara','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Bangunan GPW, Felda Inas Utara, 81000 Kulaijaya, Johor Darul Ta\'zim.','','81000',NULL,'2100003','1','1',NULL,5,1),
	(72,'Kg. Mensudut Lama','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1 Malaysia,\r\rDewan Serbaguna Kg Mensudut Lama,\r\r85100 Batu Anam,\r','','85100',NULL,'2100011','1','1',NULL,5,1),
	(73,'Felda Bukit Tangga','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'BTA25/4/1 DEWAN MAKAN JLN FELDA BUKIT TANGGA, FELDA BUKIT TANGGA, 06050 BUKIT KA','','06050',NULL,'2100019','1','1',NULL,5,2),
	(74,'PPR Pantai','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'100-00-10 PPR Seri Pantai,\r\rJalan Pantai Dalam,\r\r59200 Kuala Lumpur','','59200',NULL,'2200107','1','1',NULL,5,14),
	(75,'PPR Kerinchi','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Unit 1-00-01, Block C, PPR Kerinchi, Jalan Pantai Permai, Off Jalan Pantai Dalam','','59200',NULL,'2200109','1','1',NULL,5,14),
	(76,'Felda Kepis','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1 Malaysia Felda Kepis,\rBangunan JKKR (F) Kepis,\r72100 Bahau,Nege','','72100',NULL,'2100013','1','1',NULL,5,5),
	(77,'Felda Jelai 4','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1Malaysia (f) Jelai 4 ,\r\rBangunan Balairaya (f) Jelai 4,\r\r73480 G','','73480',NULL,'2100014','1','1',NULL,5,5),
	(78,'Felda Pasir Besar','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'PUSAT INTERNET 1MALAYSIA\r\rBANGUNAN FELDA TRADING, FELDA PASIR BESAR\r\r73420 GEMAS','','73420',NULL,'2100015','1','1',NULL,5,5),
	(79,'Felda Sg Kelamah','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Bangunan Sekolah Agama, Felda Sg. Kelamah, Gemas,73400 Negeri Sembilan','','73400',NULL,'2100016','1','1',NULL,5,5),
	(80,'Felda Bukit Jalor','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1 Malaysia Felda Bukit Jalor,\r\rBangunan GPW, Felda Bukit Jalor,\r\r','','73200',NULL,'2100017','1','1',NULL,5,5),
	(81,'Felda Bukit Rokan','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1 Malaysia FELDA Bukit Rokan\r\rd/a Balairaya Masjid FELDA Bukit Ro','','73200',NULL,'2100018','1','1',NULL,5,5),
	(82,'Felda Chuping','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'BANGUNAN BARU  ATAS TANAH LAPANG, DEPAN MASJID, P/O PEJABAT FELDA CHUPING,\r\r0250','','02500',NULL,'2100020','1','1',NULL,5,9),
	(83,'Felda Mata Air','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Pusat Internet 1 Malaysia Felda Mata Air\r\rPejabat Koperasi Felda Mata Air,\r\r0210','','02100',NULL,'2100021','1','1',NULL,5,9),
	(84,'Felda Rimba Mas','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'No 1 Bangunan belia dan GPW\r\rFelda RImba Mas\r\r02100 Padang Besar\r\rPerlis','','02100',NULL,'2100022','1','1',NULL,5,9),
	(85,'Laman Pengurus Pi1M','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'New Site','','',NULL,'0','1','1',NULL,5,10),
	(86,'Kg. Padang Lalang','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Kg. Padang Lalang, Ayer Hangat, Langkawi','','',NULL,'2200153','1','1',NULL,5,2),
	(87,'Kg. Tambalang','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Shop No. 3, Taman Ria Shoplot, Kg. Tambalang, 89200 Tuaran, Sabah.','','89200',NULL,'0','1','1',NULL,5,12),
	(88,'Pekan Membakut','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'New Site','','',NULL,'0','1','1',NULL,5,12),
	(89,'Pekan Weston','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'New Site','','',NULL,'0','1','1',NULL,5,12),
	(90,'Bandar Baru Teluk Intan','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'New Site','','',NULL,'0','1','1',NULL,5,8),
	(91,'Taman Muhibbah, Teluk Intan','','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'New Site','','',NULL,'0','1','1',NULL,5,8),
	(1001,'NuSuara','Petaling Jaya','','my','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Unit No.2-19-01 Block 2 VSQ@PJ City Centre Jalan Utara','','46200',NULL,'1001','1','1',NULL,5,8);

/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table manufacturers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `manufacturers`;

CREATE TABLE `manufacturers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `manufacturers` WRITE;
/*!40000 ALTER TABLE `manufacturers` DISABLE KEYS */;

INSERT INTO `manufacturers` (`id`, `name`, `created_at`, `updated_at`, `user_id`, `deleted_at`)
VALUES
	(1,'Apple','2015-04-14 00:00:00','2015-04-14 00:00:00',1,NULL),
	(2,'Microsoft','2015-04-14 00:00:00','2015-04-14 00:00:00',1,NULL),
	(3,'ASUS','2015-04-14 00:00:00','2015-05-28 04:16:40',1,'2015-05-28 04:16:40'),
	(4,'Dell','2015-04-14 00:00:00','2015-04-14 00:00:00',1,NULL),
	(5,'Ikea','2015-05-28 04:13:38','2015-05-28 04:13:38',10,NULL),
	(6,'General','2015-05-30 00:00:00','2015-05-30 00:00:00',1,NULL);

/*!40000 ALTER TABLE `manufacturers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`migration`, `batch`)
VALUES
	('2012_12_06_225921_migration_cartalyst_sentry_install_users',1),
	('2012_12_06_225929_migration_cartalyst_sentry_install_groups',1),
	('2012_12_06_225945_migration_cartalyst_sentry_install_users_groups_pivot',1),
	('2012_12_06_225988_migration_cartalyst_sentry_install_throttle',1),
	('2013_03_23_193214_update_users_table',2),
	('2013_11_13_075318_create_models_table',2),
	('2013_11_13_075335_create_categories_table',2),
	('2013_11_13_075347_create_manufacturers_table',2),
	('2013_11_15_015858_add_user_id_to_categories',2),
	('2013_11_15_112701_add_user_id_to_manufacturers',2),
	('2013_11_15_190327_create_assets_table',2),
	('2013_11_15_190357_create_licenses_table',2),
	('2013_11_15_201848_add_license_name_to_licenses',2),
	('2013_11_16_040323_create_depreciations_table',2),
	('2013_11_16_042851_add_depreciation_id_to_models',2),
	('2013_11_16_084923_add_user_id_to_models',2),
	('2013_11_16_103258_create_locations_table',2),
	('2013_11_16_103336_add_location_id_to_assets',2),
	('2013_11_16_103407_add_checkedout_to_to_assets',2),
	('2013_11_16_103425_create_history_table',2),
	('2013_11_17_054359_drop_licenses_table',2),
	('2013_11_17_054526_add_physical_to_assets',2),
	('2013_11_17_055126_create_settings_table',2),
	('2013_11_17_062634_add_license_to_assets',2),
	('2013_11_18_134332_add_contacts_to_users',2),
	('2013_11_18_142847_add_info_to_locations',2),
	('2013_11_18_152942_remove_location_id_from_asset',2),
	('2013_11_18_164423_set_nullvalues_for_user',2),
	('2013_11_19_013337_create_asset_logs_table',2),
	('2013_11_19_061409_edit_added_on_asset_logs_table',2),
	('2013_11_19_062250_edit_location_id_asset_logs_table',2),
	('2013_11_20_055822_add_soft_delete_on_assets',2),
	('2013_11_20_121404_add_soft_delete_on_locations',2),
	('2013_11_20_123137_add_soft_delete_on_manufacturers',2),
	('2013_11_20_123725_add_soft_delete_on_categories',2),
	('2013_11_20_130248_create_status_labels',2),
	('2013_11_20_130830_add_status_id_on_assets_table',2),
	('2013_11_20_131544_add_status_type_on_status_labels',2),
	('2013_11_20_134103_add_archived_to_assets',2),
	('2013_11_21_002321_add_uploads_table',2),
	('2013_11_21_024531_remove_deployable_boolean_from_status_labels',2),
	('2013_11_22_075308_add_option_label_to_settings_table',2),
	('2013_11_22_213400_edits_to_settings_table',2),
	('2013_11_25_013244_create_licenses_table',2),
	('2013_11_25_031458_create_license_seats_table',2),
	('2013_11_25_032022_add_type_to_actionlog_table',2),
	('2013_11_25_033008_delete_bad_licenses_table',2),
	('2013_11_25_033131_create_new_licenses_table',2),
	('2013_11_25_033534_add_licensed_to_licenses_table',2),
	('2013_11_25_101308_add_warrantee_to_assets_table',2),
	('2013_11_25_104343_alter_warranty_column_on_assets',2),
	('2013_11_25_150450_drop_parent_from_categories',2),
	('2013_11_25_151920_add_depreciate_to_assets',2),
	('2013_11_25_152903_add_depreciate_to_licenses_table',2),
	('2013_11_26_211820_drop_license_from_assets_table',2),
	('2013_11_27_062510_add_note_to_asset_logs_table',2),
	('2013_12_01_113426_add_filename_to_asset_log',2),
	('2013_12_06_094618_add_nullable_to_licenses_table',2),
	('2013_12_10_084038_add_eol_on_models_table',2),
	('2013_12_12_055218_add_manager_to_users_table',3),
	('2014_01_28_031200_add_qr_code_to_settings_table',3),
	('2014_02_13_183016_add_qr_text_to_settings_table',3),
	('2014_05_24_093839_alter_default_license_depreciation_id',3),
	('2014_05_27_231658_alter_default_values_licenses',3),
	('2014_06_19_191508_add_asset_name_to_settings',3),
	('2014_06_20_004847_make_asset_log_checkedout_to_nullable',3),
	('2014_06_20_005050_make_asset_log_purchasedate_to_nullable',3),
	('2014_06_24_003011_add_suppliers',3),
	('2014_06_24_010742_add_supplier_id_to_asset',3),
	('2014_06_24_012839_add_zip_to_supplier',3),
	('2014_06_24_033908_add_url_to_supplier',3),
	('2014_07_08_054116_add_employee_id_to_users',3),
	('2014_07_09_134316_add_requestable_to_assets',3),
	('2014_07_17_085822_add_asset_to_software',3),
	('2014_07_17_161625_make_asset_id_in_logs_nullable',3),
	('2014_08_12_053504_alpha_0_4_2_release',3),
	('2014_08_17_083523_make_location_id_nullable',3),
	('2014_10_16_200626_add_rtd_location_to_assets',3),
	('2014_10_24_000417_alter_supplier_state_to_32',3),
	('2014_10_24_015641_add_display_checkout_date',3),
	('2014_10_28_222654_add_avatar_field_to_users_table',3),
	('2014_10_29_045924_add_image_field_to_models_table',3),
	('2014_11_01_214955_add_eol_display_to_settings',3),
	('2014_11_04_231416_update_group_field_for_reporting',3),
	('2014_11_05_212408_add_fields_to_licenses',3),
	('2014_11_07_021042_add_image_to_supplier',3),
	('2014_11_20_203007_add_username_to_user',3),
	('2014_11_20_223947_add_auto_to_settings',3),
	('2014_11_20_224421_add_prefix_to_settings',3),
	('2014_11_21_104401_change_licence_type',3),
	('2014_12_09_082500_add_fields_maintained_term_to_licenses',3),
	('2015_02_04_155757_increase_user_field_lengths',3),
	('2015_02_07_013537_add_soft_deleted_to_log',3),
	('2015_02_10_040958_fix_bad_assigned_to_ids',3),
	('2015_02_10_053310_migrate_data_to_new_statuses',3),
	('2015_02_11_044104_migrate_make_license_assigned_null',3),
	('2015_02_11_104406_migrate_create_requests_table',3),
	('2015_02_12_001312_add_mac_address_to_asset',3),
	('2015_02_12_024100_change_license_notes_type',3),
	('2015_02_17_231020_add_localonly_to_settings',3),
	('2015_02_19_222322_add_logo_and_colors_to_settings',3),
	('2015_02_24_072043_add_alerts_to_settings',3),
	('2015_02_25_022931_add_eula_fields',3),
	('2015_02_25_204513_add_accessories_table',3),
	('2015_02_26_091228_add_accessories_user_table',3),
	('2015_02_26_115128_add_deleted_at_models',3),
	('2015_02_26_233005_add_category_type',3),
	('2015_03_01_231912_update_accepted_at_to_acceptance_id',3),
	('2015_03_05_011929_add_qr_type_to_settings',3),
	('2015_04_13_043628_create_divisions_table',4),
	('2015_04_14_035504_create_types_table',4),
	('2015_04_14_055214_create_department_table',5),
	('2015_04_14_061607_add_soft_delete_on_departments',5),
	('2015_04_14_065942_add_soft_delete_on_divisions',5),
	('2015_04_14_085749_add_deptid_typeid_tables',6),
	('2015_04_15_034258_add_category_code',7),
	('2015_04_14_065954_add_soft_delete_on_types',1),
	('2015_04_17_081358_add_code_column',8);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table models
# ------------------------------------------------------------

DROP TABLE IF EXISTS `models`;

CREATE TABLE `models` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modelno` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `depreciation_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `eol` int(11) DEFAULT '0',
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `show_mac_address` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `models` WRITE;
/*!40000 ALTER TABLE `models` DISABLE KEYS */;

INSERT INTO `models` (`id`, `name`, `modelno`, `manufacturer_id`, `category_id`, `created_at`, `updated_at`, `depreciation_id`, `user_id`, `eol`, `image`, `show_mac_address`, `deleted_at`)
VALUES
	(1,'Desks','DSK',0,1,'2015-05-30 03:38:56','2015-05-31 15:28:06',0,10,0,NULL,0,NULL),
	(2,'Chairs','CHR',0,1,'2015-05-30 03:39:53','2015-05-31 15:28:11',0,10,0,NULL,0,NULL),
	(3,'Rack','RCK',6,1,'2015-05-30 03:40:18','2015-05-30 03:40:18',0,10,0,NULL,0,NULL),
	(4,'Furniture and Fitting','FF',6,1,'2015-05-30 03:40:42','2015-05-30 13:28:28',0,10,0,NULL,0,NULL),
	(5,'Desktop','DSK',6,2,'2015-05-30 03:41:05','2015-05-30 03:41:05',0,10,0,NULL,0,NULL),
	(6,'Laptop','LTP',6,2,'2015-05-30 03:41:22','2015-05-30 03:41:22',0,10,0,NULL,0,NULL),
	(7,'Office Equipment','OE',6,2,'2015-05-30 03:41:40','2015-05-30 13:28:40',0,10,0,NULL,0,NULL),
	(8,'Smartphones','SP',0,4,'2015-05-31 15:25:42','2015-05-31 16:09:10',0,10,0,NULL,0,NULL),
	(9,'Vans','VAN',0,5,'2015-05-31 15:27:40','2015-06-01 04:07:50',0,10,0,NULL,0,NULL),
	(10,'Servers','SRV',0,3,'2015-05-31 15:27:54','2015-05-31 15:27:54',0,10,0,NULL,0,NULL),
	(11,'Access Point (Bridge)','AP',0,2,'2015-06-01 03:49:40','2015-06-01 05:10:45',0,12,0,NULL,0,NULL),
	(12,'Luxury Car','LC',0,6,'2015-06-01 03:57:28','2015-06-01 03:57:28',0,12,0,NULL,0,NULL);

/*!40000 ALTER TABLE `models` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table requested_assets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `requested_assets`;

CREATE TABLE `requested_assets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `accepted_at` datetime DEFAULT NULL,
  `denied_at` datetime DEFAULT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table requests
# ------------------------------------------------------------

DROP TABLE IF EXISTS `requests`;

CREATE TABLE `requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `request_code` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) NOT NULL,
  `per_page` int(11) NOT NULL DEFAULT '20',
  `site_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Snipe IT Asset Management',
  `qr_code` int(11) DEFAULT NULL,
  `qr_text` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `display_asset_name` int(11) DEFAULT NULL,
  `display_checkout_date` int(11) DEFAULT NULL,
  `display_eol` int(11) DEFAULT NULL,
  `auto_increment_assets` int(11) NOT NULL DEFAULT '0',
  `auto_increment_prefix` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `load_remote` tinyint(1) NOT NULL DEFAULT '1',
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `header_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alert_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alerts_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `default_eula_text` longtext COLLATE utf8_unicode_ci,
  `barcode_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'QRCODE',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`id`, `created_at`, `updated_at`, `user_id`, `per_page`, `site_name`, `qr_code`, `qr_text`, `display_asset_name`, `display_checkout_date`, `display_eol`, `auto_increment_assets`, `auto_increment_prefix`, `load_remote`, `logo`, `header_color`, `alert_email`, `alerts_enabled`, `default_eula_text`, `barcode_type`)
VALUES
	(1,'2015-04-14 06:50:15','2015-04-24 02:57:00',1,50,'Pusat Internet 1 Malaysia',1,'testing',0,0,0,0,'',1,'logo.png','#fff','',1,'','QRCODE');

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sites`;

CREATE TABLE `sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(10) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL,
  `site_type` varchar(10) DEFAULT NULL,
  `location_id` int(3) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sites` WRITE;
/*!40000 ALTER TABLE `sites` DISABLE KEYS */;

INSERT INTO `sites` (`id`, `name`, `code`, `userid`, `state_id`, `created_at`, `deleted_at`, `site_type`, `location_id`, `updated_at`)
VALUES
	(1,'Kg Kopimpinan, Penampang','2200011_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',1,'0000-00-00 00:00:00'),
	(2,'Kg Duvanson Ketiau, Putatan','2200012_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',2,'0000-00-00 00:00:00'),
	(3,'Kg Kuala, Papar','2200018_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',3,'0000-00-00 00:00:00'),
	(4,'Kg Biau, Bongawan Papar','2200019_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',4,'0000-00-00 00:00:00'),
	(5,'Kg. Lawa Kabajang, Beaufort ','2200022_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',5,'0000-00-00 00:00:00'),
	(6,'Kg Bundu, Kuala Penyu','2200020_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',6,'0000-00-00 00:00:00'),
	(7,'Pekan Kuala Penyu','2200017_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',7,'0000-00-00 00:00:00'),
	(8,'Pekan Menumbok','2200023_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',8,'0000-00-00 00:00:00'),
	(9,'Pekan Putera Jaya','2200119_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',9,'0000-00-00 00:00:00'),
	(10,'Pekan Babagon','2200123_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',10,'0000-00-00 00:00:00'),
	(11,'Kg. Inobong','2200116_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',11,'0000-00-00 00:00:00'),
	(12,'Kg Muhibbah, Putatan','2200122_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',12,'0000-00-00 00:00:00'),
	(13,'Pekan Kg Langkuas','2200112_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',13,'0000-00-00 00:00:00'),
	(14,'Kg Belatik','2200111_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',14,'0000-00-00 00:00:00'),
	(15,'Rumah Kebajikan OKU Kimanis','2200125_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',15,'0000-00-00 00:00:00'),
	(16,'Pekan Kg. Bambangan','2200124_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',16,'0000-00-00 00:00:00'),
	(17,'Pekan Kg Lubok','2200110_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',17,'0000-00-00 00:00:00'),
	(18,'Kuala Mengalong','2200118_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',18,'0000-00-00 00:00:00'),
	(19,'Lubok Temiang','2200115_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',19,'0000-00-00 00:00:00'),
	(20,'Kg Batu Payung, Tawau','2200026_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',20,'0000-00-00 00:00:00'),
	(21,'Kg Airport, Kunak','2200010_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',21,'0000-00-00 00:00:00'),
	(22,'Kg Hampilan, Kunak','2200009_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',22,'0000-00-00 00:00:00'),
	(23,'Kg Kadazan, Kunak','2000009_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',23,'0000-00-00 00:00:00'),
	(24,'Kg Lormalong, Kunak','2000008_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',24,'0000-00-00 00:00:00'),
	(25,'Pekan Kunak, Kunak','2000010_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',25,'0000-00-00 00:00:00'),
	(26,'Pekan Tungku, Tungku','2200027_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',26,'0000-00-00 00:00:00'),
	(27,'Pekan Beluran, Beluran','2200007_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',27,'0000-00-00 00:00:00'),
	(28,'Kg Kuala Sapi, Beluran','2000003_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',28,'0000-00-00 00:00:00'),
	(29,'Kg Bintang Mas, Beluran','2000001_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',29,'0000-00-00 00:00:00'),
	(30,'Pekan Telupid, Beluran','2000002_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',30,'0000-00-00 00:00:00'),
	(31,'Kg Wonod, Beluran','2200008_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',31,'0000-00-00 00:00:00'),
	(32,'Kg Linayukan, Tongod','2000014_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',32,'0000-00-00 00:00:00'),
	(33,'Kg Sogo Sogo, Tongod','2000015_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',33,'0000-00-00 00:00:00'),
	(34,'Pekan Tongod, Tongod','2000013_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',34,'0000-00-00 00:00:00'),
	(35,'Kg Balung Cocos, Tawau','2200025_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',35,'0000-00-00 00:00:00'),
	(36,'Layung Industrial, Lahad Datu','2200028_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',36,'0000-00-00 00:00:00'),
	(37,'Kg Bugaya, Semporna','2200117_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',37,'0000-00-00 00:00:00'),
	(38,'Pejabat Daerah Kudat, Kudat (AM)','2200016_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',38,'0000-00-00 00:00:00'),
	(39,'Kg Tandek, Kota Marudu','2200015_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',39,'0000-00-00 00:00:00'),
	(40,'Kg Sg Damit, Tuaran','2200014_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',40,'0000-00-00 00:00:00'),
	(41,'Kg Malanggang Baru, Kiulu','2200013_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',41,'0000-00-00 00:00:00'),
	(42,'Kg Mesilou, Ranau','2200005_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',42,'0000-00-00 00:00:00'),
	(43,'Kg. Desa Aman, Ranau','2000011_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',43,'0000-00-00 00:00:00'),
	(44,'Kg Lohan, Ranau','2000012_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',44,'0000-00-00 00:00:00'),
	(45,'Kg Bongkud, Ranau','2200006_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',45,'0000-00-00 00:00:00'),
	(46,'Kg Toboh, Tambunan','2200021_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',46,'0000-00-00 00:00:00'),
	(47,'Kg Bingkor, Keningau','2000006_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',47,'0000-00-00 00:00:00'),
	(48,'Kg Sook, Keningau','2000007_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',48,'0000-00-00 00:00:00'),
	(49,'Kg Apin Apin, Keningau','2200001_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',49,'0000-00-00 00:00:00'),
	(50,'Kg Bunsit, Keningau','2200002_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',50,'0000-00-00 00:00:00'),
	(51,'Taman Sabana, Keningau','2200003_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',51,'0000-00-00 00:00:00'),
	(52,'Kg Malima, Keningau','2200004_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',52,'0000-00-00 00:00:00'),
	(53,'Kg Sawang (AM)','2200024_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',53,'0000-00-00 00:00:00'),
	(54,'Kg Suang Punggur','2200114_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',54,'0000-00-00 00:00:00'),
	(55,'Pekan Kg Serusop, Tuaran','2200120_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',55,'0000-00-00 00:00:00'),
	(56,'Kg Pamilaan','2200121_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',56,'0000-00-00 00:00:00'),
	(57,'Pekan Rampayan Laut','2200113_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',57,'0000-00-00 00:00:00'),
	(58,'Rumah Benjamin','2000004_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',58,'0000-00-00 00:00:00'),
	(59,'Kg. Nanga Tada','2200029_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',59,'0000-00-00 00:00:00'),
	(60,'Kg. Muhibbah','2200030_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',60,'0000-00-00 00:00:00'),
	(61,'Felda Kahang Barat','2100005_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',61,'0000-00-00 00:00:00'),
	(62,'Felda Ayer Hitam','2100004_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',62,'0000-00-00 00:00:00'),
	(63,'Felda Kahang Timur','2100006_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',63,'0000-00-00 00:00:00'),
	(64,'Felda Bukit Tongkat','2100007_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',64,'0000-00-00 00:00:00'),
	(65,'Felda Palong Timur 1','2100010_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',65,'0000-00-00 00:00:00'),
	(66,'Felda Palong Timur 3','2100012_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',66,'0000-00-00 00:00:00'),
	(67,'Felda Bukit Permai','2100002_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',67,'0000-00-00 00:00:00'),
	(68,'Felda Bukit Batu','2100001_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',68,'0000-00-00 00:00:00'),
	(69,'Felda Layang-Layang','2100008_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',69,'0000-00-00 00:00:00'),
	(70,'Kg. Parit Hj. Idris','2100009_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',70,'0000-00-00 00:00:00'),
	(71,'Felda Inas Utara','2100003_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',71,'0000-00-00 00:00:00'),
	(72,'Kg. Mensudut Lama','2100011_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',72,'0000-00-00 00:00:00'),
	(73,'Felda Bukit Tangga','2100019_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',73,'0000-00-00 00:00:00'),
	(74,'PPR Pantai','2200107_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',74,'0000-00-00 00:00:00'),
	(75,'PPR Kerinchi','2200109_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',75,'0000-00-00 00:00:00'),
	(76,'Felda Kepis','2100013_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',76,'0000-00-00 00:00:00'),
	(77,'Felda Jelai 4','2100014_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',77,'0000-00-00 00:00:00'),
	(78,'Felda Pasir Besar','2100015_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',78,'0000-00-00 00:00:00'),
	(79,'Felda Sg Kelamah','2100016_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',79,'0000-00-00 00:00:00'),
	(80,'Felda Bukit Jalor','2100017_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',80,'0000-00-00 00:00:00'),
	(81,'Felda Bukit Rokan','2100018_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',81,'0000-00-00 00:00:00'),
	(82,'Felda Chuping','2100020_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',82,'0000-00-00 00:00:00'),
	(83,'Felda Mata Air','2100021_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',83,'0000-00-00 00:00:00'),
	(84,'Felda Rimba Mas','2100022_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',84,'0000-00-00 00:00:00'),
	(85,'Laman Pengurus Pi1M','0_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',85,'0000-00-00 00:00:00'),
	(86,'Kg. Padang Lalang','2200153_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',86,'0000-00-00 00:00:00'),
	(87,'Kg. Tambalang','0_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',87,'0000-00-00 00:00:00'),
	(88,'Pekan Membakut','0_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',88,'0000-00-00 00:00:00'),
	(89,'Pekan Weston','0_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',89,'0000-00-00 00:00:00'),
	(90,'Bandar Baru Teluk Intan','0_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',90,'0000-00-00 00:00:00'),
	(91,'Taman Muhibbah, Teluk Intan','0_00',1,NULL,'0000-00-00 00:00:00',NULL,'PIM',91,'0000-00-00 00:00:00'),
	(92,'1 Jln Sekolah PKT1','2100004_01',1,NULL,'0000-00-00 00:00:00',NULL,'KTW',62,'2015-06-24 10:05:53'),
	(93,'No.152 Jln Bunga Tanjung, PKT2','2100004_02',1,NULL,'0000-00-00 00:00:00',NULL,'KTW',62,'0000-00-00 00:00:00'),
	(94,'No.198 Jln Bkt Senyam, PKT1','2100004_03',1,NULL,'0000-00-00 00:00:00',NULL,'KTW',62,'0000-00-00 00:00:00'),
	(95,'107 Jln Tanjung','2100001_01',1,NULL,'0000-00-00 00:00:00',NULL,'KTW',68,'0000-00-00 00:00:00'),
	(96,'Klinik Desa','2100001_02',1,NULL,'0000-00-00 00:00:00',NULL,'KTW',68,'0000-00-00 00:00:00'),
	(97,'Masjid Kg Sri Paya Kelapa,Sawit','2100001_03',1,NULL,'0000-00-00 00:00:00',NULL,'KTW',68,'0000-00-00 00:00:00'),
	(1001,'NuSuara','1001_00',1,NULL,'0000-00-00 00:00:00',NULL,'',1,'0000-00-00 00:00:00'),
	(1002,'newsite','NS',NULL,NULL,'2015-06-11 05:09:01',NULL,'CEN',62,'2015-06-11 05:09:01'),
	(1003,'newsite2','ns2',10,NULL,'2015-06-11 05:21:08',NULL,'PIM',80,'2015-06-11 05:21:08'),
	(1006,'sitecsv1','SCSV1',10,NULL,'2015-06-11 07:40:29',NULL,'PIM',3,'2015-06-11 07:40:29'),
	(1007,'sitecsv2','SCSV2',10,NULL,'2015-06-11 07:40:29',NULL,'PIM',3,'2015-06-11 07:40:29');

/*!40000 ALTER TABLE `sites` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table state
# ------------------------------------------------------------

DROP TABLE IF EXISTS `state`;

CREATE TABLE `state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `state` WRITE;
/*!40000 ALTER TABLE `state` DISABLE KEYS */;

INSERT INTO `state` (`id`, `name`, `code`, `created_at`, `user_id`, `deleted_at`)
VALUES
	(1,'Johor','JHR',NULL,NULL,NULL),
	(2,'Kedah','KDH',NULL,NULL,NULL),
	(3,'Kelantan','KTN',NULL,NULL,NULL),
	(4,'Melaka','MLK',NULL,NULL,NULL),
	(5,'Negeri Sembilan','NSM',NULL,NULL,NULL),
	(6,'Pahang','PHG',NULL,NULL,NULL),
	(7,'Pulau Pinang','PPG',NULL,NULL,NULL),
	(8,'Perak','PRK',NULL,NULL,NULL),
	(9,'Perlis','PLS',NULL,NULL,NULL),
	(10,'Selangor','SGR',NULL,NULL,NULL),
	(11,'Terengganu','TRG',NULL,NULL,NULL),
	(12,'Sabah','SBH',NULL,NULL,NULL),
	(13,'Sarawak','SWK',NULL,NULL,NULL),
	(14,'Kuala Lumpur','WKL',NULL,NULL,NULL),
	(15,'Labuan','LBN',NULL,NULL,NULL),
	(16,'Putrajaya','PJY',NULL,NULL,NULL);

/*!40000 ALTER TABLE `state` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table status_labels
# ------------------------------------------------------------

DROP TABLE IF EXISTS `status_labels`;

CREATE TABLE `status_labels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deployable` tinyint(1) NOT NULL DEFAULT '0',
  `pending` tinyint(1) NOT NULL DEFAULT '0',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `dismissed` tinyint(1) NOT NULL DEFAULT '0',
  `notes` text COLLATE utf8_unicode_ci,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `status_labels` WRITE;
/*!40000 ALTER TABLE `status_labels` DISABLE KEYS */;

INSERT INTO `status_labels` (`id`, `name`, `user_id`, `created_at`, `updated_at`, `deleted_at`, `deployable`, `pending`, `archived`, `dismissed`, `notes`, `code`)
VALUES
	(1,'Ready to Deploy',1,'2015-01-13 06:50:15','2015-01-13 06:50:15',NULL,1,0,0,0,NULL,NULL),
	(2,'Pending',1,'2015-01-13 06:50:15','2015-01-13 06:50:15',NULL,0,1,0,0,NULL,NULL),
	(3,'Archived',1,'2015-01-13 06:50:15','2015-04-17 09:10:51',NULL,0,0,1,0,'','ARC'),
	(4,'Out for Diagnostics',1,'2015-01-13 06:50:15','2015-01-13 06:50:15',NULL,0,0,0,0,NULL,NULL),
	(5,'Out for Repair',1,'2015-01-13 06:50:15','2015-01-13 06:50:15',NULL,0,0,0,0,NULL,NULL),
	(6,'Broken - Not Fixable',1,'2015-01-13 06:50:15','2015-01-13 06:50:15',NULL,0,0,1,0,NULL,NULL),
	(7,'Lost/Stolen',1,'2015-01-13 06:50:15','2015-01-13 06:50:15',NULL,0,0,0,1,NULL,NULL),
	(8,'Testing',3,'2015-04-17 09:11:08','2015-04-17 09:11:08',NULL,0,0,0,0,'notes','TST'),
	(9,'Assigned To Location',1,'2015-05-30 00:00:00','0000-00-00 00:00:00',NULL,0,0,0,0,NULL,'DPT'),
	(10,'Donated',10,'2015-06-08 04:01:24','2015-06-08 04:01:24',NULL,0,0,0,1,'','DNT'),
	(11,'Disposed',10,'2015-06-08 04:01:46','2015-06-08 04:01:46',NULL,0,0,0,1,'','DPS'),
	(12,'Sold',10,'2015-06-08 04:01:59','2015-06-08 04:01:59',NULL,0,0,0,1,'','SLD');

/*!40000 ALTER TABLE `status_labels` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table suppliers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `suppliers`;

CREATE TABLE `suppliers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `zip` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `suppliers` WRITE;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;

INSERT INTO `suppliers` (`id`, `name`, `address`, `address2`, `city`, `state`, `country`, `phone`, `fax`, `email`, `contact`, `notes`, `created_at`, `updated_at`, `user_id`, `deleted_at`, `zip`, `url`, `image`, `code`)
VALUES
	(1,'Dell','','','','','','','','','','','2015-04-17 08:50:41','2015-04-17 09:51:06',3,NULL,'','http://',NULL,'DEL'),
	(2,'Toshiba','','','','','','','','','','','2015-04-17 09:51:26','2015-04-17 09:51:26',3,NULL,'','http://',NULL,'TSB');

/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table throttle
# ------------------------------------------------------------

DROP TABLE IF EXISTS `throttle`;

CREATE TABLE `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `throttle` WRITE;
/*!40000 ALTER TABLE `throttle` DISABLE KEYS */;

INSERT INTO `throttle` (`id`, `user_id`, `ip_address`, `attempts`, `suspended`, `banned`, `last_attempt_at`, `suspended_at`, `banned_at`)
VALUES
	(1,1,'::1',0,0,0,NULL,NULL,NULL),
	(2,2,NULL,0,0,0,NULL,NULL,NULL),
	(3,3,NULL,0,0,0,NULL,NULL,NULL),
	(4,4,NULL,0,0,0,NULL,NULL,NULL),
	(5,5,NULL,0,0,0,NULL,NULL,NULL),
	(6,6,NULL,0,0,0,NULL,NULL,NULL),
	(7,7,NULL,0,0,0,NULL,NULL,NULL),
	(8,8,NULL,0,0,0,NULL,NULL,NULL),
	(9,1,'175.142.8.114',0,0,0,NULL,NULL,NULL),
	(10,9,NULL,0,0,0,NULL,NULL,NULL),
	(11,1,'118.101.228.159',0,0,0,NULL,NULL,NULL),
	(12,10,NULL,0,0,0,NULL,NULL,NULL),
	(13,11,NULL,0,0,0,'2015-06-02 08:51:09','2015-06-02 08:51:09',NULL),
	(14,12,NULL,0,0,0,NULL,NULL,NULL),
	(15,13,NULL,0,0,0,NULL,NULL,NULL),
	(16,1,'175.144.24.235',0,0,0,NULL,NULL,NULL),
	(17,14,NULL,0,0,0,NULL,NULL,NULL);

/*!40000 ALTER TABLE `throttle` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `types`;

CREATE TABLE `types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `types` WRITE;
/*!40000 ALTER TABLE `types` DISABLE KEYS */;

INSERT INTO `types` (`id`, `name`, `code`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'Desktop','DSK','2015-04-14 09:37:28','2015-04-14 09:37:28',NULL),
	(2,'Network Access Point','Operations','2015-05-25 03:25:53','2015-05-25 03:25:53',NULL);

/*!40000 ALTER TABLE `types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jobtitle` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manager_id` int(11) DEFAULT NULL,
  `employee_num` text COLLATE utf8_unicode_ci,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dept_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_activation_code_index` (`activation_code`),
  KEY `users_reset_password_code_index` (`reset_password_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `password`, `permissions`, `activated`, `activation_code`, `activated_at`, `last_login`, `persist_code`, `reset_password_code`, `first_name`, `last_name`, `created_at`, `updated_at`, `deleted_at`, `website`, `country`, `gravatar`, `location_id`, `phone`, `jobtitle`, `manager_id`, `employee_num`, `avatar`, `username`, `dept_id`)
VALUES
	(1,'liyana@fulkrum.net','$2y$10$gRogsotAJpS9wS3eIcFrFuX0vsQB/tb36rqLT0ZaQbH2ato7WAS32','{\"user\":1,\"superuser\":-1}',1,NULL,NULL,'2015-05-19 08:52:22','$2y$10$6oHPjaGdVcwa.J/2hfLrjeu7SKHP5ZFSN0oSbtDuNLRWMClEg0AmC',NULL,'liyana','zaini','2015-04-13 07:10:26','2015-05-19 08:52:22',NULL,NULL,NULL,NULL,3,'','',NULL,'',NULL,NULL,1),
	(2,'john.doe@example.com','$2y$10$ERUevCkBdaYXtkn.JDfn.O.IxW/pNjhay5RBPG6K38xFHrOnunywS',NULL,1,NULL,NULL,NULL,NULL,NULL,'John','Doe','2015-04-13 07:10:27','2015-04-13 07:10:27',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(3,'azlan@fulkrum.net','$2y$10$2MYaO8jq/DHWvUHZ.zz7jeHuOwuO2utVYLKLNbuJcGtvotCw3HDIy','{\"superuser\":1}',1,NULL,NULL,'2015-06-01 03:38:31','$2y$10$r1vFDVGOUaSm8SdTb9.YbeKy0i.FV/3sYJhygScW6l6XSVkl5BmM2',NULL,'alan','noh','2015-04-14 06:46:13','2015-06-01 03:38:31',NULL,NULL,NULL,NULL,2,'','Mr',NULL,'',NULL,NULL,NULL),
	(4,'abubakar@fulkrum.net','$2y$10$uU1ncFANvuT6gckbh45rau3Pci.naJ6rgCTKogZ4u0gnGKFkdpire','{\"superuser\":-1}',1,NULL,NULL,NULL,NULL,NULL,'Abu','Bakar','2015-04-15 01:42:11','2015-05-26 18:33:56',NULL,NULL,NULL,NULL,4,'','Mr',NULL,'111',NULL,NULL,NULL),
	(5,'johnsmith@fulkrum.net','$2y$10$C86A3S3/jQdXaLd6JT./0e8p5VKCgwD334RiTVFL3DeXtxuGfXdEm','{\"superuser\":-1}',1,NULL,NULL,NULL,NULL,NULL,'john','smith','2015-04-15 02:10:10','2015-04-15 02:11:14',NULL,NULL,NULL,NULL,1,'','MR',NULL,'11111',NULL,NULL,1),
	(6,'mariasanta@fulkrum.net','$2y$10$VAxp1KrYYhnCxk8Xuu8yv.R5lFRw053zfP6.tj8j4xnoPe1rc7FsC','{\"user\":1,\"superuser\":-1}',1,NULL,NULL,'2015-05-15 08:58:02','$2y$10$RRsytpifSIQe2QFYofuzm.4X5T00ENxqRwWfBwZAt4E2jeFIXlw1C',NULL,'maria','santa','0000-00-00 00:00:00','2015-06-08 03:51:19','2015-06-08 03:51:19',NULL,NULL,NULL,5,'','',NULL,'',NULL,NULL,4),
	(7,'indianajones@fulkrum.net','$2y$10$pHEibqFSFh3HyvBudu5b6uhWkP6GJMz0PQ.9JB3A.ze6ZpfeR77MS','{\"user\":1}',1,NULL,NULL,NULL,NULL,NULL,'indiana','jones','0000-00-00 00:00:00','2015-04-22 06:33:38',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3),
	(8,'moridh@fulkrum.net','$2y$10$7pc6WDLvwfdHpoeuVBljmuM8zdki9zK7lzD9sQ7Hu3Mna8sp9yXM2','{\"superuser\":1}',1,NULL,NULL,'2015-05-30 05:35:10','$2y$10$hG2tJPzCtn3tOMslSAvtgOEoXnpT.Lal7EtTmmTXORS5LZ6n1pecK',NULL,'ridhwan','bakir','2015-04-28 10:16:15','2015-05-30 05:35:33',NULL,NULL,NULL,NULL,5,'01334277704','Mr',3,'001',NULL,NULL,NULL),
	(9,'luqman@fulkrum.net','$2y$10$DHzRQgPgder5LdZII2028eMYJyFpEpTcgjp7.DTL7f/4ZwXhxdPCC','{\"superuser\":1}',1,NULL,NULL,'2015-05-12 07:18:59','$2y$10$S/lM24rYUnxqq8Da3qoD1ecW./gCTy//QSl79Wez1qEd24UJnDjX.',NULL,'Luqman','Adris','2015-05-12 02:38:40','2015-05-12 07:18:59',NULL,NULL,NULL,NULL,5,'','Mr',NULL,'0010',NULL,NULL,NULL),
	(10,'root@gmail.com','$2y$10$CrwcpVwHg3Txb7dnOvXXKORlqnLtCHtQOcV7hN6W0GPPyZunl/9rW','{\"superuser\":1}',1,NULL,NULL,'2015-06-24 10:04:47','$2y$10$dxu86R7xkVxpH1qehZ5Bm.c5Dt5Ro267MA8ydEI7qBT8ja7YVaPci',NULL,'root','root','2015-05-18 09:41:25','2015-06-24 10:04:47',NULL,NULL,NULL,NULL,5,'','',NULL,'',NULL,NULL,NULL),
	(11,'admin@gmail.com','$2y$10$HTkvmM77ZImbFJV7ru2t0eYGpCnYTypOl3OAacd.Fl/nzis8QcaZW','{\"superuser\":-1}',1,NULL,NULL,'2015-06-08 04:26:17','$2y$10$IBdgALG/HCGuqczmwBHuDOOnhf65l2lKu.QdO0uT3dk76oeuv5dWe',NULL,'admin','admin','2015-05-18 09:41:56','2015-06-08 04:26:17',NULL,NULL,NULL,NULL,5,'','',NULL,'',NULL,NULL,6),
	(12,'finance@gmail.com','$2y$10$p.Bt798andP/id8px.i9v.rzTW2hjrBX8kaot2uG0VTCIZU97ayU.','{\"superuser\":-1}',1,NULL,NULL,'2015-06-08 04:25:39','$2y$10$tvkpyKETk81BYYdPqdrw4egnByUFQyJpJP6CQKPGpAAeRauk6ut/S',NULL,'finance','finance','2015-05-18 09:42:22','2015-06-08 04:25:39',NULL,NULL,NULL,NULL,5,'','',NULL,'',NULL,NULL,4),
	(13,'user@gmail.com','$2y$10$eIvcuDlqoigYbZp1Fk681.yGEBxQPPL1SYm.4pxxKruKN.3kj4Uk2','{\"superuser\":-1}',1,NULL,NULL,'2015-06-03 09:56:04','$2y$10$pb5sl8EWcN.nyWIpCMF1du74JBd8afpQcDiD4VF0q5CTTzj9s1i1S',NULL,'user','user','2015-05-18 09:43:44','2015-06-05 02:05:17',NULL,NULL,NULL,NULL,5,'','',NULL,'',NULL,NULL,1),
	(14,'testuser1@gmail.com','$2y$10$dZSK3/psuh5scj/G5SSkt.jhv1HDyNF1MI2QLZvl35MYPhD/nviq.','{\"superuser\":-1}',1,NULL,NULL,NULL,NULL,NULL,'testuser1','testuser1','2015-06-03 09:24:37','2015-06-05 02:08:04','2015-06-05 02:08:04',NULL,NULL,NULL,5,'','',NULL,'',NULL,NULL,3);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_groups`;

CREATE TABLE `users_groups` (
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;

INSERT INTO `users_groups` (`user_id`, `group_id`)
VALUES
	(1,1),
	(3,1),
	(5,3),
	(8,5),
	(9,1),
	(10,1),
	(11,1),
	(12,4);

/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
