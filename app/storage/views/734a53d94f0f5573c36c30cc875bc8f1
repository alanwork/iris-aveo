<?php /* Page title */ ?>
<?php $__env->startSection('title'); ?>
	<?php if($user->id): ?>
		<?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.updateuser'); ?>
		<?php echo $user->fullName(); ?> ::
	<?php else: ?>
		<?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.createuser'); ?> ::
	<?php endif; ?>

@parent
<?php $__env->stopSection(); ?>

<?php /* Page content */ ?>
<?php $__env->startSection('content'); ?>
<div class="page-header">

        <div class="pull-right">
            <a href="<?php echo URL::previous(); ?>" class="btn-flat gray"><i class="fa fa-arrow-left icon-white"></i>  <?php echo \Illuminate\Support\Facades\Lang::get('general.back'); ?></a>
        </div>
    <h3>
        <?php if($user->id): ?>
            <?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.updateuser'); ?>
            <?php echo $user->fullName(); ?>

	<?php elseif(isset($clone_user)): ?>
            <?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.cloneuser'); ?>
        <?php else: ?>
            <?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.createuser'); ?>
	<?php endif; ?>
    </h3>
</div>

<!-- Tabs -->
<ul class="nav nav-tabs">
    <li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
    <li><a href="#tab-permissions" data-toggle="tab">Permissions</a></li>
</ul>

<form class="form-horizontal" method="post" action="" autocomplete="off">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

    <!-- Tabs Content -->
    <div class="tab-content">
        <!-- General tab -->
        <div class="tab-pane active" id="tab-general">

         <div class="row form-wrapper">
            <div class="col-md-12 column">
            <br><br>

            <!-- First Name -->
            <div class="form-group <?php echo $errors->has('first_name') ? 'has-error' : ''; ?>">
                <label class="col-md-3 control-label" for="first_name"><?php echo \Illuminate\Support\Facades\Lang::get('general.first_name'); ?>
                <i class='fa fa-asterisk'></i></label>
                <div class="col-md-7">
                    <input class="form-control" type="text" name="first_name" id="first_name" value="<?php echo e(Input::old('first_name', $user->first_name)); ?>" />
                    <?php echo $errors->first('first_name', '<br><span class="alert-msg">:message</span>'); ?>

                </div>
            </div>

            <!-- Last Name -->
            <div class="form-group <?php echo $errors->has('last_name') ? 'has-error' : ''; ?>">
                <label class="col-md-3 control-label" for="first_name"><?php echo \Illuminate\Support\Facades\Lang::get('general.last_name'); ?> <i class='fa fa-asterisk'></i></label>
                <div class="col-md-7">
                    <input class="form-control" type="text" name="last_name" id="last_name" value="<?php echo e(Input::old('last_name', $user->last_name)); ?>" />
                    <?php echo $errors->first('last_name', '<br><span class="alert-msg">:message</span>'); ?>

                </div>
            </div>

			<!-- Email -->
            <div class="form-group <?php echo $errors->has('email') ? 'has-error' : ''; ?>">
                <label class="col-md-3 control-label" for="email"><?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.email'); ?> <i class='fa fa-asterisk'></i></label>
                <div class="col-md-7">
                    <input class="form-control" type="text" name="email" id="email" value="<?php echo e(Input::old('email', $user->email)); ?>"  <?php echo ((Config::get('app.lock_passwords') && ($user->id)) ? ' disabled' : ''); ?>>
                     <?php if(Config::get('app.lock_passwords') && ($user->id)): ?> 
					 	<p class="help-block"><?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.lock_passwords'); ?></p>
					 <?php endif; ?>

                    <?php echo $errors->first('email', '<br><span class="alert-msg">:message</span>'); ?>

                </div>
            </div>

        	<!-- Employee Number -->
            <div class="form-group <?php echo $errors->has('employee_num') ? 'has-error' : ''; ?>">
                <label class="col-md-3 control-label" for="employee_num"><?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.employee_num'); ?></label>
                <div class="col-md-7">
                    <input class="form-control" type="text" name="employee_num" id="employee_num" value="<?php echo e(Input::old('employee_num', $user->employee_num)); ?>" />
                    <?php echo $errors->first('employee_num', '<br><span class="alert-msg">:message</span>'); ?>

                </div>
            </div>


            <!-- Jobtitle -->
            <div class="form-group <?php echo $errors->has('jobtitle') ? 'has-error' : ''; ?>">
                <label class="col-md-3 control-label" for="jobtitle"><?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.title'); ?></label>
                <div class="col-md-7">
                    <input class="form-control" type="text" name="jobtitle" id="jobtitle" value="<?php echo e(Input::old('jobtitle', $user->jobtitle)); ?>" />
                    <?php echo $errors->first('jobtitle', '<br><span class="alert-msg">:message</span>'); ?>

                </div>
            </div>


			<!-- Manager -->
            <div class="form-group <?php echo $errors->has('manager_id') ? 'has-error' : ''; ?>">
                <label class="col-md-3 control-label" for="manager_id"><?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.manager'); ?></label>
                <div class="col-md-7">
                    <?php echo Form::select('manager_id', $manager_list , Input::old('manager_id', $user->manager_id), array('class'=>'select2', 'style'=>'width:250px')); ?>

                    <?php echo $errors->first('manager_id', '<br><span class="alert-msg">:message</span>'); ?>

                </div>
            </div>

			<!-- Location -->
            <div class="form-group <?php echo $errors->has('location_id') ? 'has-error' : ''; ?>">
                <label class="col-md-3 control-label" for="location_id"><?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.location'); ?>
                    <i class='fa fa-asterisk'></i></label>
                <div class="col-md-7">
                    <?php echo Form::select('location_id', $location_list , Input::old('location_id', $user->location_id), array('class'=>'select2', 'style'=>'width:250px')); ?>

                    <?php echo $errors->first('location_id', '<br><span class="alert-msg">:message</span>'); ?>

                </div>
            </div>

            <!-- Department -->
            <div class="form-group <?php echo $errors->has('dept_id') ? 'has-error' : ''; ?>">
                <label class="col-md-3 control-label" for="dept_id"><?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.department'); ?>
                    <i class='fa fa-asterisk'></i></label>
                <div class="col-md-7">
                    <?php echo Form::select('dept_id', $dept_list , Input::old('dept_id', $user->dept_id), array('class'=>'select2', 'style'=>'width:250px')); ?>

                    <?php echo $errors->first('dept_id', '<br><span class="alert-msg">:message</span>'); ?>

                </div>
            </div>

			<!-- Phone -->
            <div class="form-group <?php echo $errors->has('phone') ? 'has-error' : ''; ?>">
                <label class="col-md-3 control-label" for="phone"><?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.phone'); ?></label>
                <div class="col-md-4">
                    <input class="form-control" type="text" name="phone" id="phone" value="<?php echo e(Input::old('phone', $user->phone)); ?>" />
                    <?php echo $errors->first('phone', '<br><span class="alert-msg">:message</span>'); ?>

                </div>
            </div>

            <!-- Password -->
            <div class="form-group <?php echo $errors->has('password') ? 'has-error' : ''; ?>">
                <label class="col-md-3 control-label" for="password"><?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.password'); ?>
                <?php if(!$user->id): ?>
                <i class='fa fa-asterisk'></i>
                <?php endif; ?>
                </label>
                <div class="col-md-5">
                   <input type="password" name="password" class="form-control" id="password" value="" <?php echo ((Config::get('app.lock_passwords') && ($user->id)) ? ' disabled' : ''); ?>>
                    <?php echo $errors->first('password', '<br><span class="alert-msg">:message</span>'); ?>

                </div>
            </div>

            <!-- Password Confirm -->
            <div class="form-group <?php echo $errors->has('password_confirm') ? 'has-error' : ''; ?>">
                <label class="col-md-3 control-label" for="password_confirm"><?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.password_confirm'); ?>
                <?php if(!$user->id): ?>
                <i class='fa fa-asterisk'></i>
                <?php endif; ?>
                </label>
                <div class="col-md-5">
                   <input type="password" name="password_confirm" id="password_confirm"  class="form-control" value="" <?php echo ((Config::get('app.lock_passwords') && ($user->id)) ? ' disabled' : ''); ?>>
                   <?php if(Config::get('app.lock_passwords') && ($user->id)): ?> 
                    <p class="help-block"><?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.lock_passwords'); ?></p>
                   <?php endif; ?>
                    <?php echo $errors->first('password_confirm', '<br><span class="alert-msg">:message</span>'); ?>

                </div>
            </div>


            <!-- Username -->
            <div class="form-group <?php echo $errors->has('username') ? 'has-error' : ''; ?>">
                <label class="col-md-3 control-label" for="username"><?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.username'); ?></label>
                <div class="col-md-5">
                    <input class="form-control" type="text" name="username" id="username" value="<?php echo e(Input::old('username', $user->username)); ?>" disabled />

                    <?php echo $errors->first('username', '<br><span class="alert-msg">:message</span>'); ?>

                    <p class="help-block"><?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.username_note'); ?></p>
                </div>
            </div>



			<!-- Activation Status -->
            <div class="form-group <?php echo $errors->has('activated') ? 'has-error' : ''; ?>">
                <label class="col-md-3 control-label" for="activated"><?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.activated'); ?></label>
                <div class="col-md-7">
                   <div class="controls">
                    <select<?php echo ($user->id === Sentry::getId() ? ' disabled="disabled"' : ''); ?> name="activated" id="activated" <?php echo ((Config::get('app.lock_passwords') && ($user->id)) ? ' disabled' : ''); ?>>
                    <?php if($user->id): ?>
                    	<option value="1"<?php echo ($user->isActivated() ? ' selected="selected"' : ''); ?>><?php echo \Illuminate\Support\Facades\Lang::get('general.yes'); ?></option>
                        <option value="0"<?php echo ( ! $user->isActivated() ? ' selected="selected"' : ''); ?>><?php echo \Illuminate\Support\Facades\Lang::get('general.no'); ?></option>
                    <?php else: ?>
                    	<option value="1"<?php echo (Input::old('activated') == 1 ? ' selected="selected"' : ''); ?>><?php echo \Illuminate\Support\Facades\Lang::get('general.yes'); ?></option>
                        <option value="0"><?php echo \Illuminate\Support\Facades\Lang::get('general.no'); ?></option>
                    <?php endif; ?>

                    </select>

                    <?php echo $errors->first('activated', '<br><span class="alert-msg">:message</span>'); ?>

                </div>
                </div>
            </div>
            


            <!-- Groups -->
            <div class="form-group <?php echo $errors->has('groups') ? 'has-error' : ''; ?>">
                <label class="col-md-3 control-label" for="groups"><?php echo \Illuminate\Support\Facades\Lang::get('general.groups'); ?></label>
                <div class="col-md-5">
                   <div class="controls">

                    <select name="groups[]" id="groups[]" multiple="multiple" class="form-control" <?php echo ((Config::get('app.lock_passwords') && ($user->id)) ? ' disabled' : ''); ?>>

                        <?php foreach($groups as $group): ?>
                        <option value="<?php echo $group->id; ?>"
                        <?php echo (in_array($group->id, $userGroups) ? ' selected="selected"' : ''); ?>>
                        <?php echo e($group->name); ?>

                        </option>
                        <?php endforeach; ?>
                    </select>

                    <span class="help-block">
                    	<?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.groupnotes'); ?>

                    </span>
                </div>
                </div>
            </div>
            
            <!-- Email user -->
            <?php /*
            <?php if(!$user->id): ?>
			<div class="form-group">
				<div class="col-sm-3 ">
				</div>
				<div class="col-sm-5">
					<?php echo Form::checkbox('email_user', '1', Input::old('email_user')); ?> Email this user their credentials?
				</div>
			</div>
			<?php endif; ?>
            */ ?>


        </div>
          </div>
            </div>

        <!-- Permissions tab -->
        <div class="tab-pane" id="tab-permissions">
        <div class="row form-wrapper">
            <div class="col-md-12 column">
            <br><br>
            
            <?php if(Config::get('app.lock_passwords') && ($user->id)): ?> 
		 	<p class="help-block"><?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.lock_passwords'); ?></p>
		 	<?php endif; ?>

                 <?php foreach($permissions as $area => $permissions): ?>
                    <fieldset>
                        <legend><?php echo $area; ?></legend>

                        <?php foreach($permissions as $permission): ?>
                        <div class="form-group">
                        	<label class="col-md-3 control-label" for="<?php echo e($permission['label']); ?>">
                        	<?php echo e($permission['label']); ?>

                        	</label>
                             <div class="col-md-2">
                             <div class="radio inline">
                                <label for="<?php echo e($permission['permission']); ?>_allow" onclick="">
                                    <input type="radio" value="1" id="<?php echo e($permission['permission']); ?>_allow" name="permissions[<?php echo e($permission['permission']); ?>]"<?php echo (array_get($userPermissions, $permission['permission']) == '1' ? ' checked="checked"' : ''); ?><?php echo ((Config::get('app.lock_passwords') && ($user->id)) ? ' disabled' : ''); ?>>
                                    <?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.allow'); ?>
                                </label>
                            </div>
                             </div>

                            <div class="col-md-2">
                             <div class="radio inline">
                                <label for="<?php echo e($permission['permission']); ?>_deny" onclick="">
                                    <input type="radio" value="-1" id="<?php echo e($permission['permission']); ?>_deny" name="permissions[<?php echo e($permission['permission']); ?>]"<?php echo (array_get($userPermissions, $permission['permission']) == '-1' ? ' checked="checked"' : ''); ?>>
                                    <?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.deny'); ?>
                                </label>
                            </div>
                            </div>

                            <?php if($permission['can_inherit']): ?>
                            <div class="col-md-2">
                             <div class="radio inline">
                                <label for="<?php echo e($permission['permission']); ?>_inherit" onclick="">
                                    <input type="radio" value="0" id="<?php echo e($permission['permission']); ?>_inherit" name="permissions[<?php echo e($permission['permission']); ?>]"<?php echo (array_get($userPermissions, $permission['permission']) == '' ? ' checked="checked"' : ''); ?>>
                                    <?php echo \Illuminate\Support\Facades\Lang::get('admin/users/table.inherit'); ?>
                                </label>
                            </div>
                            </div>
                            <?php endif; ?>
                        </div>
                        <?php endforeach; ?>

                    </fieldset>
                    <?php endforeach; ?>
                    <br><br>

            	</div>
            </div>
        </div>
    </div>

	<!-- Form actions -->
		<div class="form-group">
		<label class="col-md-3 control-label"></label>
			<div class="col-md-7">
				<a class="btn btn-link" href="<?php echo URL::previous(); ?>"><?php echo \Illuminate\Support\Facades\Lang::get('button.cancel'); ?></a>
				<button type="reset" class="btn">Reset</button>
				<button type="submit" class="btn btn-success"><i class="fa fa-check icon-white"></i> <?php echo \Illuminate\Support\Facades\Lang::get('general.save'); ?></button>
			</div>
		</div>

</form>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend/layouts/default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>